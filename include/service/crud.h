#ifndef __AUTOMATION_HTTP_SERVICE_CRUD_H__
#define __AUTOMATION_HTTP_SERVICE_CRUD_H__

#include "vector"
#include "xapp.h"
#include "service/accept.h"

namespace automation::http::service {

class CRUD {

  public:

    CRUD(automation::http::ExtendedApp& app, std::string base_url) :
        app(app), base_url(base_url), base_url_id(base_url + "/:id") { }

    CRUD& add_pre_handler(automation::http::ExtendedApp::Handler handler) {
        common_pre_handler_list.push_back(handler);
        return *this;
    }

    CRUD& add_handler(const std::string& op, automation::http::ExtendedApp::Handler handler) {
        op_handler_list[op].push_back(handler);
        return *this;
    }

    void setup() {

        auto no_implemented_handler = [](auto, auto) -> automation::http::ExtendedApp::Handler {
            throw automation::http::ExtendedApp::not_implemented_error();
        };

        // create resource

        {
            auto& resource = app.post(base_url);
            auto op_handler_it = op_handler_list.find("create");
            if (op_handler_it == op_handler_list.end()) {
                resource << no_implemented_handler;
            } else {
                spdlog::debug("{} : Adding handler for 'create' service.", base_url);
                add_handler(resource, common_pre_handler_list);
                add_handler(resource, op_handler_it->second);
            }
        }

        // read one resource

        {
            auto& resource = app.get(base_url_id);
            auto op_handler_it = op_handler_list.find("readone");
            if (op_handler_it == op_handler_list.end()) {
                resource << no_implemented_handler;
            } else {
                spdlog::debug("{} : Adding handler for 'readone' service.", base_url);
                add_handler(resource, common_pre_handler_list);
                add_handler(resource, op_handler_it->second);
            }
        }

        // read many resource

        {
            auto& resource = app.get(base_url);
            auto op_handler_it = op_handler_list.find("readmany");
            if (op_handler_it == op_handler_list.end()) {
                resource << no_implemented_handler;
            } else {
                spdlog::debug("{} : Adding handler for 'readmany' service.", base_url);
                add_handler(resource, common_pre_handler_list);
                add_handler(resource, op_handler_it->second);
            }
        }

        // update resource

        {
            auto& resource = app.put(base_url_id);
            auto op_handler_it = op_handler_list.find("update");
            if (op_handler_it == op_handler_list.end()) {
                resource << no_implemented_handler;
            } else {
                spdlog::debug("{} : Adding handler for 'update' service.", base_url);
                add_handler(resource, common_pre_handler_list);
                add_handler(resource, op_handler_it->second);
            }
        }

        // delete resource

        {
            auto& resource = app.del(base_url_id);
            auto op_handler_it = op_handler_list.find("delete");
            if (op_handler_it == op_handler_list.end()) {
                resource << no_implemented_handler;
            } else {
                spdlog::debug("{} : Adding handler for 'delete' service.", base_url);
                add_handler(resource, common_pre_handler_list);
                add_handler(resource, op_handler_it->second);
            }
        }

    }

  private:

    automation::http::ExtendedApp& app;
    std::string base_url, base_url_id;

    std::vector<automation::http::ExtendedApp::Handler> common_pre_handler_list;
    std::map<std::string, std::vector<automation::http::ExtendedApp::Handler>> op_handler_list;

    void add_handler (
        automation::http::ExtendedApp::HandlerItem& resource,
        std::vector<automation::http::ExtendedApp::Handler>& list) {

        unsigned int cntr = 0;
        for (auto h : list) {
            cntr += 1;
            resource.add_handler(h);
        }

        spdlog::debug("Added {} handler.", cntr);

    };

};


}


#endif
