#include "xapp.h"

using namespace automation::http;

const std::string ExtendedApp::http_status_ok = "200 OK";
const std::string ExtendedApp::http_status_created = "201 Create";
const std::string ExtendedApp::http_status_no_content = "204 No Content";
const std::string ExtendedApp::http_status_not_allowed = "403 Not Allowed";
const std::string ExtendedApp::http_status_internal_error =
    "500 Internal Server Error";

ExtendedApp::ExtendedApp(uWS::App &app) : app(app) {}

void ExtendedApp::use(Handler service) { middleware_list.push_back(service); }

ExtendedApp::Handler
ExtendedApp::handle_preflight_request(const std::string pattern) {

  return [&, pattern = pattern](auto res, auto req) {
    auto it = pattern_map.find(pattern);
    std::string methods = "";
    if (it != pattern_map.end()) {
      for (auto &m : it->second) {
        methods += Utils::to_upper(m) + ", ";
      }
      methods += "OPTIONS";
    }
    res->set_header("Access-Control-Allow-Origin",
                    std::string(req->get_header("origin")));
    res->set_header("Vary", "Origin");
    res->set_header("Access-Control-Allow-Methods", methods);
    res->set_header("Access-Control-Allow-Headers",
                    "content-type, authorization");
    res->set_header("Access-Control-Max-Age", "86400");
    res->set_status(ExtendedApp::http_status_no_content);
  };
};

ExtendedApp::Handler ExtendedApp::allow_cors_header() {
  return [](auto res, auto req) {
    res->set_header("Access-Control-Allow-Origin",
                    std::string(req->get_header("origin")));
  };
}

void ExtendedApp::set_cors_enabled(bool value) { cors_enabled = value; }

ExtendedApp::HandlerItem &ExtendedApp::use(const std::string method,
                                           const std::string pattern) {

  // for cors
  if (cors_enabled && method != "options") {
    auto p = pattern_map.find(pattern);
    if (p == pattern_map.end()) {
      // create options request
      use("options", pattern) << handle_preflight_request(pattern);
    }
    pattern_map[pattern].insert(method);
  }

  const std::string method_pattern = method + ":" + pattern;
  auto it = handler_map.find(method_pattern);
  if (it == handler_map.end()) {
    pattern_list.push_back(method_pattern);
    auto item = std::make_shared<HandlerItem>(method_pattern);
    handler_map[method_pattern] = item;
    return *item;
  }

  return *handler_map[method_pattern];
}

ExtendedApp::HandlerItem &ExtendedApp::get(const std::string pattern) {
  return use("get", pattern);
};

ExtendedApp::HandlerItem &ExtendedApp::post(const std::string pattern) {
  return use("post", pattern);
}

ExtendedApp::HandlerItem &ExtendedApp::del(const std::string pattern) {
  return use("delete", pattern);
}

ExtendedApp::HandlerItem &ExtendedApp::put(const std::string pattern) {
  return use("put", pattern);
}

ExtendedApp::HandlerItem &ExtendedApp::options(const std::string pattern) {
  return use("options", pattern);
}

void ExtendedApp::ws(const std::string pattern,
                     uWS::App::WebSocketBehavior &behavior, std::string tags) {

  behavior.upgrade = [&](auto *res, auto *req, auto *webSocketContext) {
    /* Default handler upgrades to WebSocket */
    auto secWebSocketKey = req->getHeader("sec-websocket-key");
    auto secWebSocketProtocol = req->getHeader("sec-websocket-protocol");
    auto secWebSocketExtensions = req->getHeader("sec-websocket-extensions");

    const std::set<std::string> tag_set = {tags};

    auto xreq = std::make_shared<ExtendedRequest>(*req);
    auto xres = std::make_shared<ExtendedResponse>(res, tag_set);
    /* no middleware will be executed */

    for (auto mws : middleware_list) {
      mws(xres, xreq);
    }

    /* execute resource handler */

    if (xres->has_responded()) {
      spdlog::warn("{} : A middleware already responded.", xreq->get_url());
      return;
    }

    spdlog::debug("{} : Upgrading request to WebSocket Connection.",
                  xreq->get_url());

    if (xreq->get_user() == nullptr || xreq->get_user() == model::User::Noone) {
      xres->set_status(ExtendedApp::http_status_not_allowed);
      xres->apply();
      return;
    }

    res->upgrade(std::string(xreq->get_user()->get_id()), secWebSocketKey,
                 secWebSocketProtocol, secWebSocketExtensions,
                 (struct us_socket_context_t *)webSocketContext);

#if 0
        [&]() {

            /* Note: OpenSSL can be used here to speed this up somewhat */
            char secWebSocketAccept[29] = {};
            uWS::WebSocketHandshake::generate(secWebSocketKey.data(), secWebSocketAccept);

            xres->set_status("101 Switching Protocols")
            .set_header("Upgrade", "websocket")
            .set_header("Connection", "Upgrade")
            .set_header("Sec-WebSocket-Accept", secWebSocketAccept);

            /* Select first subprotocol if present */
            if (secWebSocketProtocol.length()) {
                xres->set_header("Sec-WebSocket-Protocol", std::string(secWebSocketProtocol.substr(0, secWebSocketProtocol.find(','))));
            }

            xres->apply();

        });

#endif
  };

  app.ws<const std::string>(pattern, std::move(behavior));
}

void ExtendedApp::handle_request(std::shared_ptr<HandlerItem> handler,
                                 uWS::HttpResponse<false> *res,
                                 uWS::HttpRequest *req) {

  /* get body data */

  spdlog::debug("{} {} : Handling request.", req->getMethod(), req->getUrl());

  std::string *body_string_data = new std::string();

  auto xreq = std::make_shared<ExtendedRequest>(*req);
  auto xres = std::make_shared<ExtendedResponse>(res, handler->get_tags());

  res->onAborted([body_string_data]() {});

  res->onData([&, handler, body_string_data, xreq, xres](std::string_view data,
                                                         bool last) mutable {
    (*body_string_data) += std::string(data);
    if (last) {

      xreq->set_body(*body_string_data);
      xres->set_request(xreq);

      delete body_string_data;

      try {
        for (auto mws : middleware_list) {
          mws(xres, xreq);
        }

        spdlog::debug("{}:{} - Request has a total of {} handler.",
                      xreq->get_method(), xreq->get_url(),
                      handler->get_list().size());
        for (auto h : handler->get_list()) {
          h(xres, xreq);

          /* if (xres->has_responded()) {
              break;
          } */
        }

      } catch (basic_http_error &e) {
        if (e.get_reason().length() != 0) {
          xres->set_header("content-type", "application/json; charset=utf-8");
          xres->set_data("{\"reason\":\"" + e.get_reason() + "\"}");
        }
        xres->set_status(e.get_status());
      } catch (const std::exception &e) {
        spdlog::error("{}:{} - Unknown Error {}.", xreq->get_method(),
                      xreq->get_url(), e.what());
        xres->set_status(http_status_internal_error);
      }

      if (xreq->get_method() != "options" && cors_enabled) {
        allow_cors_header()(xres, xreq);
      }

      Job j{xreq, xres};
      jobs.push(j);
    }
  });
}

void ExtendedApp::listen(const std::string &bind, unsigned int port) {

  for (auto item_pattern : pattern_list) {

    auto item = handler_map.find(item_pattern);

    std::string pattern = "";
    std::string method = "";

    if (item_pattern.rfind("get", 0) == 0) {
      pattern = item_pattern.substr(4);
      method = "get";
    } else if (item_pattern.rfind("post", 0) == 0) {
      pattern = item_pattern.substr(5);
      method = "post";
    } else if (item_pattern.rfind("put", 0) == 0) {
      pattern = item_pattern.substr(4);
      method = "put";
    } else if (item_pattern.rfind("delete", 0) == 0) {
      pattern = item_pattern.substr(7);
      method = "delete";
    } else if (item_pattern.rfind("options", 0) == 0) {
      pattern = item_pattern.substr(8);
      method = "options";
    } else {
      spdlog::warn("No matching method found in pattern {}", item_pattern);
      continue;
    }

    auto fnct = std::bind(&ExtendedApp::handle_request, this, item->second,
                          std::placeholders::_1, std::placeholders::_2);
    spdlog::debug("Setting handler for {} : {}.", method, pattern);

    if (method == "get") {
      app.get(pattern, fnct);
    } else if (method == "post") {
      app.post(pattern, fnct);
    } else if (method == "put") {
      app.put(pattern, fnct);
    } else if (method == "delete") {
      app.del(pattern, fnct);
    } else if (method == "options") {
      app.options(pattern, fnct);
    }
  }

  app.listen(bind, port,
             [&](auto *token) {
               listen_socket = token;
               if (token) {
                 spdlog::debug("Bound to {}:{}.", bind, port);
               }

               uWS::Loop::get()->addPostHandler(nullptr, [&](auto *) {
                 unsigned int cntr = jobs.size();

                 while (cntr-- > 0) {

                   Job j = jobs.front();
                   jobs.pop();

                   if (j.res->is_waiting()) {
                     jobs.push(j);
                     continue;
                   }

                   if (!j.res->has_responded()) {
                     spdlog::error("{} : No response issued.",
                                   j.req->get_url());
                     j.res->set_status("500 Internal Server Error");
                   }

                   j.res->apply();
                 }

                 // std::cout << "Still " << jobs.size() << " jobs in queue." <<
                 // std::endl;
               });
             })
      .run();
}

void ExtendedApp::close() { us_listen_socket_close(0, listen_socket); }
