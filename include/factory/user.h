#ifndef __AUTOMATION_HTTP_FACTORY_USER_H__
#define __AUTOMATION_HTTP_FACTORY_USER_H__

#include "string"
#include "set"
#include "fstream"

#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#include "event-emitter.h"
#include "factory/yaml-file.h"
#include "models/user.h"

#define DEFAULT_GUEST_PASSWORD "84983c60f7daadc1cb8698621f802c0d9f9a3c3c295c810748fb048115c186ec" // guest
#define DEFAULT_NOBODY_PASSWORD "0000000000000000000000000000000000000000000000000000000000000000"

namespace automation::http::factory {

class User : public YamlFile<model::User> {

  public:

    class username_taken_error : public std::exception { };
    class verification_error : public std::exception { };

  public:

    User(EventEmitter&, const std::string& filename);
    ~User();

    std::shared_ptr<automation::http::model::User> verify(const std::string& username, const std::string& password);
    std::shared_ptr<automation::http::model::User> add(automation::http::model::User& user) override;
    std::shared_ptr<automation::http::model::User> update(const std::string&, automation::http::model::User&) override;
    std::shared_ptr<automation::http::model::User> remove(const std::string&) override;
    std::shared_ptr<model::User> get_user_by_username(const std::string&);

    std::shared_ptr<model::User> initiate_admin(const std::string&);
    std::shared_ptr<model::User> initiate_guest();
    std::shared_ptr<model::User> initiate_nobody();

  private:

    static const std::string hash_password(const std::string&);

    std::map<std::string, std::shared_ptr<model::User>> username_map;
    std::shared_ptr<model::User> admin_user;
    std::shared_ptr<model::User> guest_user;
    std::shared_ptr<model::User> nobody_user;

    void deserialize () override;

    void group_delete_handler(std::string);

    YAML::Node to_yaml(const automation::http::model::User&) const;
    automation::http::model::User from_yaml(YAML::Node) const;

    bool filter(const automation::http::model::User&, const std::string&) const;
    int compare(const std::string&, const automation::http::model::User&, const automation::http::model::User&) const;

    std::string create_sha256_hex_hash(const std::string);

};

}


#endif
