#include "http/service/processdata.h"

using namespace automation::http::service;

ProcessData::ProcessData(const std::string& base_uri, automation::http::Stream& stream) :
    base_uri(base_uri),
    json_read_builder(),
    json_write_builder(),
    json_reader(json_read_builder.newCharReader()),
    stream(stream) {

    json_write_builder["indentation"] = "";

}

automation::http::ExtendedApp::Handler ProcessData::update_data_by_key() {

    return [&](auto& res, auto& req) {

        std::string key = std::string(req.get_parameter(0));
        spdlog::debug("{}:{} : Url Key = {}", req.get_method(), req.get_url(), key);

        Json::Value value = req.get_json_body();

        if (!value || !value["value"]) {
            throw automation::http::ExtendedApp::bad_request_error();
        }

        try {

            stream.set_new_state(key, value["value"].asString());
            res.set_status(ExtendedApp::http_status_ok);

        } catch (automation::http::IoTask::deserialize_error& e) {
            throw automation::http::ExtendedApp::bad_request_error();
        } catch (automation::http::Stream::unknown_key& e) {
            throw automation::http::ExtendedApp::not_found_error();
        } catch (automation::http::Stream::format_error& e) {
            throw automation::http::ExtendedApp::bad_request_error();
        }

        stream.submit();


    };


}

automation::http::ExtendedApp::Handler ProcessData::update_multiple_data() {

    return [&](auto& res, auto& req) {

        spdlog::debug("POST /api/1/io");

        Json::Value value = req.get_json_body();

        if (!value || !value.isArray()) {
            throw automation::http::ExtendedApp::bad_request_error();
        }

        for (auto v : value) {

            if (!v || !v.isObject()) {
                throw automation::http::ExtendedApp::bad_request_error();
            }

            if (!v["value"] || !v["value"].isString()) {
                throw automation::http::ExtendedApp::bad_request_error();
            }

            if (!v["key"] || !v["key"].isString()) {
                throw automation::http::ExtendedApp::bad_request_error();
            }

            const std::string str_key = v["key"].asString();
            const std::string str_value = v["value"].asString();

            try {

                stream.set_new_state(str_key, str_value);
                res.set_status(ExtendedApp::http_status_ok);

            } catch (automation::http::IoTask::deserialize_error& e) {
                spdlog::debug("POST /api/1/io deserialization error.");
                throw ExtendedApp::bad_request_error();
            } catch (automation::http::Stream::unknown_key& e) {
                spdlog::debug("POST /api/1/io unknown key.");
                throw ExtendedApp::not_found_error();
            } catch (automation::http::Stream::format_error& e) {
                spdlog::debug("POST /api/1/io format error.");
                throw ExtendedApp::bad_request_error();
            }
        }

        stream.submit();

    };

};

Json::Value ProcessData::create_update(Stream::Update& update) {

    Json::Value json;
    JSONCPP_STRING err;
    std::string json_string = "{\"key\":\"" + update.key + "\",\"value\":" + update.value + ",\"access\":\"" + update.access + "\"}";
    json_reader->parse(json_string.c_str(), json_string.c_str() + json_string.length(), &json, &err);

    return json;

}

Json::Value ProcessData::create_update_array(std::vector<Stream::Update>& update_list) {

    Json::Value root;

    for (auto u : update_list) {
        root.append(create_update(u));
    }

    return root;

}
