#ifndef __DEPOT_AUTOMATION_HTTP_LOG_MIDDLEWARE_H__
#define __DEPOT_AUTOMATION_HTTP_LOG_MIDDLEWARE_H__

#include "xapp.h"
#include "spdlog/spdlog.h"

namespace automation::http::middleware {

class Logger {

  public:

    operator automation::http::ExtendedApp::Handler() {
        return [](auto, auto req) {
            spdlog::debug("{}:{} : New Request coming in.", req->get_url(), req->get_method());
        };
    }

};

}

#endif
