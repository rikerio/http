#ifndef __AUTOMATION_IO_TASK_READ_JOB_H__
#define __AUTOMATION_IO_TASK_READ_JOB_H__

#include "stdlib.h"
#include "string.h"
#include "string"
#include "atomic"
#include "functional"

namespace automation::http {

class ReadJob {
  public:

    using Callback = std::function<void(char*)>;

    ReadJob(char* ptr, unsigned int size, Callback callback) :
        byte_size(size),
        callback(callback),
        memory_ptr(ptr),
        buffer(nullptr) {
        buffer = (char*) calloc(1, byte_size);
    }

    ~ReadJob() {
        free(buffer);
    }

    void read() {

        if (byte_size == 0) {
            return;
        }

        memcpy(buffer, memory_ptr, byte_size);

    }

    const char* get_buffer () const {
        return buffer;
    }

    unsigned int get_byte_size() const {
        return byte_size;
    }

    void resolve() {

        if (!callback) {
            return;
        }

        callback(buffer);

    }

  private:

    unsigned int byte_size;

    Callback callback;

    char* memory_ptr;
    char* buffer;

};

}

#endif
