#include "factory/session.h"
#include "utils.h"

using namespace automation::http;

factory::Session::Session(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::Session>(emitter, filename, "session") {

    deserialize();

    std::function<void(const std::string&)> user_removed_handler = [&](const std::string& id) {

        std::set<std::string> remove_ids;
        for (auto it : t_map) {
            try {
                if (it.second->get_user_id() == id) {
                    remove_ids.insert(it.first);
                }
            } catch (...) {

            }
        }

        for (auto& k : remove_ids) {
            t_map.erase(k);
        }


    };

    emitter.on<const std::string&>("user.removed", user_removed_handler);

}

factory::Session::~Session() {
    serialize();
}

YAML::Node factory::Session::to_yaml(const model::Session& rhs) const {
    YAML::Node node;

    auto expires_sec = std::chrono::time_point_cast<std::chrono::seconds>(rhs.get_expires());
    auto epoch = expires_sec.time_since_epoch();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(epoch);


    node["expires"] = duration.count();
    node["path"] = rhs.get_path();
    node["id"] = rhs.get_id();
    node["user_agent"] = rhs.get_user_agent();

    auto values = node["store"];

    for (auto v : rhs.get_key_value_map()) {
        values[v.first] = v.second;
    }

    return node;
}

model::Session factory::Session::from_yaml(YAML::Node node) const {

    model::Session session;

    long int expires_sec = node["expires"].as<long int>();
    auto expires_duration = std::chrono::duration<long int>(expires_sec);
    std::chrono::system_clock::time_point expires_tp(expires_duration);

    session.set_id(node["id"].as<std::string>());
    session.set_path(node["path"].as<std::string>());

    auto node_user_agent = node["user_agent"];

    if (node_user_agent && node_user_agent.Type() == YAML::NodeType::Scalar) {
        session.set_user_agent(node_user_agent.as<std::string>());
    }

    session.set_expires(expires_tp);

    YAML::Node store = node["store"];

    if (store && store.Type() == YAML::NodeType::Map) {
        for (auto key_value : store) {
            session.set_value(key_value.first.as<std::string>(), key_value.second.as<std::string>());
        }
    }

    return session;

}

bool factory::Session::filter(const model::Session& session, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != session.get_id().find(value)) {
        return true;
    }

    auto it = session.get_key_value_map().find("user_id");

    if (it != session.get_key_value_map().end()) {
        if (std::string::npos != it->second.find(value)) {
            return true;
        }
    }

    return false;

}

int factory::Session::compare(const std::string& field, const model::Session& a, const model::Session& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "user") {
        try {
            auto user_id_a = a.get_user_id();
            auto user_id_b = b.get_user_id();
            return user_id_a.compare(user_id_b);
        } catch (...) {
            return -1;
        }
    }

    if (field == "age") {
        if (a.get_expires() > b.get_expires()) {
            return 1;
        } else if (a.get_expires() < b.get_expires()) {
            return -1;
        } else {
            return 0;
        }
    }

    return 0;

}
