#ifndef __AUTOMATION_HTTP_MODEL_KEYFACTORY_H__
#define __AUTOMATION_HTTP_MODEL_KEYFACTORY_H__

#include "string"
#include "set"
#include "fstream"

#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#include "event-emitter.h"
#include "factory/yaml-file.h"
#include "models/key.h"

namespace automation::http::factory {

class Key : public YamlFile<model::Key> {

  public:

    class verification_error : public std::exception { };

  public:

    Key(EventEmitter& emitter, const std::string& filename);
    ~Key();

  private:

    YAML::Node to_yaml(const model::Key&) const;
    model::Key from_yaml(YAML::Node) const;
    bool filter(const model::Key&, const std::string&) const;
    int compare(const std::string&, const model::Key&, const model::Key&) const;

    const std::string provide_unique_id() const {
        throw not_implemented_error();
    }


};

}

#endif
