#ifndef __AUTOMATION_HTTP_CONFIG_H__
#define __AUTOMATION_HTTP_CONFIG_H__

#include "string"
#include "yaml-cpp/yaml.h"

namespace automation::http {

struct Config {

    std::string bind;
    unsigned int port;
    bool cors_enabled;

    struct {
        std::string profile;
    } rikerio;

    struct {
        std::string webapp;
        std::string db;
        std::string files;
        std::string apps;
    } folder;

    size_t max_file_size;

    struct {
        unsigned int short_cycle;
        unsigned int long_cycle;
        unsigned int priority;
        unsigned int buffer_size;
        unsigned int requests_per_cycle;
    } io;


    static const Config deserialize(const std::string&);

};

}

#endif
