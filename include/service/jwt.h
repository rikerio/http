#ifndef __AUTOMATION_HTTP_SERVICE_JWT_H__
#define __AUTOMATION_HTTP_SERVICE_JWT_H__

#include "factory/jwt.h"
#include "factory/user.h"
#include "xapp.h"
#include "config.h"


namespace automation::http::service {

class JWT {

  public:

    JWT(factory::User& user_factory, factory::JWT& jwt_factory);
    operator automation::http::ExtendedApp::Handler();

  private:
    factory::User& user_factory;
    factory::JWT& jwt_factory;

};

}

#endif
