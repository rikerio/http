#ifndef __AUTOMATION_HTTP_SERVICE_VERSION_H__
#define __AUTOMATION_HTTP_SERVICE_VERSION_H__

#include "xapp.h"
#include "common.h"
#include "spdlog/spdlog.h"
#include "parser/json.h"

namespace automation::http::service {

class VersionService {

  public:

    static automation::http::ExtendedApp::Handler get() {
        return [](auto res, auto req) {

            Json::Value root;
            Json::Value version;

            version["major"] = automation::http::version.get_major();
            version["minor"] = automation::http::version.get_minor();
            version["patch"] = automation::http::version.get_patch();

            root["version"] = version;

            res->set_header("Access-Control-Allow-Origin", std::string(req->get_header("origin")));
            res->set_header("Content-Type", "application/json; charset=utf-8");
            res->set_data(automation::http::parser::json::stringify(root));
            res->set_status(ExtendedApp::http_status_ok);

        };

    }
};
}

#endif
