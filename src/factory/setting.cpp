#include "factory/setting.h"
#include "xapp.h"
#include "regex"

using namespace automation::http;

factory::Setting::Setting(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::Setting>(emitter, filename, "setting") {
    deserialize();
}

factory::Setting::~Setting() {
    serialize();
}

std::shared_ptr<automation::http::model::Setting> factory::Setting::add(automation::http::model::Setting& model) {

    const static std::regex id_regex("^[[:alnum:]|\\.\\-\\_\\$]+$", std::regex::ECMAScript);
    std::smatch match;

    std::regex_match(model.get_id(), match, id_regex);

    if (match.size() != 1) {
        throw automation::http::ExtendedApp::bad_request_error("Id not valid.");
    }

    if (!match[0].matched) {
        throw automation::http::ExtendedApp::bad_request_error("ID not valid.");
    }

    return automation::http::factory::YamlFile<automation::http::model::Setting>::add(model);

}

YAML::Node factory::Setting::to_yaml(const model::Setting& setting) const {

    YAML::Node node;

    node["id"] = setting.get_id();
    node["value"] = setting.get_value();
    node["type"] = setting.get_type();

    return node;

}

model::Setting factory::Setting::from_yaml(YAML::Node node) const {

    auto yaml_id = node["id"];
    auto yaml_value = node["value"];
    auto yaml_type = node["type"];

    model::Setting setting;

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        setting.set_id(yaml_id.as<std::string>());
    }

    if (yaml_value && yaml_value.Type() == YAML::NodeType::Scalar) {
        setting.set_value(yaml_value.as<std::string>());
    }

    if (yaml_type && yaml_type.Type() == YAML::NodeType::Scalar) {
        setting.set_type(yaml_type.as<std::string>());
    }

    return setting;

}

bool factory::Setting::filter(const model::Setting& setting, const std::string& str) const {

    if (str == "") {
        return true;
    }

    if (std::string::npos != setting.get_id().find(str)) {
        return true;
    }

    if (setting.get_type() == "string" && std::string::npos != setting.get_value().find(str)) {
        return true;
    }

    if (std::string::npos != setting.get_type().find(str)) {
        return true;
    }

    return false;

}

int factory::Setting::compare(const std::string& field, const model::Setting& a, const model::Setting& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "value") {
        return a.get_value().compare(b.get_value());
    }

    if (field == "type") {
        return a.get_type().compare(b.get_type());
    }

    return 0;

}
