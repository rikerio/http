#ifndef __AUTOMATION_HTTP_IPC_MESSAGE_H__
#define __AUTOMATION_HTTP_IPC_MESSAGE_H__

#include "string"
#include "string.h"

namespace automation::http {

using ValueBuffer = char[8];

class IPCJob {
  public:


    IPCJob() : id() { }
    IPCJob(
        std::string_view id,
        unsigned int byte_size,
        unsigned int page,
        unsigned int page_count,
        const char* ptr) : id(id), page(page), page_count(page_count) {
        memcpy(buffer, ptr, byte_size);
    }

    std::string_view get_id() const {
        return id;
    }

    unsigned int get_page() const {
        return page;
    }

    unsigned int get_page_count() const {
        return page_count;
    }

    const char* get_buffer() const {
        return buffer;
    }

  private:

    std::string_view id;
    unsigned int page;
    unsigned int page_count;
    ValueBuffer buffer;

};

};

#endif
