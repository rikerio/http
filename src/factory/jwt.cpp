#include "factory/jwt.h"
#include "jwt.h"

using namespace automation::http::factory;

JWT::JWT(
    factory::User& user_factory,
    std::string secret) :
    user_factory(user_factory),
    secret(secret) { };

const std::string JWT::create_token(const model::User& user) {

    jwt_t *jwt = nullptr;
    jwt_alg_t opt_alg = JWT_ALG_HS256;

    int ret = jwt_new(&jwt);

    unsigned char secr[secret.length()];
    memset(secr, 0, secret.length());
    memcpy(secr, secret.c_str(), secret.length());

    ret = jwt_set_alg(jwt, opt_alg, secr, secret.length());
    ret = jwt_add_grant(jwt, "uid", user.get_id().c_str());

    // TODO: check ret
    (void)(ret);

    char* jwt_str_dump = jwt_encode_str(jwt);

    std::string token(jwt_str_dump);

    free(jwt_str_dump);
    jwt_free(jwt);

    /* auto token = jwt::create()
                 .set_type("JWS")
                 .set_payload_claim("uid", jwt::claim(user.get_id()))
                 .sign(jwt::algorithm::hs256{secret}); */

    return token;

}

const std::string JWT::decode_token(std::string& token) {

    jwt_t *jwt = nullptr;

    unsigned char secr[secret.length() + 1];
    memset(secr, 0, secret.length() + 1);
    memcpy(secr, secret.c_str(), secret.length());

    int res = jwt_decode(&jwt, token.c_str(), secr, secret.length());

    if (res != 0) {
        std::cout << "Error decoding jwt: " <<  errno << " " << strerror(errno) << std::endl;
        throw decoding_error();
    }

    const char* uid = jwt_get_grant(jwt, "uid");

    if (uid == nullptr) {
        throw decoding_error();
    }

    return std::string(uid);

}

