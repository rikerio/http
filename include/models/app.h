#ifndef __AUTOMATION_HTTP_MODEL_APP_H__
#define __AUTOMATION_HTTP_MODEL_APP_H__

#include "string"
#include "utils.h"

namespace automation::http::model {

class App {

  public:

    App() :
        id(""),
        name(""),
        description(""),
        version(""),
        author_name(""),
        author_email(""),
        disabled(false) {
    }

    const std::string& get_id() const {
        return id;
    }

    App& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    App& set_id(std::string_view& view) {
        id = std::string(view);
        return *this;
    }

    const std::string& get_name() const {
        return name;
    }

    App& set_name(const std::string& value) {
        name = value;
        return *this;
    }

    App& set_name(std::string_view view) {
        name = std::string(view);
        return *this;
    }

    const std::string& get_description() const {
        return description;
    }

    App& set_description(const std::string& value) {
        description = value;
        return *this;
    }

    App& set_description(std::string_view view) {
        description = std::string(view);
        return *this;
    }

    const std::string& get_version() const {
        return version;
    }

    App& set_version(const std::string& value) {
        version = value;
        return *this;
    }

    App& set_version(const std::string_view view) {
        version = view;
        return *this;
    }

    const std::string& get_author_name() const {
        return author_name;
    }

    App& set_author_name(const std::string& value) {
        author_name = value;
        return *this;
    }

    App& set_author_name(const std::string_view view) {
        author_name = view;
        return *this;
    }

    const std::string& get_author_email() const {
        return author_email;
    }

    App& set_author_email(const std::string& value) {
        author_email = value;
        return *this;
    }

    App& set_author_email(const std::string_view view) {
        author_email = view;
        return *this;
    }

    bool is_disabled() const {
        return disabled;
    }

    App& set_disabled(bool value) {
        disabled = value;
        return *this;
    }

  private:

    std::string id;
    std::string name;
    std::string description;
    std::string version;
    std::string author_name;
    std::string author_email;
    bool disabled;

};

}


#endif
