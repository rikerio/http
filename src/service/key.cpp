#include "service/key.h"

using namespace automation::http;

service::Key::Key(const std::string& base_uri, factory::Key& factory) :
    FactoryService<factory::Key, model::Key>(base_uri, factory) { }

Json::Value service::Key::to_json(const model::Key& key) const {

    Json::Value result = Json::objectValue;
    result["id"] = key.get_id();
    result["userid"] = key.get_user_id();
    result["disabled"] = key.is_disabled();
    return result;

}

model::Key service::Key::from_json(Json::Value body, const std::string&) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_user_id = body["userid"];
    auto json_disabled = body["disabled"];

    if (!json_id || !json_id.isString()) {
        spdlog::debug("No id present");
        throw ExtendedApp::bad_request_error("Missing id in request body.");
    }

    if (!json_user_id || !json_user_id.isString()) {
        spdlog::debug("No username present");
        throw ExtendedApp::bad_request_error("Missing user id in request body.");
    }

    if (!json_disabled || !json_disabled.isBool()) {
        spdlog::debug("No disabled present.");
        throw ExtendedApp::bad_request_error("Missing disabled state in request body.");
    }


    const std::string id = json_id.as<std::string>();
    const std::string user_id = json_user_id.as<std::string>();
    const bool disabled = json_disabled.as<bool>();

    if (user_id.length() < 3) {
        spdlog::debug("User Id length < 3.");
        throw ExtendedApp::bad_request_error("User Id to short.");
    }

    model::Key key;

    key
    .set_id(id)
    .set_user_id(user_id)
    .set_disabled(disabled);

    return key;

}
