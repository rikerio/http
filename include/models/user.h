#ifndef __AUTOMATION_HTTP_SERVICE_USER_H__
#define __AUTOMATION_HTTP_SERVICE_USER_H__

#include "string"
#include "set"
#include "memory"
#include "utils.h"
#include "models/model.h"

namespace automation::http::model {

class User : public automation::http::model::Base {

  public:

    static std::shared_ptr<User> Noone;

    User() :
        id(""),
        username(""),
        name(""),
        password(""),
        disabled(false) { }


    User& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    const std::string& get_id() const {
        return id;
    }

    User& set_username(const std::string& value) {
        username = Utils::to_lower(value);
        return *this;
    }

    const std::string& get_username() const {
        return username;
    }

    User& set_name(const std::string& value) {
        name = value;
        return *this;
    }

    const std::string& get_name() const {
        return name;
    }

    User& set_password(const std::string& value) {
        password = value;
        return *this;
    }

    const std::string& get_password() const {
        return password;
    }

    User& set_disabled(bool value) {
        disabled = value;
        return *this;
    }

    bool is_disabled() const {
        return disabled;
    }

  private:

    std::string id;
    std::string username;
    std::string name;
    std::string password;
    bool disabled;

};

}

#endif
