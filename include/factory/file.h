#ifndef __AUTOMATION_HTTP_FACTORY_FILE_H__
#define __AUTOMATION_HTTP_FACTORY_FILE_H__

#include "factory/yaml-file.h"
#include "event-emitter.h"
#include "models/file.h"

namespace automation::http::factory {

class File : public YamlFile<model::File> {

  public:

    File(EventEmitter&, const std::string&);
    ~File();

  private:

    YAML::Node to_yaml(const model::File&) const;
    model::File from_yaml(YAML::Node) const;

    bool filter(const model::File&, const std::string&) const;
    int compare(const std::string&, const model::File&, const model::File&) const;

};


}


#endif
