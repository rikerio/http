#include "service/jwt.h"

using namespace automation::http::service;

JWT::JWT(
    automation::http::factory::User& user_factory,
    automation::http::factory::JWT& jwt_factory) :
    user_factory(user_factory),
    jwt_factory(jwt_factory) {

};


JWT::operator automation::http::ExtendedApp::Handler() {

    return [&](auto, auto req) {

        static const std::string _bearer = "Bearer ";
        std::string uid = "";

        auto auth_header = req->get_header("authorization");
        auto auth_query = req->get_query("jwt", "");

        if (auth_header.length() != 0) {

            spdlog::debug("{}:{} : JWT Token present in Authorization Header.", req->get_method(), req->get_url());
            spdlog::debug("{}:{} : Authorization Header = {}.", req->get_method(), req->get_url(), auth_header);
            std::string token(auth_header.substr(_bearer.length()));
            uid = jwt_factory.decode_token(token);

        } else if (auth_query != "") {

            spdlog::debug("{}:{} : JWT Token Present in requests query ({}).", req->get_method(), req->get_url(), auth_query);
            try {
                uid = jwt_factory.decode_token(auth_query);

            } catch (...) {
                spdlog::error("{}:{} - Error decoding token '{}'.", req->get_method(), req->get_url(), auth_query);
                req->set_user(user_factory.get_user_by_username("nobody"));
                return;
            }


        } else {

            spdlog::debug("{}:{} : No JWT Token Present in request.", req->get_method(), req->get_url(), auth_header);
            req->set_user(user_factory.get_user_by_username("nobody"));
            return;

        }

        spdlog::debug("{}:{} : User ID embedded in JWT Token = {}.", req->get_method(), req->get_url(), uid);

        try {
            auto user = user_factory.get_one(uid);
            if (user->is_disabled()) {
                spdlog::debug("{}:{} : User found, but is disabled.", req->get_method(), req->get_url());
                return;
            }
            req->set_user(user);
        } catch (automation::http::model::Session::no_such_key_error& e) {
            spdlog::debug("{}:{} : No user id found in session.", req->get_method(), req->get_url());
            req->set_user(user_factory.get_user_by_username("nobody"));
        } catch (...) {
            spdlog::error("{}:{} : Something else occured.", req->get_method(), req->get_url());
            req->set_user(user_factory.get_user_by_username("nobody"));
        }


    };

}

