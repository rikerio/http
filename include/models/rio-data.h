#ifndef __AUTOMATION_HTTP_MODEL_RIO_DATA_H__
#define __AUTOMATION_HTTP_MODEL_RIO_DATA_H__

#include "rikerio-automation/automation.h"

namespace automation::http::model {

class RioData {

  public:
    static std::string type_to_string(RikerIO::Type type) {

        switch (type) {
        case (RikerIO::Type::BIT):
            return "bit";
        case (RikerIO::Type::BOOL):
            return "bool";
        case (RikerIO::Type::UINT8):
            return "uint8";
        case (RikerIO::Type::INT8):
            return "int8";
        case (RikerIO::Type::UINT16):
            return "uint16";
        case (RikerIO::Type::INT16):
            return "int16";
        case (RikerIO::Type::UINT32):
            return "uint32";
        case (RikerIO::Type::INT32):
            return "int32";
        case (RikerIO::Type::UINT64):
            return "uint64";
        case (RikerIO::Type::INT64):
            return "int64";
        case (RikerIO::Type::FLOAT):
            return "float";
        case (RikerIO::Type::DOUBLE):
            return "double";
        case (RikerIO::Type::STRING):
            return "string";
        case (RikerIO::Type::UNDEF):
            return "binary";
        }

        return "binary";

    }

  public:
    RioData() : id(""), type(""), bit_size(0), byte_offset(0), bit_index(0) { }
    RioData(RikerIO::DataPoint& a) :
        id(std::string(a.id)),
        type(type_to_string(a.type)),
        bit_size(a.bit_size),
        byte_offset(a.byte_offset),
        bit_index(a.bit_index) { }

    const std::string& get_id() const {
        return id;
    }

    RioData& set_id(const std::string value) {
        id = value;
        return *this;
    }

    const std::string& get_type() const {
        return type;
    }

    RioData& set_type(const std::string& value) {
        type = value;
        return *this;
    }

    unsigned int get_byte_offset() const {
        return byte_offset;
    }

    RioData& set_byte_offset(unsigned int value) {
        byte_offset = value;
        return *this;
    }

    unsigned int get_bit_index() const {
        return bit_index;
    }

    RioData& set_bit_index(unsigned int value) {
        bit_index = value;
        return *this;
    }

    unsigned int get_bit_size() const {
        return bit_size;
    }

    RioData& set_bit_size(unsigned int value) {
        bit_size = value;
        return *this;
    }

  private:

    std::string id;
    std::string type;
    unsigned int bit_size;
    unsigned int byte_offset;
    unsigned int bit_index;

};

}

#endif
