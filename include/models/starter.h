#ifndef __AUTOMATION_HTTP_MODEL_STARTER_H__
#define __AUTOMATION_HTTP_MODEL_STARTER_H__

#include "string"
#include "map"
#include "utils.h"

namespace automation::http::model {

class Starter {

  public:

    using ParameterMap = std::map<std::string, std::string>;

    Starter():
        id(""),
        app_id(""),
        label(""),
        description(""),
	disabled(false) {
    }

    const std::string& get_id() const {
        return id;
    }

    Starter& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    const std::string& get_app_id() const {
        return app_id;
    }

    Starter& set_app_id(const std::string& value) {
        app_id = value;
        return *this;
    }

    const std::string& get_label() const {
        return label;
    }

    Starter& set_label(const std::string& value) {
        label = value;
        return *this;
    }

    const std::string& get_description() const {
        return description;
    }

    Starter& set_description(const std::string& value) {
        description = value;
        return *this;
    }

    const ParameterMap& get_parameter_map() const {
        return parameter_map;
    }

    Starter& add_parameter(const std::string& key, const std::string& value) {
        parameter_map[key] = value;
        return *this;
    }

    Starter& clear_parameter() {
        parameter_map.clear();
        return *this;
    }

    Starter& set_disabled(bool value) {
	disabled = value;
	return *this;
    }

    bool is_disabled() const {
	return disabled;
    }

  private:

    std::string id;
    std::string app_id;
    std::string label;
    std::string description;
    ParameterMap parameter_map;
    bool disabled;

};

}


#endif
