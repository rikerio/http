#ifndef __AUTOMATION_HTTP_WEBSOCKET_SERVICE_H__
#define __AUTOMATION_HTTP_WEBSOCKET_SERVICE_H__

#include "xapp.h"

namespace automation::http::service {

class WebSocket {

  public:

    static void send_error_response(uWS::WebSocket<false, true>*, Json::Value, std::string_view);
    static void send_success_response(uWS::WebSocket<false, true>*, Json::Value, Json::Value);

    static void send_json(uWS::WebSocket<false, true>*, Json::Value);

  public:

    class Service {
      public:
        virtual ~Service() { };
        virtual void on_connected(uWS::WebSocket<false, true>*) = 0;
        virtual void on_close(uWS::WebSocket<false, true>*) = 0;
        virtual void on_data(uWS::WebSocket<false, true>*, Json::Value) = 0;
    };

  public:

    WebSocket();

    operator uWS::App::WebSocketBehavior& ();

    void register_service(const std::string&, Service&);
    void close_all();

  private:

    uWS::App::WebSocketBehavior behavior;

    Json::CharReaderBuilder json_read_builder;
    std::unique_ptr<Json::CharReader> json_reader;

  private:

    std::map<std::string, Service*> service_map;
    std::set<uWS::WebSocket<false, true>*> websocket_set;

};

}


#endif
