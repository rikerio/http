#ifndef __AUTOMATION_HTTP_RESPONSE_HANDLER_H__
#define __AUTOMATION_HTTP_RESPONSE_HANDLER_H__

#include "string"
#include "memory"
#include "functional"
#include "xrequest.h"
#include "xresponse.h"

namespace automation::http {

class MetaObject {
  public:
    MetaObject(const std::string& uri) : uri(uri) { };
    const std::string& get_uri() const {
        return uri;
    }
  private:
    const std::string uri;
};

class ResponseHandler {

  public:

    virtual ~ResponseHandler() { };

    virtual void set_response_body(ExtendedRequest&, ExtendedResponse&) const = 0;
    virtual void filter(std::function<bool(MetaObject&)> filter_handler) = 0;

};

}


#endif
