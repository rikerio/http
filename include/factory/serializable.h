#ifndef __RIKERIO_HTTP_SERIALIZABLE_H__
#define __RIKERIO_HTTP_SERIALIZABLE_H__

namespace automation::http {

class Serializable {

  public:

    virtual ~Serializable() {};
    virtual void serialize() const = 0;
    virtual void deserialize() = 0;
    virtual unsigned int get_change_count() const = 0;
    virtual void reset_change_count() = 0;


  private:

};

}

#endif
