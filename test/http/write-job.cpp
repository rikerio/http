#include "gtest/gtest.h" // googletest header file

#include "write-job.h"
#include "rikerio-automation/common/types.h"
#include <string>

using namespace automation;

TEST(Http_WriteJob, Uint8_Write) {

    uint8_t memory_value = 0;
    bool callback_called = false;

    automation::http::WriteJob job((char*) &memory_value, TypeInfos<uint8_t>::BitSize(), 0, [&]() {
        callback_called = true;
    });

    job.init<uint8_t>(123);
    job.write();
    job.resolve();

    ASSERT_EQ(memory_value, 123);
    ASSERT_TRUE(callback_called);

}

TEST(Http_WriteJob, Bool_Write_A) {

    uint8_t memory_value = 0;
    bool callback_called = false;

    automation::http::WriteJob job((char*) &memory_value, TypeInfos<bool>::BitSize(), 1, [&]() {
        callback_called = true;
    });

    job.init<bool>(true);
    job.write();
    job.resolve();

    ASSERT_EQ(memory_value, 2);
    ASSERT_TRUE(callback_called);

    callback_called = false;

    job.init<bool>(false);
    job.write();
    job.resolve();

    ASSERT_EQ(memory_value, 0);
    ASSERT_TRUE(callback_called);

}
