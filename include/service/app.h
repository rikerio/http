#ifndef __AUTOMATION_HTTP_SERVICE_APP_H__
#define __AUTOMATION_HTTP_SERVICE_APP_H__

#include "service/service.h"
#include "factory/app.h"
#include "factory/file.h"
#include "factory/user.h"
#include "xapp.h"
#include "zip.h"

namespace automation::http::service {

class App : public FactoryService<factory::App, model::App> {

  public:

    App(
        const std::string& base_uri,
        factory::App& factory,
        factory::File&,
        factory::User&,
        const std::string& app_folder,
        const std::string& file_folder);

    automation::http::ExtendedApp::Handler create() override;
    automation::http::ExtendedApp::Handler remove() override; 

  protected:

    model::App from_json(Json::Value, const std::string&) const;
    Json::Value to_json(const model::App&) const;

  private:

    struct CreateForm {
        std::string file_id;
        bool disabled;
    };

    CreateForm from_json(Json::Value) const;

    bool open_zip_file(::zip**, std::shared_ptr<model::File>);
    bool extract_app_yaml(::zip*, YAML::Node&);
    bool validate_app_yaml(YAML::Node&, model::App&, std::string&);
    bool extract_zip_file(::zip*, model::App&);
    bool close_zip_file(::zip*);

    void create_dir(const std::string& dir);

  private:

    factory::File& file_factory;
    factory::User& user_factory;

    const std::string app_folder;
    const std::string file_folder;

};

}

#endif
