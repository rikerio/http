#ifndef __AUTOMATION_HTTP_MULTIPART_FORM_DATA_H__
#define __AUTOMATION_HTTP_MULTIPART_FORM_DATA_H__

#include "utils.h"
#include "string"
#include "map"
#include "vector"

namespace automation::http::parser {

struct DispositionParam {
    std::string_view key;
    std::string_view value;
};

class ContentDisposition {
  public:

    ContentDisposition(std::string_view);

    std::string_view get_type() const;
    std::string_view get_header(std::string_view) const;
    bool has_header(std::string_view) const;

  private:

    std::string_view orig;
    std::string_view disposition_type;
    std::vector<DispositionParam> disposition_param_list;

};

struct Header {
    std::string_view key;
    std::string_view value;
};

class MultipartFormDataPart {

  public:

    MultipartFormDataPart(std::string_view);
    ~MultipartFormDataPart();

    std::string_view get_data() const;

    std::string_view get_header(std::string_view key) const;
    const ContentDisposition& get_content_disposition() const;

  private:

    enum class State {
        header_field_start,
        header_field,
        headers_almost_done,
        header_value_start,
        header_value,
        header_value_almost_done,
        part_data_start,
        part_data
    };

    static const char LF = 10;
    static const char CR = 13;

  private:

    ContentDisposition* content_disposition;
    std::vector<Header> header_list;
    std::string_view data;

};



class MultipartFormData {

  public:

  public:

    MultipartFormData(const std::string_view, const std::string_view);

    std::string_view get_value(std::string_view);

    const std::vector<MultipartFormDataPart>& get_data() const;

  private:

    const std::string_view content_type;
    const std::string_view body_content;

    std::string boundary_string;

    std::vector<MultipartFormDataPart> data_vector;

  private:

    void parse_body();

};

}

#endif
