#ifndef __AUTOMATION_HTTP_SERVICE_STARTER_H__
#define __AUTOMATION_HTTP_SERVICE_STARTER_H__

#include "service/service.h"
#include "service/static.h"
#include "factory/starter.h"

namespace automation::http::service {

class Starter : public FactoryService<factory::Starter, model::Starter> {

  public:

    Starter(const std::string&, factory::Starter&, factory::App&);

    automation::http::ExtendedApp::Handler serve_static(const std::string&, const std::string&);

  protected:

    model::Starter from_json(Json::Value, const::string&) const;
    Json::Value to_json(const model::Starter&) const;

  private:

    factory::App& app_factory; 

};


}


#endif
