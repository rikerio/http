#include "service/starter.h"
#include "service/static.h"

using namespace automation::http;

service::Starter::Starter(
    const std::string& base_uri,
    factory::Starter& factory,
    factory::App& app_factory) :
    service::FactoryService<factory::Starter,
    model::Starter>(base_uri, factory),
    app_factory(app_factory) { }

automation::http::ExtendedApp::Handler service::Starter::serve_static(
    const std::string& base_url,
    const std::string& path) {

    return [&,base_url=base_url,path=path](auto res, auto req) {

        spdlog::debug("{} {} : New static app request.", req->get_method(), req->get_url());

        std::string id(req->get_parameter(0));

        auto starter = factory.get_one(id);

        std::string app_id = starter->get_app_id();
        std::string file(req->get_url());
        std::string new_base_url = base_url + "/" + app_id;
        std::string new_path = path + "/" +  app_id;

        service::Static static_service(new_path, "index.html");

        static_service(new_base_url, [&starter](const std::filesystem::path& p, std::string& content) {

            if (p.filename() != "index.html") {
                return;
            }

            // replacing </head> in response with meta tag list of key value pairs
            // filled with the data from the starters parameter map
            std::string parameter_string = "";

            auto map = starter->get_parameter_map();
            for (auto item_it = map.begin(); item_it != map.end(); item_it++) {
                parameter_string += "<meta name=\"rikerio-app-data\" key=\"" + item_it->first + "\" value=\"" + item_it->second + "\"/>\n";
            }
            parameter_string += "</head>";

            auto head_end_tag_it = Utils::case_ins_find(content, "</head>");
            content.replace(head_end_tag_it, head_end_tag_it + 7, parameter_string);

        })(res, req);


    };
}

Json::Value service::Starter::to_json(const model::Starter& model) const {

    Json::Value result;

    result["id"] = model.get_id();
    result["app_id"] = model.get_app_id();
    result["label"] = model.get_label();
    result["description"] = model.get_description();

    Json::Value parameter_map;

    for (auto it : model.get_parameter_map()) {
        parameter_map[it.first] = it.second;
    }

    result["parameter"] = parameter_map;
    result["disabled"] = model.is_disabled();

    return result;

}


model::Starter service::Starter::from_json(Json::Value body, const std::string& method) const {

    model::Starter model;

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_app_id = body["app_id"];
    auto json_label = body["label"];
    auto json_description = body["description"];
    auto json_parameter_map = body["parameter"];
    auto json_disabled = body["disabled"];

    if (method == "edit" && (!json_id || !json_id.isString())) {
        spdlog::debug("No id present");
        throw ExtendedApp::bad_request_error("Missing id in request body.");
    }

    if (!json_app_id || !json_app_id.isString()) {
        spdlog::debug("No Application ID present.");
        throw ExtendedApp::bad_request_error("Missing application ID in request body.");
    }

    if (!json_label) {
        spdlog::debug("No label present.");
        throw ExtendedApp::bad_request_error("Missing label in request body.");
    }

    if (json_description && !json_description.isString()) {
        spdlog::debug("Description is no string.");
        throw ExtendedApp::bad_request_error("Description musst be a string.");
    }

    if (json_parameter_map && !json_parameter_map.isObject()) {
        spdlog::debug("Parameter is no Map.");
        throw ExtendedApp::bad_request_error("Parameter musst be a map.");
    }

    for (auto it = json_parameter_map.begin(); it != json_parameter_map.end(); it++ ) {
        if (!it.key().isString() || !(*it).isString()) {
            spdlog::debug("Parameter musst contain string keys and string values.");
            throw ExtendedApp::bad_request_error("Parameter Object musst contain string keys and string values.");
        }
    }

    if (json_disabled && !json_disabled.isBool()) {
        spdlog::debug("Disabled musst be string.");
        throw ExtendedApp::bad_request_error("Disabled musst be string.");
    }

    const std::string id = json_id.as<std::string>();
    const std::string app_id = json_app_id.as<std::string>();
    const std::string label = json_label.as<std::string>();
    const std::string description = "";

    if (method == "edit" && id.length() < 3) {
        spdlog::debug("id length < 3.");
        throw ExtendedApp::bad_request_error("id to short.");
    }

    if (label.length() < 3) {
        spdlog::debug("label length < 3.");
        throw ExtendedApp::bad_request_error("label to short.");
    }


    model
    .set_id(id)
    .set_app_id(app_id)
    .set_label(label);

    if (json_description) {
        model.set_description(json_description.as<std::string>());
    }

    for (auto it = json_parameter_map.begin(); it != json_parameter_map.end(); it++ ) {
        model.add_parameter(it.key().as<std::string>(), (*it).as<std::string>());
    }

    if (json_disabled) {
        model.set_disabled(json_disabled.as<bool>());
    }

    return model;

}
