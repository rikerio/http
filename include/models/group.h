#ifndef __AUTOMATION_HTTP_MODEL_GROUP_H__
#define __AUTOMATION_HTTP_MODEL_GROUP_H__

#include "set"
#include "string"
#include "memory"
#include "models/model.h"

namespace automation::http::model {

class Group : public Base {

  public:

      static std::shared_ptr<Group> Noone;

    Group() { 
        whitelist_set.insert("get:/api/1/setting/application.*");
    }

    const std::string& get_id() const {
        return id;
    }

    Group& set_id(const std::string& value) {
        id = value;
        return *this;
    }

    Group& add_whitelist_pattern(const std::string& v) {
        whitelist_set.insert(v);
        return *this;
    }

    Group& add_blacklist_pattern(const std::string& v) {
        blacklist_set.insert(v);
        return *this;
    }

    Group& clear_whitelist() {
        whitelist_set.clear();
        return *this;
    }

    Group& clear_blacklist() {
        blacklist_set.clear();
        return *this;
    }

    const std::set<std::string>& get_whitelist() const {
        return whitelist_set;
    }

    const std::set<std::string>& get_blacklist() const {
        return blacklist_set;
    }

  private:
    std::string id;

    std::set<std::string> whitelist_set;
    std::set<std::string> blacklist_set;

};

}

#endif
