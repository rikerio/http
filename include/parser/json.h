#ifndef __AUTOMATION_HTTP_JSON_BODY_PARSER_H__
#define __AUTOMATION_HTTP_JSON_BODY_PARSER_H__

#include "xapp.h"
#include "json/json.h"
#include "memory"

namespace automation::http::parser {

class json {
  public:
    static Json::Value parse(const std::string_view);
    static std::string stringify(const Json::Value);
};

}


#endif
