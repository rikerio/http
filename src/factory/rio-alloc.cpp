#include "factory/rio-alloc.h"

using namespace automation::http;

factory::RioAlloc::RioAlloc(EventEmitter& emitter, std::string event_prefix, RikerIO::Profile& rio_profile) :
    Cached(emitter, event_prefix),
    rio_profile(rio_profile) {
    update_cache();
}

std::shared_ptr<model::RioAlloc> factory::RioAlloc::add(model::RioAlloc&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioAlloc> factory::RioAlloc::update(const std::string&, model::RioAlloc&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioAlloc> factory::RioAlloc::remove(const std::string&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioAlloc> factory::RioAlloc::get_one(const std::string&) {
    throw std::runtime_error("Not implemented");
    return nullptr;

}

bool factory::RioAlloc::filter(const model::RioAlloc& a, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != a.get_id().find(value)) {
        return true;
    }

    return false;

}


int factory::RioAlloc::compare(const std::string& field, const model::RioAlloc& a, const model::RioAlloc& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "offset") {
        if (a.get_offset() < b.get_offset()) {
            return -1;
        } else if (a.get_offset() > b.get_offset()) {
            return 1;
        } else {
            return 0;
        }
    }

    if (field == "size") {
        if (a.get_size() < b.get_size()) {
            return -1;
        } else if (a.get_size() > b.get_size()) {
            return 1;
        } else {
            return 0;
        }
    }

    return 0;

}

void factory::RioAlloc::update_cache() {

    std::filesystem::file_time_type new_last_write;

    spdlog::debug("RioAlloc::update_cache() : Checking rikerio for updates.");
    if (RikerIO::last_write(rio_profile, new_last_write) == RikerIO::result_error) {
        spdlog::error("RioAlloc::update_cache() : Error receiving last write time.");
    };

    if (new_last_write == rio_last_write) {
        spdlog::debug("RioAlloc::update_cache() : No updates available, cache stays unchanged.");
        return;
    }

    spdlog::debug("RioAlloc::update_cache() : Updates available!");

    rio_last_write = new_last_write;

    std::vector<RikerIO::Allocation> list;
    if (RikerIO::list(rio_profile, list) == RikerIO::result_error) {
        spdlog::error("Error updating RikerIO Allocation list.");
        return;
    }

    t_map.clear();
    query_cache_map.clear();

    for (auto& a : list) {
        auto ptr_a = std::make_shared<model::RioAlloc>(a);
        t_map[a.id] = ptr_a;
    }


}
