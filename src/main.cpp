
#include "cstdlib"
#include "semver.h"

#include "rikerio-automation/automation.h"

#include "CLI11.h"

#include "io-task.h"
#include "event-emitter.h"

#include "factory/serializer.h"
#include "factory/user.h"
#include "factory/permission.h"
#include "factory/key.h"
#include "factory/setting.h"
#include "factory/rio-alloc.h"
#include "factory/rio-data.h"
#include "factory/rio-link.h"
#include "factory/file.h"
#include "factory/app.h"
#include "factory/starter.h"

#include "service/crud.h"
#include "service/version.h"
#include "service/accept.h"
#include "service/websocket.h"
#include "service/io.h"
#include "service/io-ws.h"
#include "service/static.h"
#include "service/redirect.h"
#include "service/jwt.h"
#include "service/user.h"
#include "service/permission.h"
#include "service/key.h"
#include "service/setting.h"
#include "service/authenticate.h"
#include "service/authorize.h"
#include "service/rio-alloc.h"
#include "service/rio-data.h"
#include "service/rio-link.h"
#include "service/file.h"
#include "service/app.h"
#include "service/starter.h"

#include "middleware/log.h"

#include "config.h"

#include "dlfcn.h"

#include "spdlog/spdlog.h"
#include "xapp.h"


static void systemd_notify(char* msg) {

    typedef int (*systemd_notify)(int code, const char *param);

    void *systemd_so = dlopen("libsystemd.so", RTLD_NOW);

    if (systemd_so == NULL) {
        spdlog::warn("No libsystemd.so found. No SystemD notification happening.");
        return;
    }

    systemd_notify *sd_notify = (systemd_notify*) dlsym(systemd_so, "sd_notify");
    spdlog::debug("Notifying systemd.");
    systemd_notify func = (systemd_notify) sd_notify;
    int retVal = func(0, msg);

    if (retVal < 0) {
        spdlog::error("Error notifying (sd_notify) SystemD {}.", retVal);
    }

    dlclose(systemd_so);

}


int main(int argc, char** argv) {

    /* logging */

    std::string config_file = "/etc/rikerio/http/config.yaml";
    bool debug_mode = false;

    // CLI

    /* setup command line parser */
    CLI::App cli_app{"RikerIO Http Interface"};


    cli_app.add_option("-c,--config", config_file, "Config File.");
    cli_app.add_flag("-d,--debug", debug_mode, "Debug Mode.");


    CLI11_PARSE(cli_app, argc, argv);

    spdlog::set_level(debug_mode ? spdlog::level::debug : spdlog::level::info);
    spdlog::info("Starting RikerIO Http Server Version {}.", automation::http::version.get_version());
    spdlog::debug("Verbose Mode Activated.");

    /* parse configuration */

    automation::http::Config config;

    try {

        config = automation::http::Config::deserialize(config_file);

    } catch (...) {

        spdlog::error("Error reading configuration file {}.", config_file);
        return EXIT_FAILURE;

    }

    /* create IO-Task */

    RikerIO::Profile rio_profile;

    try {
        RikerIO::init(config.rikerio.profile, rio_profile);
    } catch (std::runtime_error& e) {
        spdlog::error("Error initiating RikerIO Profile '{}'.", config.rikerio.profile);
        spdlog::error("{}", e.what());
        return EXIT_FAILURE;
    }

    thread_local automation::http::IoTask io_task(
        rio_profile,
        config.io.short_cycle,
        config.io.buffer_size,
        config.io.requests_per_cycle);

    io_task.set_lock_handler([&rio_profile]() {
        RikerIO::lock(rio_profile);
    });

    io_task.set_unlock_handler([&rio_profile]() {
        RikerIO::unlock(rio_profile);
    });

    /* io task */

    automation::os::ThreadConfig io_thread_config = {
        .scheduler = {
            .type = automation::os::SchedulerType::rr
        },
        .args = {
            .realtime = {
                .cycle = config.io.short_cycle,
                .priority = config.io.priority
            }
        }
    };
    automation::os::RealtimeThread io_thread(io_thread_config);

    io_thread.add_task(io_task);
    io_thread.start();

    /* wait for io task to be running */

    while (!io_thread.is_running()) {
        usleep(10000);
    }

    /* app and services */

    using namespace automation::http;

    thread_local EventEmitter emitter;

    thread_local factory::Serializer serializer;

    thread_local factory::User user_factory(emitter, config.folder.db + "/user.yaml");
    thread_local factory::JWT jwt_factory(user_factory, "temporary.secret.change.to.config.secret.later.");
    thread_local factory::Permission permission_factory(emitter, config.folder.db + "/permissions.yaml");
    thread_local factory::Key key_factory(emitter, config.folder.db + "/keys.yaml");
    thread_local factory::Setting setting_factory(emitter, config.folder.db + "/settings.yaml");
    thread_local factory::File file_factory(emitter, config.folder.db + "/files.yaml");
    thread_local factory::App app_factory(emitter, config.folder.db + "/apps.yaml");
    thread_local factory::Starter starter_factory(emitter, config.folder.db + "/starter.yaml", app_factory);
    thread_local factory::RioAlloc rio_alloc_factory(emitter, "rikerio_alloc", rio_profile);
    thread_local factory::RioData rio_data_factory(emitter, "rikerio_data", rio_profile);
    thread_local factory::RioLink rio_link_factory(emitter, "rikerio_link", rio_profile);

    // create basic user admin, guest and nobody

    try {
        user_factory.get_user_by_username("admin");
    } catch (factory::not_found_error& e) {
        std::string admin_password = automation::http::Utils::create_random_token(8);
        auto admin_user = user_factory.initiate_admin(admin_password);

        model::Permission admin_perm = model::Permission::CreateAllow(
                                           admin_user->get_id(), "*", "*");

        permission_factory.add(admin_perm);

        spdlog::info("IMPORTANT!!!! User 'admin' created with password '{}', write this down!.", admin_password);
    }

    try {
        user_factory.get_user_by_username("guest");
    } catch (factory::not_found_error& e) {
        user_factory.initiate_guest();
    }

    try {
        user_factory.get_user_by_username("nobody");
    } catch (factory::not_found_error& e) {
        auto nobody_user = user_factory.initiate_nobody();

        model::Permission nobody_perm = model::Permission::CreateAllow(
                                            nobody_user->get_id(), "get", "/api/1/settings/application.*");

        permission_factory.add(nobody_perm);

    }

    serializer
    .add(user_factory, 10000)
    .add(permission_factory, 10000)
    .add(key_factory, 10000)
    .add(file_factory, 10000)
    .add(app_factory, 10000)
    .add(starter_factory, 10000)
    .add(setting_factory, 1000);

    thread_local uWS::App app;
    thread_local ExtendedApp xApp(app);
    thread_local middleware::Logger log_middleware;
    thread_local service::JWT jwt_service(user_factory, jwt_factory);
    thread_local service::Static static_login_service(config.folder.webapp, "login.html");
    thread_local service::Static static_apps_service(config.folder.webapp, "apps.html");
    thread_local service::Static static_admin_service(config.folder.webapp, "admin.html");
    thread_local service::WebSocket websocket_service;
    thread_local service::Io io_service(io_task, rio_profile);
    thread_local service::IoWebsocket io_websocket_service(io_task, rio_profile);
    thread_local service::User user_service("/api/1/user", user_factory);
    thread_local service::Permission permission_service("/api/1/permission", permission_factory, user_factory);
    thread_local service::Key key_service("/api/1/key", key_factory);
    thread_local service::Setting setting_service("/api/1/setting", setting_factory);
    thread_local service::Authenticate authenticate_service(user_factory, permission_factory, key_factory, jwt_factory);
    thread_local service::RioAlloc rikerio_alloc_service("/api/1/rio/allocation", rio_alloc_factory);
    thread_local service::RioData rikerio_data_service("/api/1/rio/data", rio_data_factory);
    thread_local service::RioLink rikerio_link_service("/api/1/rio/link", rio_link_factory);
    thread_local service::File file_service("/api/1/file", file_factory, config.folder.files);
    thread_local service::App app_service(
        "/api/1/app",
        app_factory,
        file_factory,
        user_factory,
        config.folder.apps,
        config.folder.files);
    thread_local service::Starter starter_service("/api/1/starter", starter_factory, app_factory);

    thread_local us_timer_t* fast_timer = us_create_timer((us_loop_t*) uWS::Loop::get(), 0, 0);
    thread_local us_timer_t* slow_timer = us_create_timer((us_loop_t*) uWS::Loop::get(), 0, 0);

    xApp.set_cors_enabled(config.cors_enabled);

    xApp.use(log_middleware);
    xApp.use(jwt_service);
    xApp.use(user_service);
    xApp.use(permission_service);

    xApp.get("/") << service::Redirect::temporary("/www/apps");
    xApp.get("/www/*") << static_login_service("/www");
    xApp.get("/www/login") << service::Static::replace_url("/www/login.html")
                           << static_login_service("/www");
    xApp.get("/www/apps") << service::Static::replace_url("/www/apps.html")
                          << static_apps_service("/www");
    xApp.get("/www/apps/*") << static_apps_service("/www");
    xApp.get("/www/admin") << service::Static::replace_url("/www/admin.html")
                           << static_admin_service("/www");
    xApp.get("/www/admin/*") << static_admin_service("/www");

    xApp.get("/app/1/:id")
    << [](auto res, auto req) {
        std::string id(req->get_parameter(0));
        service::Redirect::temporary("/app/1/" + id + "/")(res, req);
    };

    // VERSION API

    xApp.get("/api/1/version") << automation::http::service::VersionService::get();

    // USER API

    automation::http::service::CRUD user_service_setup(xApp, "/api/1/user");

    user_service_setup
    .add_handler("create", user_service.create())
    .add_handler("readone", user_service.get_one())
    .add_handler("readmany", user_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", user_service.update())
    .add_handler("delete", user_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // query example
    /* xApp.get("/api/1/user")
    << [](auto res, auto req) {
        // 1 extract filter_string, sort_string and sort_order from request
        // 2 create rest query request
        // 3 execute query request on rest service
        // 4 filter rest results by permissions
        // 5 transform rest results to json
    } */

    // PERMISSION API

    automation::http::service::CRUD permission_service_setup(xApp, "/api/1/permission");

    permission_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", permission_service.create())
    .add_handler("readone", permission_service.get_one())
    .add_handler("readmany", permission_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", permission_service.update())
    .add_handler("delete", permission_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // KEY API

    automation::http::service::CRUD key_service_setup(xApp, "/api/1/key");
    key_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", key_service.create())
    .add_handler("readone", key_service.get_one())
    .add_handler("readmany", key_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", key_service.update())
    .add_handler("delete", key_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // RIKERIO Allocations API

    automation::http::service::CRUD rio_alloc_service_setup(xApp, "/api/1/rio/allocation");

    rio_alloc_service_setup
    .add_handler("readmany", rikerio_alloc_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // RIKERIO Data API

    automation::http::service::CRUD rio_data_service_setup(xApp, "/api/1/rio/data");

    rio_data_service_setup
    .add_handler("readmany", rikerio_data_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // RIKERIO Link API

    automation::http::service::CRUD rio_link_service_setup(xApp, "/api/1/rio/link");

    rio_link_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", rikerio_link_service.create())
    .add_handler("readmany", rikerio_link_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("readone", rikerio_link_service.get_one())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", rikerio_link_service.get_one())
    .add_handler("delete", rikerio_link_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // IO API

    automation::http::service::CRUD io_service_setup(xApp, "/api/1/io");

    io_service_setup
    .add_handler("readone", io_service.get_one())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", io_service.update())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // Authenticate API

    automation::http::service::CRUD auth_service_setup(xApp, "/api/1/auth");
    auth_service_setup
    .add_handler("create", authenticate_service.authenticate())
    .setup();

    // Settings API

    automation::http::service::CRUD settings_service_setup(xApp, "/api/1/setting");
    settings_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", setting_service.create())
    .add_handler("readmany", setting_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("readone", setting_service.get_one())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", setting_service.update())
    .add_handler("delete", setting_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // FILE API

    automation::http::service::CRUD file_service_setup(xApp, "/api/1/file");
    file_service_setup
    .add_handler("create", service::Accept::contentType("multipart/form-data; boundary=*"))
    .add_handler("create", service::Accept::maxContentLength(config.max_file_size))
    .add_handler("create", file_service.create())
    .add_handler("readmany", file_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("readone", file_service.get_one())
    .add_handler("delete", file_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    xApp.get("/api/1/file/:id/file")
            << service::Authorize::authorize()
            << file_service.download();

    // APP API

    automation::http::service::CRUD app_service_setup(xApp, "/api/1/app");
    app_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", app_service.create())
    .add_handler("readmany", app_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("readone", app_service.get_one())
    .add_handler("delete", app_service.remove())
    .add_pre_handler(service::Authorize::authorize())
    .setup();

    // STARTER API

    automation::http::service::CRUD starter_service_setup(xApp, "/api/1/starter");
    starter_service_setup
    .add_handler("create", service::Accept::contentType("application/json*"))
    .add_handler("create", starter_service.create())
    .add_handler("update", service::Accept::contentType("application/json*"))
    .add_handler("update", starter_service.update())
    .add_handler("readmany", starter_service.get_all())
    .add_handler("readmany", service::Authorize::filter())
    .add_handler("readone", starter_service.get_one())
    .add_handler("delete", starter_service.remove())
    .setup();

    xApp.get("/app/1/:id/*")
            << starter_service.serve_static("/app/1", config.folder.apps);


    // WEBSOCKET API

    websocket_service.register_service("rioasync", io_websocket_service);

    xApp.ws("/api/1/ws", websocket_service);

    /* link up io_task and io_service */

    us_timer_set(fast_timer, [](struct us_timer_t*) {

        io_task.receive();

        io_service.trigger();

    }, config.io.short_cycle / 1000, config.io.short_cycle / 1000);

    us_timer_set(slow_timer, [](struct us_timer_t*) {

        io_websocket_service.trigger();

        serializer.trigger();

    }, config.io.long_cycle / 1000, config.io.long_cycle / 1000);

    /* signal handler */

    auto signal_handler = [](int) {

        spdlog::debug("SIGINT fired, exiting application.");

        /* close all websocket connections */

        us_timer_close(slow_timer);
        us_timer_close(fast_timer);

        websocket_service.close_all();

        xApp.close();

    };


    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    // notify systemd
    char sysd_running[] = "READY=1";
    systemd_notify(sysd_running);


    xApp.listen(config.bind, config.port);
    io_thread.stop();

    RikerIO::release(rio_profile);

    // notify systemd
    char sysd_stop[] = "STOPPING=1";
    systemd_notify(sysd_stop);

    spdlog::info("Exiting application.");

    return EXIT_SUCCESS;

}
