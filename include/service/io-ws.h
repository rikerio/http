#ifndef __AUTOMATION_HTTP_SERVICE_IO_WEBSOCKET_H__
#define __AUTOMATION_HTTP_SERVICE_IO_WEBSOCKET_H__

#include "rikerio-automation/automation.h"

#include "io-task.h"
#include "service/websocket.h"
#include "service/io-base.h"

namespace automation::http::service {

class IoWebsocket : public IoBase, public WebSocket::Service {

  public:

    IoWebsocket(automation::http::IoTask&, RikerIO::Profile&);
    ~IoWebsocket();

    void on_connected(uWS::WebSocket<false, true>*);
    void on_close(uWS::WebSocket<false, true>*);
    void on_data(uWS::WebSocket<false, true>*, Json::Value);

    void trigger();

  private:

    struct Data {
        std::string id;
        std::string string_value;
        std::string type;
        unsigned int bit_size;
        unsigned int byte_size;
        RikerIO::DataPoint dp;
        unsigned int holder_count;
        bool buffer_initialized;
        bool buffer_updated;
        Data(std::string id, RikerIO::DataPoint dp) :
            id(id),
            string_value(""),
            type(RikerIO::type_to_string(dp.type)),
            bit_size(RikerIO::get_bitsizeof(dp.type)),
            byte_size(RikerIO::calc_bytesize(bit_size)),
            dp(dp),
            holder_count(1),
            buffer_initialized(false),
            buffer_updated(true) {

            if (dp.type == RikerIO::Type::UNDEF) {
                type = "binary";
                byte_size = dp.bit_size / 8;
                if (dp.bit_size % 8 > 0) {
                    byte_size += 1;
                }
            }

        }
    };

    struct WebSocketContext {
        struct uWS::WebSocket<false, true>* websocket;
        std::map<std::string, bool> key_map;
        std::vector<Data*> data_vector;
        WebSocketContext(uWS::WebSocket<false, true>* ws) : websocket(ws) { }
    };

  private:

    automation::http::IoTask& io_task;
    RikerIO::Profile& rio_profile;

  private:

    std::map<std::string, Data*> data_map;

  private:

    std::map<uWS::WebSocket<false, true>*, std::shared_ptr<WebSocketContext>> websocket_map;

  private:

    Json::CharReaderBuilder json_read_builder;
    std::unique_ptr<Json::CharReader> json_reader;

  private:

    void add_ids(uWS::WebSocket<false, true>* ws, const std::string& pattern);
    void remove_ids(uWS::WebSocket<false, true>* ws, const std::string& pattern);
    void send_updates(WebSocketContext&, bool updated_only = true);

};

}


#endif
