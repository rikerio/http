#include "service/app.h"
#include "yaml-cpp/yaml.h"
#include "fcntl.h"
#include "unistd.h"
#include "sys/stat.h"
#include "sys/types.h"
#include "experimental/filesystem"
#include "service/static.h"

using namespace automation::http;

service::App::App(
    const std::string& base_uri,
    factory::App& factory,
    factory::File& file_factory,
    factory::User& user_factory,
    const std::string& app_folder,
    const std::string& file_folder) :
    service::FactoryService<factory::App, model::App>(base_uri, factory),
    file_factory(file_factory),
    user_factory(user_factory),
    app_folder(app_folder),
    file_folder(file_folder) { }

automation::http::ExtendedApp::Handler service::App::create() {

    auto original_cb = service::FactoryService<factory::App, model::App>::create();

    return [&, ocb = original_cb](auto res, auto req) {

        auto json_body = automation::http::parser::json::parse(req->get_body());
        auto create_form = from_json(json_body);
        model::App app;

        /* make id reservation */

        auto prov_id = factory.reserve_new_id();
        app.set_id(prov_id);

        /* get file and check file type */

        std::shared_ptr<model::File> file_ptr;

        try {

            file_ptr = file_factory.get_one(create_form.file_id);

            if (file_ptr->get_type() != "application/zip" && file_ptr->get_type() != "application/x-zip-compressed") {
                factory.release_id(prov_id);
                throw automation::http::ExtendedApp::bad_request_error("File musst be of type application/zip.");
            }

        } catch (automation::http::factory::not_found_error& e) {
            factory.release_id(prov_id);
            throw automation::http::ExtendedApp::bad_request_error("Archive file not found.");

        }

        /* open zip and look for app.yaml file */

        ::zip* za;
        if (!open_zip_file(&za, file_ptr)) {
            factory.release_id(prov_id);
            throw automation::http::ExtendedApp::bad_request_error("Unknown file for file_id.");
        }

        YAML::Node doc;

        if (!extract_app_yaml(za, doc)) {
            factory.release_id(prov_id);
            throw automation::http::ExtendedApp::bad_request_error("Error extracting app.yaml from archive.");
        }

        std::string err_details = "";
        if (!validate_app_yaml(doc, app, err_details)) {
            factory.release_id(prov_id);
            throw automation::http::ExtendedApp::bad_request_error("Error parsing app.yaml from archive (" + err_details + ").");
        }

        if (!extract_zip_file(za, app)) {
            factory.release_id(prov_id);
            throw automation::http::ExtendedApp::bad_request_error("Error extracting archive.");
        }

        close_zip_file(za);

        std::shared_ptr<model::App> app_ptr;

        try {

            spdlog::debug("{} {} : Passing object to factory.", req->get_method(), req->get_url());
            app_ptr = factory.add(app);

        } catch (typename automation::http::factory::duplicate_error& e) {

            spdlog::debug("Error adding resource, duplicate id.");
            throw ExtendedApp::bad_request_error("Duplicate id.");

        }

        // res->set_header("Content-Type", "application/json; charset=utf-8");
        // res->set_data(automation::http::parser::json::stringify(to_json(*app_ptr)));

        std::shared_ptr<json::SingleResponse<model::App>> response_handler =
        std::make_shared<json::SingleResponse<model::App>>(base_uri, [&](model::App& obj) -> Json::Value {
            return to_json(obj);
        });
        response_handler->set_data(app_ptr);

        res->set_data(response_handler);
        res->set_status(ExtendedApp::http_status_created);

    };

}

automation::http::ExtendedApp::Handler service::App::remove() {

    return [&](auto res, auto req) {

        const std::string& id = req->get_parameter(0);

        try {
            auto t = factory.remove(id);

            std::shared_ptr<json::SingleResponse<model::App>> response_handler =
            std::make_shared<json::SingleResponse<model::App>>(base_uri, [&](model::App& obj) -> Json::Value {
                return to_json(obj);

            });
            response_handler->set_data(t);


            res->set_data(response_handler);
            res->set_status(ExtendedApp::http_status_ok);

            std::filesystem::path app_folder_path(app_folder);

            spdlog::debug("Removing folder located at {}.", (string) app_folder_path);
            try {
                std::filesystem::remove_all(app_folder_path / id);
            } catch (std::filesystem::filesystem_error& e) {
                spdlog::error("Error removing folder on app {} with error code {}({}).", id, e.code().value(), e.what());
            } catch (...) {
                spdlog::error("Unknown error occured while removing folder for app {}.", id);
            }

        } catch (typename automation::http::factory::fixed_item_error& e) {
            throw ExtendedApp::bad_request_error("Resource '" + id + "' cannot be removed.");
        } catch (typename automation::http::factory::not_found_error& e) {
            throw ExtendedApp::not_found_error();
        }

    };

}


Json::Value service::App::to_json(const model::App& app) const {

    Json::Value result;

    result["id"] = app.get_id();
    result["name"] = app.get_name();
    result["description"] = app.get_description();
    result["version"] = app.get_version();

    Json::Value json_author;

    json_author["name"] = app.get_author_name();
    json_author["email"] = app.get_author_email();

    result["author"] = json_author;

    result["disabled"] = app.is_disabled();

    return result;

}


model::App service::App::from_json(Json::Value, const std::string&) const {

    throw automation::http::ExtendedApp::internal_error("from_json should not be called.");

}

service::App::CreateForm service::App::from_json(Json::Value body) const {

    // file_id, diabled, user_id_list

    if (!body || !body.isObject()) {
        throw automation::http::ExtendedApp::bad_request_error("json body musst be an object.");
    }

    CreateForm create_form;

    auto json_file_id = body["file_id"];

    if (!json_file_id || !json_file_id.isString()) {
        throw automation::http::ExtendedApp::bad_request_error("missing file_id or file_id not a string.");
    }

    create_form.file_id = json_file_id.asString();

    auto json_disabled = body["disabled"];

    if (!json_disabled || !json_disabled.isBool()) {
        throw automation::http::ExtendedApp::bad_request_error("missing disabled or disabled is not a boolean.");
    }

    create_form.disabled = json_disabled.asBool();

    return create_form;

}

bool service::App::open_zip_file(::zip** za, shared_ptr<model::File> file_ptr) {

    std::string file_path = file_folder + "/" + file_ptr->get_id();
    int err = 0;
    *za = zip_open(file_path.c_str(), 0, &err);

    if (*za == NULL) {
        char buf[100] = { };
        zip_error_to_str(buf, sizeof(buf), err, errno);
        spdlog::debug("can't open zip archive {}", buf);
        return false;
    }

    return true;

}

bool service::App::extract_app_yaml(::zip* za, YAML::Node& doc) {

    int file_index = -1;
    struct zip_stat sb;

    for (unsigned int i = 0; i < zip_get_num_entries(za, 0); i += 1) {
        if (zip_stat_index(za, i, 0, &sb) != 0) {
            continue;
        }

        if (strcmp(sb.name, "app.yaml") == 0) {
            file_index = i;
            break;
        }

    }

    if (file_index == -1) {
        spdlog::debug("service::App::extract_app_yaml(): file {} not found.", sb.name);
        return false;
    }

    /* extract found_app_yaml_file into local string */

    struct zip_file* zf = zip_fopen_index(za, file_index, 0);

    if (!zf) {
        spdlog::debug("service::App::extract_app_yaml(): error opening file {}.", sb.name);
        return false;
    }

    std::string app_yaml_string = "";
    int len = 0;
    char buf[1024 * 16] = { };
    while (1) {
        memset(&buf, 0, 1024 * 16);
        len = zip_fread(zf, buf, 1024 * 16);
        if (len < 0) {
            spdlog::debug("service::App::extract_app_yaml(): error reading from file.", sb.name);
            return false;
        }
        app_yaml_string += buf;

        if (len == 0 || len < 100) {
            break;
        }
    }

    std::cout << app_yaml_string << std::endl;

    zip_fclose(zf);

    /* convert string to yaml and return */

    try {
        doc = YAML::Load(app_yaml_string);
    } catch (...) {
        spdlog::debug("{}", app_yaml_string);
        spdlog::debug("service::App::extract_app_yaml(): error parsing yaml file app.yaml in archive.");
        return false;
    }

    return true;

}

bool service::App::validate_app_yaml(YAML::Node& node, model::App& app, std::string& err_detail) {

    // required
    auto yaml_name = node["name"];

    if (yaml_name && yaml_name.Type() == YAML::NodeType::Scalar) {
        app.set_name(yaml_name.as<std::string>());
    } else {
        err_detail = "Missing name";
        spdlog::debug("Missing name");
        return false;
    }

    // optional
    auto yaml_description = node["description"];

    if (yaml_description && yaml_description.Type() == YAML::NodeType::Scalar) {
        app.set_description(yaml_description.as<std::string>());
    }

    // required
    auto yaml_version = node["version"];

    if (yaml_version && yaml_version.Type() == YAML::NodeType::Scalar) {
        app.set_version(yaml_version.as<std::string>());
    } else {
        err_detail = "Missing version";
        spdlog::debug("Missing version");
        return false;
    }

    // required
    auto yaml_author = node["author"];

    if (yaml_author && yaml_author.Type() == YAML::NodeType::Map) {

        // required
        auto yaml_author_name = yaml_author["name"];

        if (yaml_author_name && yaml_author_name.Type() == YAML::NodeType::Scalar) {
            app.set_author_name(yaml_author_name.as<std::string>());
        } else {
            err_detail = "Missing author name";
            spdlog::debug("Missing author name");
            return false;
        }

        // required
        auto yaml_author_email = yaml_author["email"];

        if (yaml_author_email && yaml_author_email.Type() == YAML::NodeType::Scalar) {
            app.set_author_email(yaml_author_email.as<std::string>());
        } else {
            err_detail = "Missing author email";
            spdlog::debug("Missing author email");
            return false;
        }

    } else {
        err_detail = "Missing author field";
        spdlog::debug("Missing author field");
        return false;
    }

    return true;

}

void service::App::create_dir(const std::string& dir) {
    spdlog::debug("Creating folder {} for app.", dir);
    if (mkdir(dir.c_str(), 0755) < 0) {
        if (errno != EEXIST) {
            perror(dir.c_str());
        }
    }
}

bool service::App::extract_zip_file(::zip* za, model::App& app) {

    const std::string target_folder = app_folder + "/" + app.get_id();

    create_dir(target_folder);

    struct zip_file *zf = NULL;
    struct zip_stat sb;
    char buf[100] = {};
    int len = 0;
    int fd = 0;
    size_t sum = 0;

    for (int i = 0; i < zip_get_num_entries(za, 0); i++) {
        if (zip_stat_index(za, i, 0, &sb) != 0) {
            continue;
        }
        len = strlen(sb.name);
        std::string filename = target_folder + "/"+ sb.name;
        if (strcmp(sb.name, "app.yaml") == 0) {
            continue;
        }

        if (sb.name[len - 1] == '/') {
            create_dir(filename);
        } else {
            zf = zip_fopen_index(za, i, 0);
            if (!zf) {
                spdlog::error("Error extracting file {} from archive.", sb.name);
                continue;
            }

            fd = open(filename.c_str(), O_RDWR | O_TRUNC | O_CREAT, 0644);
            if (fd < 0) {
                spdlog::error("Error opening new file on fs {}.", sb.name);
                continue;
            }

            sum = 0;
            while (sum != sb.size) {
                len = zip_fread(zf, buf, 100);
                if (len < 0) {
                    spdlog::error("Error reading file {} from archive.", sb.name);
                    continue;
                }
                write(fd, buf, len);
                sum += len;
            }
            close(fd);
            zip_fclose(zf);
        }
    }

    return true;

}

bool service::App::close_zip_file(::zip* za) {
    return zip_close(za) != -1;
}
