#ifndef __AUTOMATION_HTTP_FACTORY_RIO_DATA_H__
#define __AUTOMATION_HTTP_FACTORY_RIO_DATA_H__

#include "stdexcept"
#include "rikerio-automation/automation.h"
#include "spdlog/spdlog.h"

#include "factory/cache.h"
#include "models/rio-data.h"

namespace automation::http::factory {

class RioData : public Cached<model::RioData> {

  public:

    RioData(EventEmitter& emitter, std::string event_prefix, RikerIO::Profile& rio_profile);

    std::shared_ptr<model::RioData> add(model::RioData&);
    std::shared_ptr<model::RioData> update(const std::string&, model::RioData&);
    std::shared_ptr<model::RioData> remove(const std::string&);
    std::shared_ptr<model::RioData> get_one(const std::string&);

  private:

    RikerIO::Profile& rio_profile;
    std::filesystem::file_time_type rio_last_write;

    bool filter(const model::RioData&, const std::string&) const;
    int compare(const std::string&, const model::RioData&, const model::RioData&) const;

    void update_cache() override;

};

}

#endif
