#include "factory/group.h"

using namespace automation::http;

factory::Group::Group(EventEmitter& emitter, const std::string& filename) :
    YamlFile<model::Group>(emitter, filename, "group") {

    deserialize();

    auto admin_it = t_map.find("admin");
    auto guest_it = t_map.find("guest");
    auto noone_it = t_map.find("noone");

    /* create default group */
    if (admin_it == t_map.end()) {
        model::Group admin_group;
        admin_group.set_id("admin");
        admin_group.add_whitelist_pattern("*:*");
        t_map["admin"] = std::make_shared<model::Group>(admin_group);
    }

    if (guest_it == t_map.end()) {
        model::Group guest_group;
        guest_group.set_id("guest");
        guest_group.add_blacklist_pattern("*:*");
        t_map["guest"] = std::make_shared<model::Group>(guest_group);
    }

    if (noone_it == t_map.end()) {
        model::Group noone_group;
        noone_group.set_id("noone");
        noone_group.add_whitelist_pattern("get:/api/1/setting/application.*");
        t_map["noone"] = std::make_shared<model::Group>(noone_group);
    }

}

factory::Group::~Group() {
    serialize();
}

std::shared_ptr<model::Group> factory::Group::add(model::Group& group) {

    if (group.get_id() == "admin" || group.get_id() == "guest" || group.get_id() == "noone") {
        throw fixed_item_error();
    }

    return factory::YamlFile<model::Group>::add(group);

}

std::shared_ptr<model::Group> factory::Group::remove(const std::string& id) {

    if (id == "admin" || id == "guest" || id == "noone") {
        throw fixed_item_error();
    }

    return factory::YamlFile<model::Group>::remove(id);

}

std::shared_ptr<model::Group> factory::Group::update(const std::string& id, model::Group& group) {

    if (id == "admin") {
        throw fixed_item_error();
    }

    return factory::YamlFile<model::Group>::update(id, group);

}

YAML::Node factory::Group::to_yaml(const model::Group& rhs) const {

    YAML::Node node;

    node["id"] = rhs.get_id();
    YAML::Node node_whitelist;
    YAML::Node node_blacklist;

    for (auto& key : rhs.get_whitelist()) {
        node_whitelist.push_back(key);
    }

    for (auto& key : rhs.get_blacklist()) {
        node_blacklist.push_back(key);
    }

    node["whitelist"] = node_whitelist;
    node["blacklist"] = node_blacklist;

    return node;

}

model::Group factory::Group::from_yaml(YAML::Node node) const {

    model::Group group;

    auto node_id = node["id"];
    auto node_whitelist = node["whitelist"];
    auto node_blacklist = node["blacklist"];

    if (node_id && node_id.Type() == YAML::NodeType::Scalar) {
        group.set_id(node_id.as<std::string>());
    }

    if (node_whitelist && node_whitelist.Type() == YAML::NodeType::Sequence) {
        for (auto n : node_whitelist) {
            if (n && n.Type() == YAML::NodeType::Scalar) {
                group.add_whitelist_pattern(n.as<std::string>());
            }
        }
    }

    if (node_blacklist && node_blacklist.Type() == YAML::NodeType::Sequence) {
        for (auto n : node_blacklist) {
            if (n && n.Type() == YAML::NodeType::Scalar) {
                group.add_blacklist_pattern(n.as<std::string>());
            }
        }
    }

    return group;

}

bool factory::Group::filter(const model::Group& group, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != group.get_id().find(value)) {
        return true;
    }

    return false;

}

int factory::Group::compare(const std::string& field, const model::Group& a, const model::Group& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    return 0;

}

const std::string factory::Group::provide_unique_id () const {
    throw not_implemented_error();
}
