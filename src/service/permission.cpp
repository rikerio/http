#include "service/permission.h"

using namespace automation::http;

service::Permission::Permission(
    const std::string& base_uri,
    factory::Permission& factory, factory::User& user_factory) :
    FactoryService<factory::Permission, model::Permission>(base_uri, factory),
    user_factory(user_factory) { }

Json::Value service::Permission::to_json(const model::Permission& model) const {

    Json::Value result = Json::objectValue;
    result["id"] = model.get_id();
    result["userid"] = model.get_user_id();
    result["type"] = model.get_type() == model::Permission::Type::ALLOW ? "allow" : "deny";
    result["method"] = model.get_method_pattern();
    result["uri"] = model.get_uri_pattern();
    result["disabled"] = model.is_disabled();

    return result;

}

model::Permission service::Permission::from_json(Json::Value body, const std::string& method) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_user_id = body["userid"];
    auto json_type = body["type"];
    auto json_method_pattern = body["method"];
    auto json_uri_pattern = body["uri"];
    auto json_disabled = body["disabled"];

    if (method == "edit") {
        if (!json_id || !json_id.isString()) {
            spdlog::debug("No id present");
            throw ExtendedApp::bad_request_error("Missing id in request body.");
        }
    }

    if (!json_user_id || !json_user_id.isString()) {
        spdlog::debug("No username present");
        throw ExtendedApp::bad_request_error("Missing user id in request body.");
    }

    if (!json_type || !json_type.isString()) {
        spdlog::debug("No type present.");
        throw ExtendedApp::bad_request_error("Missing type state in request body.");
    }


    const std::string id = json_id.as<std::string>();
    const std::string user_id = json_user_id.as<std::string>();
    const std::string type = json_type.as<std::string>();
    const std::string method_pattern = json_method_pattern.as<std::string>();
    const std::string uri_pattern = json_uri_pattern.as<std::string>();
    bool disabled = false;

    if (json_disabled) {
        disabled = json_disabled.as<bool>();
    }

    if (user_id.length() < 3) {
        spdlog::debug("User Id length < 3.");
        throw ExtendedApp::bad_request_error("User Id to short.");
    }

    if (type != "allow" && type != "deny") {
        spdlog::debug("Type musst be 'allow' or 'deny'.");
        throw ExtendedApp::bad_request_error("Type musst be 'allow' or 'deny'.");
    }


    try {
        user_factory.get_one(user_id);
    } catch (factory::not_found_error& e) {
        spdlog::debug("User with id not found.");
        throw ExtendedApp::bad_request_error("User with id not found.");
    }

    model::Permission model;

    model
    .set_id(id)
    .set_user_id(user_id)
    .set_type(type == "allow" ? model::Permission::Type::ALLOW : model::Permission::Type::DENY)
    .set_method_pattern(method_pattern)
    .set_uri_pattern(uri_pattern)
    .set_disabled(disabled);

    return model;

}
