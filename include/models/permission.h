#ifndef __AUTOMATION_HTTP_MODELS_PERMISSION_H__
#define __AUTOMATION_HTTP_MODELS_PERMISSION_H__

#include "string"
#include "utils.h"
#include "models/model.h"


namespace automation::http::model {

class Permission : public automation::http::model::Base {

  public:

    enum Type : unsigned int {
        DENY = 0, ALLOW = 1
    };

    static Permission CreateAllow(
        const std::string& user_id,
        const std::string& method_pattern,
        const std::string& uri_pattern) {
        Permission perm;
        perm.set_user_id(user_id);
        perm.set_type(ALLOW);
        perm.set_method_pattern(method_pattern);
        perm.set_uri_pattern(uri_pattern);
	perm.set_disabled(false);
        return perm;
    }

    static Permission CreateDeny(
        const std::string& user_id,
        const std::string& method_pattern,
        const std::string& uri_pattern) {
        Permission perm;
        perm.set_user_id(user_id);
        perm.set_type(DENY);
        perm.set_method_pattern(method_pattern);
        perm.set_uri_pattern(uri_pattern);
	perm.set_disabled(false);
        return perm;
    }

    Permission() :
        id(""),
        user_id(""),
        type(DENY),
        method_pattern("*"),
        uri_pattern("*"),
	disabled(false) {

    }

    Permission& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    const std::string& get_id() const {
        return id;
    }

    Permission& set_user_id(const std::string& value) {
        user_id = value;
        return *this;
    }

    const std::string& get_user_id() const {
        return user_id;
    }

    Permission& set_type(const Type& value) {
        type = value;
        return *this;
    }

    const Type& get_type () const {
        return type;
    }

    Permission& set_method_pattern(const std::string& value) {
        method_pattern = value;
        return *this;
    }

    const std::string& get_method_pattern() const {
        return method_pattern;
    }

    Permission& set_uri_pattern(const std::string& value) {
        uri_pattern = value;
        return *this;
    }

    const std::string& get_uri_pattern() const {
        return uri_pattern;
    }

    Permission& set_disabled(const bool value) {
	disabled = value;
	return *this;
    }

    const bool is_disabled() const {
	return disabled;
    }

  private:

    std::string id;
    std::string user_id;
    Type type;
    std::string method_pattern;
    std::string uri_pattern;
    bool disabled;
};

}


#endif
