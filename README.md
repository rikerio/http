# RikerIO HTTP Interface

HTTP Server working with RikerIO and the following features :

- RikerIO Allocation, Data and Link Operations
- User/Groups Management and Authentication
- Websocket Service for asynchronous data retrieval from the RikerIO Shared Memory Segment
- Key/Value Store for Application Configuration
- RFID Authentication

## Build instructions

RikerIO HTTP needs the RikerIO Automation Library and uWebSockets as well as some other components.

I usually create an SDK folder lets call it ${SDK}

Install the rikerio automation library into the SDK


```
git clone --branch dev/v2.1 https://gitlab.com/rikerio/automation-library.git ${SDK}/src/automation-library
meson setup --prefix=${SDK}/usr ${SDK}/build/automation-library ${SDK}/src/automation-library
meson install -C ${SDK}/build/automation-library

```

Install uWebSockets into the SDK
```
git clone --recursive --branch v18.24.0 https://github.com/uNetworking/uWebSockets.git ${SDK}/src/uWebSockets
make -C ${SDK}/src/uWebSockets
make install prefix=${SDK}/usr -C ${SDK}/src/uWebSockets
ln -s ${SDK}/src/uWebSockets/uSockets/uSockets.a ${SDK}/usr/lib/libuSockets.a
cp ${SDK}/uWebSockets/uSockets/src/libusockets.h ${SDK}/usr/include

```

Add the SDK folder to your LD_LIBRARY_PATH and PKG_CONFIG before building this application.

RikerIO HTTP Interface uses meson and ninja as build tools. 

```
meson setup build
LIBRARY_PATH=${SDK}/usr/lib meson compile -C build
```

