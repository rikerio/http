#include "middleware/cookies.h"
#include "utils.h"

using namespace automation::http;

middleware::Cookies::Cookies(factory::Session& session_factory) : session_factory(session_factory) { };
middleware::Cookies::~Cookies() { };

middleware::Cookies::operator automation::http::ExtendedApp::Handler() {
    return [&](auto res, auto req) {

        spdlog::debug("{} : Parsing Request for session information.", req->get_url());

        auto sid_value = req->get_cookie("sid");

        if (sid_value != nullptr) {
            spdlog::debug("{} : Found cookie {} = {}.", req->get_url(), "sid", *sid_value);

            try {
                auto session = session_factory.get_one(*sid_value);
                spdlog::debug("{} : Found session object for session id {}", req->get_url(), *sid_value);

                req->set_session(session);

            } catch (factory::not_found_error& e) {

                spdlog::debug("{} : No session found for id {}", req->get_url(), *sid_value);

                if (!res->has_tag("session.init")) {
                    spdlog::debug("{} : No cookie session detected.", req->get_url());
                    return;
                }

                spdlog::debug("{} : Creating new session and new id", req->get_url());

                spdlog::debug("{} : Sending new cookie.", req->get_url());
                model::Session new_session;
                new_session.set_path("/");
                new_session.set_max_age(31536000);
                new_session.set_user_agent(std::string(req->get_header("user-agent")));
                auto session = session_factory.add(new_session);
                res->set_header("Set-Cookie","sid=" + session->get_id() + "; Path=" + session->get_path() + "; Expires=" + session->get_expires_utc_string());

                req->set_session(session);

            }

        } else {

            spdlog::debug("{} : No cookie or session found.", req->get_url());
            if (res->has_tag("session.init")) {
                spdlog::debug("{} : Sending new cookie.", req->get_url());
                model::Session new_session;
                new_session.set_path("/");
                new_session.set_max_age(31536000);
                new_session.set_user_agent(std::string(req->get_header("user-agent")));
                auto session = session_factory.add(new_session);
                res->set_header("Set-Cookie","sid=" + session->get_id() + "; Path=" + session->get_path() + "; Expires=" + session->get_expires_utc_string());
                return;
            } else {
                spdlog::debug("{} : No cookie session detected.", req->get_url());
                return;
            }

        }
    };

}
