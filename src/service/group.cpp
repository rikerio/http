#include "service/group.h"

using namespace automation::http;

service::Group::Group(const std::string& base_uri, factory::Group& group_factory) :
    service::FactoryService<factory::Group, model::Group>(base_uri, group_factory) { };

Json::Value service::Group::to_json(const model::Group& group) const {

    Json::Value result;
    result["id"] = group.get_id();
    Json::Value json_whitelist = Json::arrayValue;
    Json::Value json_blacklist = Json::arrayValue;

    for (auto& pattern : group.get_whitelist()) {
        json_whitelist.append(pattern);
    }

    for (auto& pattern : group.get_blacklist()) {
        json_blacklist.append(pattern);
    }

    result["whitelist"] = json_whitelist;
    result["blacklist"] = json_blacklist;

    return result;

}


model::Group service::Group::from_json(Json::Value body, const std::string&) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_whitelist = body["whitelist"];
    auto json_blacklist = body["blacklist"];

    if (!json_id || !json_id.isString()) {
        spdlog::debug("No groupname present");
        throw ExtendedApp::bad_request_error("Missing groupname in request body.");
    }

    if (json_whitelist && !json_whitelist.isArray()) {
        spdlog::debug("No whitelist present.");
        throw ExtendedApp::bad_request_error("Missing whitelist in request body.");
    }

    if (json_blacklist && !json_blacklist.isArray()) {
        spdlog::debug("No blacklist present.");
        throw ExtendedApp::bad_request_error("Missing blacklist in request body.");
    }

    model::Group group;

    group.set_id(json_id.as<std::string>());

    if (json_whitelist) {
        for (auto n : json_whitelist) {
            if (n && !n.isString()) {
                throw ExtendedApp::bad_request_error("Malormed pattern in whitelist.");
            }
            group.add_whitelist_pattern(n.as<std::string>());
        }
    }

    if (json_blacklist) {
        for (auto n : json_blacklist) {
            if (n && !n.isString()) {
                throw ExtendedApp::bad_request_error("Malformed pattern in blacklist.");
            }
            group.add_blacklist_pattern(n.as<std::string>());

        }
    }

    if (group.get_id().length() < 3) {
        spdlog::debug("Groupname length < 3.");
        throw ExtendedApp::bad_request_error("Groupname to short.");
    }

    return group;

}
