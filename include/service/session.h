#ifndef __AUTOMATION_HTTP_SERVICE_SESSION_H__
#define __AUTOMATION_HTTP_SERVICE_SESSION_H__

#include "service/service.h"
#include "models/session.h"
#include "factory/session.h"

namespace automation::http::service {

class Session : public FactoryService<factory::Session, model::Session> {

  public:
    Session(const std::string&, factory::Session& factory);

  protected:

    model::Session from_json(Json::Value, const std::string&) const;
    Json::Value to_json(const model::Session&) const;

};

}


#endif
