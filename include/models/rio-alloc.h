#ifndef __AUTOMATION_HTTP_MODEL_RIO_ALLOCATION_H__
#define __AUTOMATION_HTTP_MODEL_RIO_ALLOCATION_H__

#include "rikerio-automation/automation.h"

namespace automation::http::model {

class RioAlloc {

  public:
    RioAlloc() : offset(0), size(0), id("") { }
    RioAlloc(RikerIO::Allocation& a) :
        offset(a.offset),
        size(a.byte_size),
        id(a.id) { }

    unsigned int get_offset() const {
        return offset;
    }

    RioAlloc& set_offset(unsigned int value) {
        offset = value;
        return *this;
    }

    unsigned int get_size() const {
        return size;
    }

    RioAlloc& set_size(unsigned int value) {
        size = value;
        return *this;
    }

    const std::string& get_id() const {
        return id;
    }

    RioAlloc& set_id(const std::string& value) {
        id = value;
        return *this;
    }

  private:

    unsigned int offset;
    unsigned int size;
    std::string id;

};

}

#endif
