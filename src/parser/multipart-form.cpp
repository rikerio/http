#include "parser/multipart-form.h"

#include "utils.h"
#include "vector"
#include "spdlog/spdlog.h"

using namespace automation::http::parser;

ContentDisposition::ContentDisposition(std::string_view orig) {

    std::vector<std::string_view> value_parts_list;

    Utils::Split(orig, ';', value_parts_list);

    if (value_parts_list.size() > 0 ) {
        disposition_type = value_parts_list[0];
    }

    for (unsigned int i = 1; i < value_parts_list.size(); i += 1) {

        std::vector<std::string_view> params_list;
        Utils::Split(value_parts_list[i], '=', params_list);

        if (params_list.size() == 2) {

            DispositionParam params;

            params.key = Utils::trim(params_list[0]);
            params.value = Utils::trim(params_list[1].substr(1, params_list[1].size() - 2));

            disposition_param_list.push_back(params);

        }

    }

}

std::string_view ContentDisposition::get_header(std::string_view key) const {

    for (auto h : disposition_param_list) {
        if (Utils::equals_lower(h.key, key)) {
            return h.value;
        }
    }

    return std::string_view({nullptr}, 0);

}

bool ContentDisposition::has_header(std::string_view key) const {

    for (auto h : disposition_param_list) {
        if (Utils::equals_lower(h.key, key)) {
            return true;
        }
    }

    return false;

}

MultipartFormDataPart::MultipartFormDataPart(std::string_view chunk) {

    size_t len = chunk.size();

    size_t i = 0;
    size_t mark = 0;
    char c, cl;
    int is_last = 0;

    size_t data_start = 0;

    std::string_view current_header;

    State state = State::header_field_start;

    while(i < len) {
        c = chunk.data()[i];
        is_last = (i == (len - 1));
        switch (state) {
        case State::header_field_start:
            //spdlog::debug("s_header_field_start");
            mark = i;
            state = State::header_field;

        /* fallthrough */
        case State::header_field:
            //spdlog::debug("s_header_field");
            if (c == CR) {
                state = State::headers_almost_done;
                break;
            }

            if (c == ':') {
                current_header = chunk.substr(mark, i - mark);
                //spdlog::debug("current header = {}", current_header);
                state = State::header_value_start;
                break;
            }

            cl = tolower(c);
            if ((c != '-') && (cl < 'a' || cl > 'z')) {
                spdlog::debug("invalid character in header name");
                return;
            }
            if (is_last)
                current_header = chunk.substr(mark, (i-mark) + 1);
            break;

        case State::headers_almost_done:
            //spdlog::debug("s_headers_almost_done");
            if (c != LF) {
                return;
            }

            state = State::part_data_start;
            break;

        case State::header_value_start:
            //spdlog::debug("s_header_value_start");
            if (c == ' ') {
                break;
            }

            mark = i;
            state = State::header_value;

        /* fallthrough */
        case State::header_value:
            //spdlog::debug("s_header_value");
            if (c == CR) {
                if (Utils::equals_lower(current_header, "content-disposition")) {
                    content_disposition = new ContentDisposition(Utils::trim(chunk.substr(mark, i - mark)));
                } else {
                    header_list.push_back({ current_header, Utils::trim(chunk.substr(mark, i - mark)) });
                    spdlog::debug("{} = {}", current_header, Utils::trim(chunk.substr(mark, i-mark)));
                }
                state = State::header_value_almost_done;
                break;
            }
            if (is_last) {
                if (Utils::equals_lower(current_header, "content-disposition")) {
                    content_disposition = new ContentDisposition(Utils::trim(chunk.substr(mark, i - mark)));
                } else {
                    header_list.push_back({current_header, Utils::trim(chunk.substr(mark, (i-mark) + 1))});
                    spdlog::debug("{} = {}", current_header, chunk.substr(mark, (i-mark) + 1));
                }
            }
            break;

        case State::header_value_almost_done:
            //spdlog::debug("s_header_value_almost_done");
            if (c != LF) {
                return;
            }
            state = State::header_field_start;
            break;

        case State::part_data_start:
            //spdlog::debug("s_part_data_start");
            mark = i;
            data_start = i;
            state = State::part_data;

        /* fallthrough */
        case State::part_data:
            if (is_last) {
                data = chunk.substr(data_start, (i - data_start) - 3);
                // spdlog::debug("{}", Utils::trim(chunk.substr(mark, (i-mark) - 2)));
                return;
            }
            break;

        default:
            spdlog::debug("Multipart parser unrecoverable error");
            return;
        }
        ++ i;
    }

}

MultipartFormDataPart::~MultipartFormDataPart() {
    if (content_disposition == nullptr) {
        free(content_disposition);
    }
}

std::string_view MultipartFormDataPart::get_data() const {
    return data;
}

std::string_view MultipartFormDataPart::get_header(std::string_view key) const {

    for (auto h : header_list) {

        if (Utils::equals_lower(h.key, key)) {
            return h.value;
        }

    }

    return std::string_view({nullptr}, 0);

}

const ContentDisposition& MultipartFormDataPart::get_content_disposition() const {
    return *content_disposition;
}


MultipartFormData::MultipartFormData(const std::string_view content_type, const std::string_view body_view) : content_type(content_type), body_content(body_view) {

    parse_body();

}

const std::vector<MultipartFormDataPart>& MultipartFormData::get_data() const {
    return data_vector;
}

void MultipartFormData::parse_body() {

    /* get boundary, the complicated way :-) */

    std::string ct_copy = std::string(content_type);
    std::vector<std::string> ct_parts;
    std::vector<std::string> boundary_parts;
    Utils::Split(ct_copy, ';', ct_parts);
    Utils::Split(ct_parts[1], '=', boundary_parts);
    boundary_string = boundary_parts[1];

    std::string start_boundary = "--" + boundary_string;
    std::string final_boundary = boundary_string + "--";

    size_t start = 0;
    size_t size = body_content.size();
    size_t i = 0, count = 0;
    size_t boundary_size = boundary_string.size() + 2;

    bool started = false;

    while (i < size) {

        std::string_view sstr = body_content.substr(i, boundary_size);

        if (sstr == start_boundary && !started) {
            start = i + boundary_size;
            i += boundary_size;
            count = 0;
            started = true;
            continue;
        }

        if (sstr == start_boundary || sstr == final_boundary) {
            if (started) {
                spdlog::debug("adding part data");
                std::string_view part_data_view = body_content.substr(start + 2, count); // +2 = LFCR
                MultipartFormDataPart part_data(part_data_view);
                data_vector.push_back(part_data);
                start = i + boundary_size;
                count = 0;
                i += boundary_size;
                continue;
            }
        }

        count += 1;
        i += 1;

    }

}
