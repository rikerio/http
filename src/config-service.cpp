#include "http/config-service.h"
#include "fstream"
#include "spdlog/spdlog.h"

using namespace depot::http::service;

Config::Config(uWS::App& app, const std::string& filename) :
    filename(filename),
    http_app(app) {

    json_builder["indentation"] = ""; // No whitespace in json output

    json_reader = json_reader_builder.newCharReader();

    root_node = YAML::LoadFile(filename);

    http_app.get("/api/1/config/:key",
                 bind(&Config::get_data_by_key, this, std::placeholders::_1, std::placeholders::_2));

    http_app.get("/api/1/config",
                 bind(&Config::get_data_by_filter, this, std::placeholders::_1, std::placeholders::_2));

    http_app.post("/api/1/config/:key",
                  bind(&Config::update_data_by_key, this, std::placeholders::_1, std::placeholders::_2));

    http_app.post("/api/1/config",
                  bind(&Config::update_bulk_data, this, std::placeholders::_1, std::placeholders::_2));

}

Config::~Config() {
    serialize();
}

void Config::serialize() const {

    std::ofstream fout(filename);
    fout << root_node;

}

void Config::get_data_by_key(uWS::HttpResponse<false>* res, uWS::HttpRequest* req) {

    std::string key(req->getParameter(0));

    YAML::Node key_node = root_node[key];

    if (!key_node) {
        res->writeStatus("404 Not Found")->end();
        return;
    }

    std::string json_str = "{\"key\":\"" + key + "\",\"value\":" + key_node.as<std::string>() + "}";

    res->writeHeader("Content-Type", "application/json; charset=utf-8")->end(json_str);

}

void Config::get_data_by_filter(uWS::HttpResponse<false>* res, uWS::HttpRequest*) {

    Json::Value json;

    for (auto yaml_node : root_node) {
        Json::Value item;
        std::string json_key = yaml_node.first.as<std::string>();
        std::string json_value = yaml_node.second.as<std::string>();

        std::string json_string = "{\"key\":\"" + json_key + "\",\"value\":" + json_value +"}";

        json_reader->parse(json_string.c_str(), json_string.c_str() + json_string.length(), &item, NULL);
        json.append(item);

    }

    std::string res_str = Json::writeString(json_builder, json);
    res->writeHeader("Content-Type", "application/json; charset=utf-8")->end(res_str);

}

void Config::update_data_by_key(uWS::HttpResponse<false>* res, uWS::HttpRequest* req) {

    std::string *buffer = new std::string();
    std::string *key = new std::string(req->getParameter(0));
    spdlog::debug("POST /api/1/config/{}", *key);



    res->onAborted([]() {  });
    res->onData([&, buffer, key, res](std::string_view data, bool last) {

        buffer->append(data.data(), data.size());

        if (!last) {
            return;
        }

        YAML::Node yaml_node = root_node[*key];

        if (!root_node[*key]) {
            res->writeStatus(http_status_not_found)->end();
            return;
        }

        spdlog::debug("POST /api/1/config/{} data {}", *key, *buffer);

        try {

            Json::Value json;

            if (!json_reader->parse(buffer->c_str(), buffer->c_str() + buffer->length(), &json, NULL)) {
                res->writeStatus(http_status_bad_request)->end();
                return;
            }

            if (!json || !json["value"]) {
                res->writeStatus(http_status_bad_request)->end();
                return;
            }

            root_node[*key] = json["value"].as<std::string>();

            res->writeStatus(http_status_ok)->end();

        } catch (...) {
            spdlog::debug("POST /api/1/config/{} internal error.", *key);
            res->writeStatus(http_status_internal_error)->end();
        }

        delete(buffer);
        delete(key);

    });



    res->end();

}

void Config::update_bulk_data(uWS::HttpResponse<false>* res, uWS::HttpRequest*) {

    std::cout << "update bulkd data" << std::endl;

    res->end();

}
