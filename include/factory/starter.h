#ifndef __AUTOMATION_HTTP_MODEL_STARTERFACTORY_H__
#define __AUTOMATION_HTTP_MODEL_STARTERFACTORY_H__

#include "factory/yaml-file.h"
#include "factory/app.h"
#include "event-emitter.h"
#include "models/starter.h"


namespace automation::http::factory {

class Starter : public YamlFile<model::Starter> {

  public:
    Starter(EventEmitter&, const std::string&, App&);
    ~Starter();

    std::shared_ptr<model::Starter> add(model::Starter& model) override;
    std::shared_ptr<model::Starter> update(const std::string& id, model::Starter& model) override;

  private:

    App& app_factory;

    YAML::Node to_yaml(const model::Starter&) const;
    model::Starter from_yaml(YAML::Node) const;
    bool filter(const model::Starter&, const std::string&) const;
    int compare(const std::string& field, const model::Starter&, const model::Starter&) const;

};

}


#endif
