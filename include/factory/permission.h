#ifndef __AUTOMATION_HTTP_MODEL_PERMISSIONFACTORY_H__
#define __AUTOMATION_HTTP_MODEL_PERMISSIONFACTORY_H__

#include "string"
#include "set"
#include "fstream"

#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#include "event-emitter.h"
#include "factory/yaml-file.h"
#include "models/permission.h"

namespace automation::http::factory {

class Permission : public YamlFile<model::Permission> {

  public:

    class no_permission_error : public std::exception { };

  public:

    Permission(EventEmitter& emitter, const std::string& filename);
    ~Permission();

    std::vector<std::shared_ptr<model::Permission>> get_all_by_user(const std::string& user_id) const {

        std::vector<std::shared_ptr<model::Permission>> result_list;

        auto upper_it = user_permission_list.upper_bound(user_id);
        auto lower_it = user_permission_list.lower_bound(user_id);
        while (lower_it != upper_it) {

            if (lower_it->first == user_id) {
                result_list.push_back(lower_it->second);
            }

            lower_it++;

        }

        return result_list;

    }

    std::shared_ptr<model::Permission> add(model::Permission& permission) override {

        auto res = factory::YamlFile<model::Permission>::add(permission);

        user_permission_list.insert({ permission.get_user_id(), res });

        return res;

    }

    std::shared_ptr<model::Permission> remove(const std::string& id) override {

        auto model = factory::YamlFile<model::Permission>::remove(id);

        auto itr1 = user_permission_list.lower_bound(model->get_user_id());
        auto itr2 = user_permission_list.upper_bound(model->get_user_id());

        while (itr1 != itr2) {
            if (itr1->second->get_id() == id) {
                user_permission_list.erase(itr1);
                break;
            }
            itr1++;
        }

        return model;

    }

  protected:

    void deserialize() override;

  private:

    std::multimap<std::string, std::shared_ptr<model::Permission>> user_permission_list;

    YAML::Node to_yaml(const model::Permission&) const;
    model::Permission from_yaml(YAML::Node) const;
    bool filter(const model::Permission&, const std::string&) const;
    int compare(const std::string&, const model::Permission&, const model::Permission&) const;

};

}

#endif
