#include "utils.h"
#include <algorithm>
#include <functional>
#include <random>
#include <sstream>
#include <string.h>

using namespace automation::http;

inline bool Utils::case_ins_char_comp_single(char a, char b) {
  return (toupper(a) == b);
}

std::string::const_iterator Utils::case_ins_find(std::string &s,
                                                 const std::string &p) {
  std::string tmp;

  transform(p.begin(), p.end(), // Make the pattern
            back_inserter(tmp), // upper-case
            toupper);

  return (search(s.begin(), s.end(),                 // Return the iter-
                 tmp.begin(), tmp.end(),             // ator returned by
                 Utils::case_ins_char_comp_single)); // search
}

bool Utils::pattern_match(const std::string_view &str,
                          const std::string_view &pattern) {
  unsigned int n = str.length();
  unsigned int m = pattern.length();

  // empty pattern can only match with
  // empty string
  if (m == 0)
    return (n == 0);

  // lookup table for storing results of
  // subproblems
  bool lookup[n + 1][m + 1];

  // initailze lookup table to false
  memset(lookup, false, sizeof(lookup));

  // empty pattern can match with empty string
  lookup[0][0] = true;

  // Only '*' can match with empty string
  for (unsigned int j = 1; j <= m; j++)
    if (pattern[j - 1] == '*')
      lookup[0][j] = lookup[0][j - 1];

  // fill the table in bottom-up fashion
  for (unsigned int i = 1; i <= n; i++) {
    for (unsigned int j = 1; j <= m; j++) {
      // Two cases if we see a '*'
      // a) We ignore ‘*’ character and move
      //    to next  character in the pattern,
      //     i.e., ‘*’ indicates an empty sequence.
      // b) '*' character matches with ith
      //     character in input
      if (pattern[j - 1] == '*')
        lookup[i][j] = lookup[i][j - 1] || lookup[i - 1][j];

      // Current characters are considered as
      // matching in two cases
      // (a) current character of pattern is '?'
      // (b) characters actually match
      else if (pattern[j - 1] == '?' || str[i - 1] == pattern[j - 1])
        lookup[i][j] = lookup[i - 1][j - 1];

      // If characters don't match
      else
        lookup[i][j] = false;
    }
  }

  return lookup[n][m];
}

std::vector<std::string> &Utils::Split(const std::string &s, char delim,
                                       std::vector<std::string> &elems) {

  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }

  return elems;
}

void Utils::Split(const std::string_view s, char delim,
                  std::vector<std::string_view> &elems) {

  unsigned int index = 0;
  unsigned int mark = 0;
  unsigned int count = 1;
  while (index < s.length()) {

    char cur_char = s[index];

    if (cur_char != delim && index == s.length() - 1 && count > 0) {
      elems.push_back(s.substr(mark, count));
      break;
    }

    if (cur_char == delim) {
      if (count > 0) {
        elems.push_back(s.substr(mark, count - 1));
        mark = index + 1;
        count = 0;
      }
    }

    index += 1;
    count += 1;
  }
}

// given a function that generates a random character,
// return a string of the requested length
std::string random_string(size_t length, std::function<char(void)> rand_char) {
  std::string str(length, 0);
  std::generate_n(str.begin(), length, rand_char);
  return str;
}

const std::string Utils::create_random_token(unsigned int len) {

  typedef std::vector<char> char_array;

  static const char_array global_ch_set{
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b',
      'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

  const auto &ch_set = global_ch_set;

  // 1) create a non-deterministic random number generator
  std::default_random_engine rng(std::random_device{}());

  // 2) create a random number "shaper" that will give
  //    us uniformly distributed indices into the character set
  std::uniform_int_distribution<> dist(0, ch_set.size() - 1);

  // 3) create a function that ties them together, to get:
  //    a non-deterministic uniform distribution from the
  //    character set of your choice.
  auto randchar = [&ch_set, &dist, &rng]() { return ch_set[dist(rng)]; };

  // 4) set the length of the string you want and profit!
  return random_string(len, randchar);
}

std::string Utils::to_lower(const std::string &str) {

  std::string lower_str = str;
  std::transform(lower_str.begin(), lower_str.end(), lower_str.begin(),
                 [&](unsigned char c) { return std::tolower(c); });

  return lower_str;
}

std::string Utils::to_upper(const std::string &str) {

  std::string upper_str = str;
  std::transform(upper_str.begin(), upper_str.end(), upper_str.begin(),
                 [&](unsigned char c) { return std::toupper(c); });

  return upper_str;
}

std::string Utils::remove_chars(const std::string_view str,
                                unsigned char remove_c) {

  std::string new_string;

  std::copy_if(str.begin(), str.end(), std::back_inserter(new_string),
               [&remove_c](char c) { return c != remove_c; });

  return new_string;
}

std::string_view Utils::trim(const std::string_view str) {

  std::string_view result = str;

  result.remove_prefix(std::min(result.find_first_not_of(' '), result.size()));

  return result;
}

bool Utils::equals_lower(std::string_view a, std::string_view b) {

  if (a.size() != b.size()) {
    return false;
  }

  for (unsigned int i = 0; i < a.size(); i += 1) {
    if (tolower(a[i]) != tolower(b[i])) {
      return false;
    }
  }

  return true;
}
