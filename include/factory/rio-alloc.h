#ifndef __AUTOMATION_HTTP_FACTORY_RIO_ALLOCATION_H__
#define __AUTOMATION_HTTP_FACTORY_RIO_ALLOCATION_H__

#include "stdexcept"
#include "rikerio-automation/automation.h"
#include "spdlog/spdlog.h"

#include "factory/cache.h"
#include "models/rio-alloc.h"

namespace automation::http::factory {

class RioAlloc : public Cached<model::RioAlloc> {

  public:

    RioAlloc(EventEmitter&, std::string, RikerIO::Profile&);

    std::shared_ptr<model::RioAlloc> add(model::RioAlloc&);

    std::shared_ptr<model::RioAlloc> update(const std::string&, model::RioAlloc&);
    std::shared_ptr<model::RioAlloc> remove(const std::string&);
    std::shared_ptr<model::RioAlloc> get_one(const std::string&);

  private:

    RikerIO::Profile& rio_profile;
    std::filesystem::file_time_type rio_last_write;

    bool filter(const model::RioAlloc&, const std::string&) const;
    int compare(const std::string&, const model::RioAlloc&, const model::RioAlloc&) const;

    void update_cache() override;

};

}

#endif
