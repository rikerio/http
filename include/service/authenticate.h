#ifndef __AUTOMATION_HTTP_SERVICE_AUTHENTICATION_H__
#define __AUTOMATION_HTTP_SERVICE_AUTHENTICATION_H__

#include "xapp.h"
#include "factory/user.h"
#include "factory/key.h"
#include "factory/permission.h"
#include "service/jwt.h"
#include "parser/json.h"

namespace automation::http::service {

class Authenticate {

  public:

    Authenticate(
        factory::User& user_factory,
        factory::Permission& permission_factory,
        factory::Key& key_factory,
        factory::JWT& jwt_factory);

    ~Authenticate();

    automation::http::ExtendedApp::Handler authenticate();

    void auth_with_credentials(
        std::shared_ptr<ExtendedRequest> req,
        std::shared_ptr<ExtendedResponse> res,
        Json::Value username,
        Json::Value password);
    void auth_with_key(
        std::shared_ptr<ExtendedRequest> req,
        std::shared_ptr<ExtendedResponse> res,
        Json::Value json_key);

    automation::http::ExtendedApp::Handler is_authenticated();

  private:

    factory::User& user_factory;
    factory::Permission& permission_factory;
    factory::Key& key_factory;
    factory::JWT& jwt_factory;

    void send_response(
        std::shared_ptr<model::User> user,
        std::shared_ptr<ExtendedResponse> res);

    Json::Value to_json(const model::User& user, const std::vector<std::shared_ptr<model::Permission>>&) const;
};

}


#endif
