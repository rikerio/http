#include "factory/rio-data.h"

using namespace automation::http;

factory::RioData::RioData(EventEmitter& emitter, std::string event_prefix, RikerIO::Profile& rio_profile) :
    factory::Cached<model::RioData>(emitter, event_prefix),
    rio_profile(rio_profile) {
    update_cache();
}

std::shared_ptr<model::RioData> factory::RioData::add(model::RioData&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioData> factory::RioData::update(const std::string&, model::RioData&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioData> factory::RioData::remove(const std::string&) {
    throw std::runtime_error("Not implemented");
    return nullptr;
}

std::shared_ptr<model::RioData> factory::RioData::get_one(const std::string&) {
    throw std::runtime_error("Not implemented");
    return nullptr;

}

bool factory::RioData::filter(const model::RioData& data, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != data.get_id().find(value)) {
        return true;
    }

    if (std::string::npos != data.get_type().find(value)) {
        return true;
    }

    return false;


}

int factory::RioData::compare(const std::string& field, const model::RioData& a, const model::RioData& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "byte_offset") {
        if (a.get_byte_offset() < b.get_byte_offset()) {
            return -1;
        } else if (a.get_byte_offset() > b.get_byte_offset()) {
            return 1;
        } else {
            if (a.get_bit_index() < b.get_bit_index()) {
                return -1;
            } else if (a.get_bit_index() > b.get_bit_index()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    if (field == "bit_size") {
        if (a.get_bit_size() < b.get_bit_size()) {
            return -1;
        } else if (a.get_bit_size() > b.get_bit_size()) {
            return 1;
        } else {
            return 0;
        }
    }

    return 0;

}

void factory::RioData::update_cache() {

    std::filesystem::file_time_type new_last_write;

    spdlog::debug("RioDataFactory::update_cache() : Checking rikerio for updates.");
    if (RikerIO::last_write(rio_profile, new_last_write) == RikerIO::result_error) {
        spdlog::error("RioDataFactory::update_cache() : Error receiving last write time.");
    };

    if (new_last_write == rio_last_write) {
        spdlog::debug("RioDataFactory::update_cache() : No updates available, cache stays unchanged.");
        return;
    }

    spdlog::debug("RioDataFactory::update_cache() : Updates available!");

    rio_last_write = new_last_write;

    std::vector<std::string> id_list;
    if (RikerIO::Data::list(rio_profile, id_list) == RikerIO::result_error) {
        spdlog::error("Error updating RikerIO Data Points list.");
        return;
    }

    std::vector<RikerIO::DataPoint> data_list;
    for (auto& id : id_list) {

        RikerIO::DataPoint dp;
        if (RikerIO::Data::get(rio_profile, id, dp) == RikerIO::result_error) {
            spdlog::error("Error getting RikerIO Data Point {}.", id);
        }

        data_list.push_back(dp);

    }

    t_map.clear();
    query_cache_map.clear();

    for (auto& a : data_list) {
        auto ptr_a = std::make_shared<model::RioData>(a);
        t_map[a.id] = ptr_a;
    }


}
