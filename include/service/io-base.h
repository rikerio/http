#ifndef __AUTOMATION_HTTP_SERVICE_IO_BASE_H__
#define __AUTOMATION_HTTP_SERVICE_IO_BASE_H__

#include "spdlog/spdlog.h"
#include "map"
#include "set"
#include "string"
#include "bitset"
#include "string.h"
#include "rikerio-automation/automation.h"

#include "base64.h"

namespace automation::http::service {

class IoBase {

  public:
    IoBase() { }

  protected:

    struct InterpreterContext {
        std::string io_task_id;
        std::string id;
        RikerIO::DataPoint dp;
    };


    static const std::string translate_buffer(
        unsigned int byte_size,
        uint8_t* buffer,
        const RikerIO::DataPoint& dp) {


        auto type = dp.type;
        std::string result = "";

        if (type == RikerIO::Type::BIT) {
            //result.type_as_string = "boolean";
            std::bitset<8> tmp_bs(*buffer);
            return tmp_bs[dp.bit_index] ? "true" : "false";
            //result.size = 1;
        } else  if (type == RikerIO::Type::BOOL) {
            //result.type_as_string = "boolean";
            bool tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return tmp ? "true" : "false";
            //result.size = 8;
        } else if (type == RikerIO::Type::INT8) {
            //result.type_as_string = "int8";
            int8_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 8;
        } else if (type == RikerIO::Type::UINT8) {
            //result.type_as_string = "uint8";
            uint8_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 8;
        } else if (type == RikerIO::Type::INT16) {
            //result.type_as_string = "int16";
            int16_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 16;
        } else if (type == RikerIO::Type::UINT16) {
            //result.type_as_string = "uint16";
            uint16_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 16;
        } else if (type == RikerIO::Type::INT32) {
            //result.type_as_string = "int32";
            int32_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 32;
        } else if (type == RikerIO::Type::UINT32) {
            //result.type_as_string = "uint32";
            uint32_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 32;
        } else if (type == RikerIO::Type::INT64) {
            //result.type_as_string = "int64";
            int64_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 64;
        } else if (type == RikerIO::Type::UINT64) {
            //result.type_as_string = "uint64";
            uint64_t tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 64;
        } else if (type == RikerIO::Type::FLOAT) {
            //result.type_as_string = "float";
            float tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 32;
        } else if (type == RikerIO::Type::DOUBLE) {
            //result.type_as_string = "double";
            double tmp;
            memcpy(&tmp, buffer, sizeof(tmp));
            return std::to_string(tmp);
            //result.size = 64;
        } else if (type == RikerIO::Type::STRING) {
            //result.type_as_string = "string";
            char* tmp = (char*) calloc(1, byte_size);
            memcpy(&tmp, buffer, byte_size);
            std::string res = std::string(tmp);
            free(tmp);
            return res;
            //result.size = 8 * byte_size;
        } else {
            //result.type_as_string = "binary";

            return base64_encode((const unsigned char*) buffer, (unsigned int) byte_size);
            //result.size = 8 * byte_size;
        }

        //spdlog::debug("Update::data_update({},...) : Value = {}, Type = {}.", id, value_as_string, type_as_string);
    }

};

}

#endif
