#ifndef __DEPOT_AUTOMATION_HTTP_REDIRECT_SERVICE_H__
#define __DEPOT_AUTOMATION_HTTP_REDIRECT_SERVICE_H__

#include "xapp.h"
#include "spdlog/spdlog.h"

namespace automation::http::service {

class Redirect {
  public:

    static automation::http::ExtendedApp::Handler permanent(std::string target) {
        return [target](auto res, auto req) {
            spdlog::debug("{} : Redirecting to {}", req->get_url(), target);
            res->set_status("308 Permanent Redirect").set_header("Location", target);
        };
    }

    static automation::http::ExtendedApp::Handler temporary(std::string target) {
        //std::string* target_ptr = new std::string(target);
        return [target=target](auto res, auto req) {
            spdlog::debug("{}:{} : Redirecting to {}", req->get_method(), req->get_url(), target);
            res->set_status("307 Temporary Redirect").set_header("Location", target);
        };
    }

};

};

#endif
