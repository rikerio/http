#include "parser/json.h"

using namespace automation::http::parser;

Json::Value json::parse(const std::string_view view) {

    static Json::CharReaderBuilder builder;
    static std::unique_ptr<Json::CharReader> reader(builder.newCharReader());

    Json::Value root;
    JSONCPP_STRING err;
    auto raw_json_length = static_cast<int>(view.length());
    auto start = view.data();
    auto end = start + raw_json_length;
    if (!reader->parse(start, end, &root, &err)) {
        throw automation::http::ExtendedApp::bad_request_error();
    }

    return root;

}

std::string json::stringify(const Json::Value json) {
    static Json::StreamWriterBuilder json_write_builder;

    json_write_builder["indentation"] = "";

    return Json::writeString(json_write_builder, json);

}
