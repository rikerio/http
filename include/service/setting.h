#ifndef __AUTOMATION_HTTP_SERVICE_SETTING_H__
#define __AUTOMATION_HTTP_SERVICE_SETTING_H__

#include "service/service.h"
#include "factory/setting.h"
#include "xapp.h"

namespace automation::http::service {

class Setting : public FactoryService<factory::Setting, model::Setting> {

  public:

    Setting(const std::string&, factory::Setting& factory);

  protected:

    model::Setting from_json(Json::Value, const std::string&) const;
    Json::Value to_json(const model::Setting&) const;

};

}

#endif
