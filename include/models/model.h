#ifndef __AUTOMATION_HTTP_MODEL_H__
#define __AUTOMATION_HTTP_MODEL_H__

#include "string"
#include "stdexcept"

namespace automation::http::model {

class Base {
  public:
    virtual ~Base() { }
    virtual const std::string& get_id() const = 0;
};

}

#endif
