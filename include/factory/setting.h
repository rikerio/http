#ifndef __AUTOMATION_HTTP_MODEL_SETTINGFACTORY_H__
#define __AUTOMATION_HTTP_MODEL_SETTINGFACTORY_H__

#include "string"
#include "set"
#include "fstream"

#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#include "factory/yaml-file.h"
#include "event-emitter.h"
#include "models/setting.h"

namespace automation::http::factory {

class Setting : public YamlFile<model::Setting> {

  public:

    Setting(EventEmitter&, const std::string&);
    ~Setting();

    std::shared_ptr<automation::http::model::Setting> add(automation::http::model::Setting& setting) override;

  private:

    YAML::Node to_yaml(const model::Setting&) const;
    model::Setting from_yaml(YAML::Node) const;
    bool filter(const model::Setting&, const std::string&) const;
    int compare(const std::string& field, const model::Setting&, const model::Setting&) const;

    const std::string provide_unique_id() const override {
        throw not_implemented_error();
    }

};

}

#endif
