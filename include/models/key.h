#ifndef __AUTOMATION_HTTP_MODEL_KEY_H__
#define __AUTOMATION_HTTP_MODEL_KEY_H__

#include "string"
#include "yaml-cpp/yaml.h"
#include "json/json.h"

namespace automation::http::model {

class Key {
  public:

    Key() :
        id(""), user_id(""), disabled(false) { }

    const std::string& get_id() const {
        return id;
    }

    Key& set_id(const std::string& value) {
        id = value;
        return *this;
    }

    const std::string& get_user_id() const {
        return user_id;
    }

    Key& set_user_id(const std::string& value) {
        user_id = value;
        return *this;
    }

    bool is_disabled() const {
        return disabled;
    }

    Key& set_disabled(bool value) {
        disabled = value;
        return *this;
    }

  private:

    std::string id;
    std::string user_id;

    bool disabled;

};

}

#endif
