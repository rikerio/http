#ifndef __DEPOT_AUTOMATION_HTTP_UTILS_H__
#define __DEPOT_AUTOMATION_HTTP_UTILS_H__

#include "string"
#include "vector"

namespace automation::http {

class Utils {

  public:

    static bool case_ins_char_comp_single(char, char);
    static std::string::const_iterator case_ins_find(std::string&, const std::string&);


    static bool pattern_match(const std::string_view& str, const std::string_view& pattern);
    static std::vector<std::string>& Split (const std::string&, char, std::vector<std::string>&);
    static void Split (const std::string_view, char, std::vector<std::string_view>&);

    static const std::string create_random_token(unsigned int len);
    static std::string to_lower(const std::string&);
    static std::string to_upper(const std::string&);

    static std::string remove_chars(const std::string_view, unsigned char);
    static std::string_view trim(const std::string_view);
    static bool equals_lower(std::string_view, std::string_view);

};

}

#endif
