#ifndef __AUTOMATION_HTTP_JSON_LIST_RESPONSE_H__
#define __AUTOMATION_HTTP_JSON_LIST_RESPONSE_H__

#include "xapp.h"
#include "vector"
#include "memory"
#include "functional"
#include "response-data-handler.h"
#include "parser/json.h"
#include "json/json.h"

namespace automation::http::json {

template <typename T>
class ListResponse : public ResponseHandler {

  public:

    ListResponse(
        const std::string& base_uri,
        std::function<Json::Value(T&)> jsonify) :
        base_uri(base_uri),
        jsonify(jsonify)  {

    }

    void set_response_body(ExtendedRequest& req, ExtendedResponse& res) const {
        const std::string page_string = req.get_query("page", "0");
        const std::string page_size_string = req.get_query("page_size", "25");
        const std::string filter_string = req.get_query("filter", "");
        const std::string sort_string = req.get_query("sort", "");
        const std::string sort_order_string = req.get_query("sort_order", "asc");

        spdlog::debug("{}:{} : page={},page_size={},filter={},sort={},sort_order={}", req.get_method(), req.get_url(),page_string,page_size_string,filter_string,sort_string,sort_order_string);

        int page = stoi(page_string);
        int page_size = stoi(page_size_string);

        if (page < 1) {
            throw automation::http::ExtendedApp::bad_request_error("Invalid page.");
        }

        if (page_size == 0 || page_size < -1) {
            throw automation::http::ExtendedApp::bad_request_error("Invalid page size.");
        }

        bool descending = sort_order_string == "desc";

        Json::Value result = Json::objectValue;
        Json::Value meta = Json::objectValue;
        Json::Value results = Json::arrayValue;

        unsigned int page_count = page_size == -1 ? 1 : std::ceil((float)list.size() / (float)page_size);

        unsigned int end = 0;
        unsigned int start = 0;

        if (page == 0 && page_size == -1) {
            end = result.size();
            start = 0;
        } else {
            end = page * page_size;
            start = (page - 1) * page_size;
        }

        spdlog::debug("{}:{} : Serving from index {} to index {}.", req.get_method(), req.get_url(), start, end);

        meta["page"] = page;
        meta["page_size"] = page_size;
        meta["page_count"] = page_count;
        meta["total_count"] = list.size();
        meta["filter"] = filter_string;
        meta["sort_by"] = sort_string;
        meta["sort_order"] = !descending ? "asc" : "desc";

        if (start < list.size()) {

            auto start_it = list.begin() + start;
            auto end_it = list.begin() + end;

            for (auto it = start_it; it != list.end() && it != std::next(end_it); ++it) {
                std::string t_id = (*it)->get_id();
                Json::Value json_t = jsonify(**it);
                Json::Value obj_t;
                Json::Value meta_t;
                meta_t["uri"] = base_uri + "/" + t_id;
                obj_t["meta"] = meta_t;
                obj_t["data"] = json_t;

                results.append(obj_t);

            };

        }

        result["meta"] = meta;
        result["results"] = results;

        res.set_header("Content-Type", "application/json; charset=utf-8");
        res.set_data(automation::http::parser::json::stringify(result));

    }

    void set_data(std::vector<std::shared_ptr<T>> value) {
        list = value;
    }

    void filter(std::function<bool(MetaObject&)> filter_handler) {

        std::vector<unsigned int> remove_list;

        for (unsigned int i = 0; i < list.size(); i += 1) {
            auto t = list[i];
            MetaObject mo(base_uri + "/" + t->get_id());
            if (filter_handler(mo)) {
                remove_list.push_back(i);
            }
        }

        for (int j = remove_list.size() - 1; j >= 0; j -= 1) {
            list.erase(list.begin() + remove_list[j]);
        }


    }

  private:

    const std::string base_uri;
    std::function<Json::Value(T&)> jsonify;
    std::vector<std::shared_ptr<T>> list;

};

}



#endif
