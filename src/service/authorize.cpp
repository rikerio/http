#include "service/authorize.h"

#include "response-data-handler.h"

using namespace automation::http::service;

Authorize::xHandler Authorize::authorize_handler(
    std::function<void(
        std::shared_ptr<automation::http::ExtendedResponse>,
        std::shared_ptr<automation::http::ExtendedRequest>)> reject) {
    return [reject](auto res, auto req) {

        auto permission_list = req->get_permission_list();

        // form url for permission check
        const std::string method_url = std::string(req->get_method()) + ":" + std::string(req->get_url());

        // whitelist
        bool request_whitelisted = false;
        spdlog::debug("{}:{} : Trying to match with {} patterns.", req->get_method(), req->get_url(), permission_list.size());

        for (auto permission : permission_list) {

            if (permission->get_type() == model::Permission::Type::DENY) {
                continue;
            }

            if (permission->is_disabled()) {
                continue;
            }

            const std::string pattern = permission->get_method_pattern() + ":" + permission->get_uri_pattern();
            spdlog::debug("{}:{} : Matching {} with {}.", req->get_method(), req->get_url(), req->get_url(), pattern);
            if (Utils::pattern_match(method_url, pattern)) {
                spdlog::debug("{}:{} : Request whitelisted by pattern {}.", req->get_method(), req->get_url(), pattern);
                request_whitelisted = true;
                return;
            }

        }

        if (!request_whitelisted) {
            spdlog::debug("{}:{} : Request did not pass whitelist.", req->get_method(), req->get_url());
            spdlog::debug("{}:{} : Executing reject handler.", req->get_method(), req->get_url());
            reject(res, req);
            return;
        }

        for (auto permission : permission_list) {

            if (permission->get_type() == model::Permission::Type::ALLOW) {
                continue;
            }

            if (permission->is_disabled()) {
                continue;
            }

            const std::string pattern = permission->get_method_pattern() + ":" + permission->get_uri_pattern();

            if (Utils::pattern_match(method_url, pattern)) {
                spdlog::debug("{}:{} : Request did not pass blacklist {}.", req->get_method(), req->get_url(), pattern);
                spdlog::debug("{}:{} : Executing exit handler.", req->get_method(), req->get_url());
                reject(res, req);
                return;

            }
        }
    };
}

Authorize::xHandler Authorize::filter() {
    return [&](auto res, auto req) {

        spdlog::debug("{}:{} : Filtering response list.", req->get_method(), req->get_url());

        if (res->get_response_handler() == nullptr) {
            return;
        }

        auto permission_list = req->get_permission_list();
        auto rh = res->get_response_handler();

        if (!rh) {
            return;
        }

        rh->filter([&](auto& mo) -> bool{

            // form url for permission check
            const std::string method_url = "GET:" + mo.get_uri();
            // whitelist
            bool request_whitelisted = false;
            for (auto permission : permission_list) {

                if (request_whitelisted && permission->get_type() == model::Permission::Type::ALLOW) {
                    continue;
                }

                const std::string pattern = permission->get_method_pattern() + ":" + permission->get_uri_pattern();

                spdlog::debug("{}:{} - Matching {} against permission {}.", req->get_method(), req->get_url(), method_url, pattern);

                if (Utils::pattern_match(method_url, pattern)) {
                    if (permission->get_type() == model::Permission::Type::ALLOW) {
                        request_whitelisted = true;
                        continue;
                    } else {
                        // filter this one
                        //spdlog::debug("{}:{} - Filtering {}.", req->get_method(), req->get_url(), method_url);
                        return true;
                    }
                }

            }

            // !request_whitelisted == automatic DENY
            return !request_whitelisted;


        });

    };

}


Authorize::xHandler Authorize::authorize() {
    return authorize_handler([](auto, auto) -> xHandler {
        throw ExtendedApp::unauthorized_error();
    });
}


Authorize::xHandler Authorize::authorize(const std::string redir_url) {
    return authorize_handler([redir_url=redir_url](auto res, auto req) {
        automation::http::service::Redirect::temporary(redir_url)(res, req);
    });
}

Authorize::xHandler Authorize::authorize(
    std::function<xHandler(
        std::shared_ptr<automation::http::ExtendedResponse>,
        std::shared_ptr<automation::http::ExtendedRequest>)> handler) {
    return authorize_handler(handler);
};

automation::http::ExtendedApp::Handler Authorize::permit_by_username(const std::string user_id) {
    return [&](auto, auto req) {

        auto user = req->get_user();

        if (!user) {
            throw ExtendedApp::unauthorized_error();
        }

        if (user->get_id() != user_id) {
            throw ExtendedApp::unauthorized_error();
        }

    };
}

automation::http::ExtendedApp::Handler Authorize::permit_if_logged_in() {
    return [&](auto, auto req) {

        auto user = req->get_user();

        if (!user) {
            throw ExtendedApp::unauthorized_error();
        }

    };
}
