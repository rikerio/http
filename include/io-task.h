/* Task for copying data from the RT Thread into a non RT Thread.
 * The Copy happens byte wise, so no bits can be extracted directly.
 *
 * 1. add the read and write data you want to submit out of the RT Context by
 *    calling add_read_data<T>("rikerio link", callback) and add_write_data<T>("rikerio link", callback)
 * 2. When done, the callback will be called.
 *
 *
 */


#ifndef __AUTOMATION_HTTP_IO_TASK_H__
#define __AUTOMATION_HTTP_IO_TASK_H__

#include "spdlog/spdlog.h"
#include "ipc-message.h"
#include "read-job.h"
#include "write-job.h"

#include "rikerio-automation/automation.h"
#include "regex"
#include "set"

using namespace automation::rikerio;

namespace automation::http {

class IoTask : public automation::os::TaskInterface {

  public:

    using UpdateHandler = std::function<void(const std::string&, unsigned int, uint8_t*)>;

    class max_capacity_error : public std::exception { };

    IoTask(
        RikerIO::Profile&,
        unsigned int,
        unsigned int read_list_size = 1024,
        unsigned int requests_per_cycle = 10000);
    ~IoTask();

    void init();
    void loop();
    void quit();

    /**
     * \brief Read Data from the Shared Memory. Thread Safe
     * @param byte offset
     * @param byte size
     * @param callback function with buffer
     */
    void add_read_data(unsigned int, unsigned int, std::function<void(char*)>);

    /**
     * @brief Write Data to shared memory. Thread Safe
     * @param byte_offset
     * @param byte_size
     * @param memory pointer
     * @param callback function
     */
    void add_write_data(unsigned int, unsigned int, char* ptr, std::function<void()>);


    /**
     * @brief Write Typed Data to shared memory. Thread Safe
     * @param byte_offset
     * @param bit_offset
     * @param bit_size
     * @param typed value
     * @param callback function
     */
    template<typename T>
    void add_write_data(
        unsigned int byte_offset,
        unsigned int bit_offset,
        unsigned int bit_size,
        T value,
        WriteJob::Callback callback) {

        WriteJob* job = new WriteJob(
            (char*) memory_ptr + byte_offset,
            bit_size,
            bit_offset,
            callback);

        job->init<T>(value);

        Request req = { RequestCommand::ADD_WRITE_JOB, { .write = job } };

        try {
            to_rt_command_list.push(req);
        } catch (automation::utils::BufferOverflow& e) {
            spdlog::error("Error adding WriteJob to IoTask, Buffer capacity zero.");
            delete job;
        }

    };


    /* work through the response queue */
    void receive();

    void set_lock_handler(std::function<void()> handler);
    void set_unlock_handler(std::function<void()> handler);

  private:

    enum class RequestCommand {
        ADD_READ_JOB,
        ADD_WRITE_JOB
    };

    enum class ResponseType {
        READ_JOB_DONE,
        WRITE_JOB_DONE
    };

    struct Request {
        RequestCommand cmd;
        union {
            ReadJob* read;
            WriteJob* write;
        } job;
    };

    struct Response {
        ResponseType type;
        union {
            ReadJob* read;
            WriteJob* write;
        } job;
    };

  private:

    struct {
        struct {
            Uint16::Data major;
            Uint16::Data minor;
            Uint16::Data patch;
        } version;
        struct {
            struct {
                Uint64::Data cap;
                Uint64::Data size;
            } list;
            struct {
                Uint64::Data count;
            } jobs;
        } rx;
        struct {
            struct {
                Uint64::Data cap;
                struct {
                    Uint64::Data last;
                    Uint64::Data max;
                    Uint64::Data avg;
                } size;
            } list;
            struct {
                Uint64::Data count;
            } jobs;
        } tx;
        struct {
            Uint32::Data last;
            Uint32::Data avg;
            Uint32::Data max;
            Uint32::Data base;
        } cycle;

    } io;

    automation::Context                                         context;

    unsigned int                                                cycle;
    const unsigned int                                          requests_per_cycle;

    double                                                      tx_list_size_sum = 0;
    uint32_t                                                    tx_list_data_counter = 0;

  private:

    automation::os::Measurement                                 measure;

  private:

    uint8_t*                                                    memory_ptr;

    std::map<std::string, ReadJob*>                             read_job_map;

    utils::RingBuffer<Request>                                  to_rt_command_list;
    utils::RingBuffer<Response>                                 from_rt_command_list;

    uint8_t*                                                    multipart_buffer;
    unsigned int                                                multipart_buffer_size;

    /* to avoid duplicate reads */
    std::map<std::string, unsigned long>                        stream_map;

  private:

    std::function<void()>                                       lock_handler;
    std::function<void()>                                       unlock_handler;
    UpdateHandler                                               read_job_done_handler;

    uint8_t*                                                    tmp_buffer;


};

}

#endif
