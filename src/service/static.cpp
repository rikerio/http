#include "service/static.h"
#include "service/redirect.h"
#include "filesystem"
#include "fstream"

using namespace automation::http::service;

Static::Static(const std::string& base_path, const std::string& index_file = "index.html") :
    base_path(base_path),
    index_file(index_file) { }

automation::http::ExtendedApp::Handler Static::replace_url(std::string replacement) {

    return [replacement](auto, auto req) {

        spdlog::debug("{} {} : Replace url with {}.", req->get_method(), req->get_url(), replacement);

        req->set_url(replacement);

    };

}

automation::http::ExtendedApp::Handler Static::operator () (
    const std::string& base_url,
    std::function<void(const std::filesystem::path&, std::string&)> content_handler) {

    return [&,base_url=base_url,content_handler=content_handler](auto res, auto req) {

        std::string file(req->get_url());
        std::string new_file = "";

        if (file.length() < base_url.length()) {
            throw automation::http::ExtendedApp::bad_request_error("Wrong url");
        }

        std::string tmp_file = file;
        tmp_file.erase(0, base_url.length());

        spdlog::debug("{} {} : {}", req->get_method(), req->get_url(), tmp_file);

        if (tmp_file[0] == '/') {
            tmp_file.erase(0, 1);
        }

        spdlog::debug("{} {} : {}", req->get_method(), req->get_url(), tmp_file);

        if (tmp_file.length() == 0) {
            tmp_file = index_file;
        }

        spdlog::debug("{} {} : {}", req->get_method(), req->get_url(), tmp_file);

        std::filesystem::path file_p(base_path);
        file_p /= tmp_file;


        spdlog::debug("{} {} : Transforming {} into {}.", req->get_method(), req->get_url(), file, (std::string)file_p);

        if (!std::filesystem::exists(file_p)) {
            throw automation::http::ExtendedApp::not_found_error();
            // spdlog::debug("{} {} : File {} does not exist, serving file {} instead.", req->get_method(), req->get_url(), file, index_file);
            // file = path + "/" + index_file;
        }

        spdlog::debug("{} {} : Serving file {} from path {}.", req->get_method(), req->get_url(), (std::string)file_p.filename(), base_path);

        std::string ext = file_p.extension();
        std::string contentType = "";

        if (ext == ".svg") {
            contentType = "image/svg+xml";
        } else if (ext == ".html") {
            contentType = "text/html; charset=UTF-8";
        } else if (ext == ".js") {
            contentType = "application/javascript; charset=UTF-8";
        } else if (ext == ".css") {
            contentType = "text/css; charset=UTF-8";
        } else {
            contentType = "text/plain; charset=UTF-8";
        }


        spdlog::debug("Requesting file {}.", file);

        try {

            std::ifstream t(file_p);
            std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

            if (content_handler != nullptr) {
                content_handler(file_p, str);
            }

            res->set_header("Content-Type", contentType);
            res->set_status("200 OK");
            res->set_data(str);

        } catch (...) {
            res->set_status("501 Internal Server Error");
        }

        /*                async_file_streamer.streamHead(res, file);

                        if (req->getMethod() == "head") {
                            return;
                        }

                        async_file_streamer.streamFile(res, file);

                    } catch (not_found_error& e) {

                        res->set_status("404 Not Found");

                    }
                    */
    };
}
