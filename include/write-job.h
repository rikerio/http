#ifndef __AUTOMATION_IO_TASK_WRITE_JOB_H__
#define __AUTOMATION_IO_TASK_WRITE_JOB_H__

#include "stdlib.h"
#include "string.h"
#include "string"
#include "bitset"
#include "cmath"
#include "stdexcept"
#include "algorithm"
#include "iostream"
#include "functional"

namespace automation::http {

class WriteJob {
  public:

    using Callback = std::function<void()>;

    WriteJob(char* ptr, unsigned int bit_size, unsigned int bit_offset, Callback callback ) :
        bit_size(bit_size),
        bit_offset(bit_offset),
        callback(callback),
        memory_ptr(ptr),
        read(nullptr),
        data(nullptr),
        mask(nullptr) {
    }

    ~WriteJob() {
        free(read);
        free(data);
        free(mask);
    }

    WriteJob& init(char* ptr, unsigned int byte_size) {

        unsigned int self_byte_size = calc_bytesize();

        if (byte_size > self_byte_size) {
            throw std::invalid_argument("invalid byte size.");
        }

        read = (char*) calloc(1, byte_size);
        data = (char*) calloc(1, byte_size);
        mask = (char*) calloc(1, byte_size);

        memset(mask, 0xFF, byte_size);
        memcpy(data, ptr, byte_size);

        return *this;
    }

    template<typename T>
    WriteJob& init(T value) {

        unsigned int byte_size = calc_bytesize();

        if (sizeof(T) > byte_size) {
            throw std::invalid_argument("invalid byte size.");
        }

        read = (char*) calloc(1, byte_size);
        data = (char*) calloc(1, byte_size);
        mask = (char*) calloc(1, byte_size);

        // set mask with offset

        unsigned int tmp_offset = bit_offset;
        unsigned int tmp_size = bit_size;
        unsigned int byte_counter = 0;

        while (tmp_size > 0) {

            std::bitset<8> bs_mask;

            for (unsigned int i = 0; i < 8; i+= 1) {
                if (tmp_offset > 0) {
                    bs_mask[i] = 0;
                    tmp_offset -= 1;
                    continue;
                }

                if (tmp_size == 0) {
                    bs_mask[i] = 0;
                    continue;
                }

                bs_mask[i] = 1;
                tmp_size -= 1;
            }

            uint8_t byte_mask = static_cast<uint8_t>(bs_mask.to_ulong());

            *(mask + byte_counter) = byte_mask;

            byte_counter += 1;

        }

        tmp_size = bit_size;

        unsigned int new_bit_offset = bit_offset;
        unsigned int orig_bit_offset = 0;

        uint8_t* tmp_orig_value = (uint8_t*) &value;
        uint8_t* tmp_new_value = (uint8_t*) data;
        std::bitset<8> orig_data(*tmp_orig_value);
        std::bitset<8> new_data(0);

        tmp_size = std::max(8u, tmp_size);

        while (tmp_size > 0) {

            new_data[new_bit_offset] = orig_data[orig_bit_offset];

            new_bit_offset += 1;
            orig_bit_offset += 1;
            tmp_size -= 1;


            if (new_bit_offset == 8) {
                *tmp_new_value = static_cast<uint8_t>(new_data.to_ulong());
                tmp_new_value += 1;
                new_data.reset();
                new_bit_offset = 0;
            }

            if (tmp_size == 0) {
                break;
            }

            if (orig_bit_offset == 8) {
                tmp_orig_value += 1;
                orig_data = std::bitset<8>(*tmp_orig_value);
                orig_bit_offset = 0;
            }

        }


        return *this;

    }

    void write() {

        if (bit_size == 0) {
            return;
        }

        unsigned int byte_size = calc_bytesize();

        memcpy(read, memory_ptr, byte_size);

        uint8_t* tmp_rd = (uint8_t*) read;
        uint8_t* tmp_dd = (uint8_t*) data;
        uint8_t* tmp_tar = (uint8_t*) memory_ptr;

        for (unsigned int i = 0; i < byte_size; i += 1) {

            uint8_t tmp_mask = *(mask + i);
            uint8_t tmp_mask_inv = 0xFF ^ tmp_mask;
            uint8_t tmp_read = *tmp_rd & tmp_mask_inv;
            uint8_t tmp_data = *tmp_dd & tmp_mask;
//            uint8_t tmp_val = tmp_read | tmp_data;

            /*
             * std::cout << std::hex << "tmp_read =  " << +*(tmp_rd) << " & " << +tmp_mask_inv << " = " << +tmp_read << std::endl;
             * std::cout << std::hex << "tmp_data = " << +*(tmp_dd) << " & " << +tmp_mask << " = " << +tmp_data << std::endl;
             * std::cout << std::hex << "tmp_val = " << +tmp_read << " | " << +tmp_data << " = " << +tmp_val << std::endl;
            */

            tmp_rd += 1;
            tmp_dd += 1;

            *tmp_tar = tmp_read | tmp_data;
            tmp_tar += 1;

        }

    }

    void resolve() {
        if (!callback) {
            return;
        }

        callback();
    }

  private:

    unsigned int bit_size;
    unsigned int bit_offset;
    Callback callback;

    char* memory_ptr;
    char* read;
    char* data;
    char* mask;

  private:

    unsigned int calc_bytesize() {

        unsigned int real_size = bit_offset + bit_size;
        unsigned int rest = real_size % 8;
        unsigned int bit_size_without_rest = real_size - rest;
        unsigned int full_bytes = bit_size_without_rest / 8;

        return rest > 0 ? (full_bytes + 1) : full_bytes;

    }


};

}

#endif
