#ifndef __AUTOMATION_HTTP_SERVICE_RIKERIO_H__
#define __AUTOMATION_HTTP_SERVICE_RIKERIO_H__

#include "http/xapp.h"
#include "rikerio.h"

namespace automation::http::service {

class RikerIOService {

  public:
    RikerIOService() {

        if (RikerIO::init("default", profile) == RikerIO::result_error) {
            spdlog::error("Error intializing RikerIO Profile default.");
        }

    }

    automation::http::ExtendedApp::Handler get_all_allocations() {

        return [&](auto& res, auto&) {

            std::vector<RikerIO::Allocation> alloc_list;

            if (RikerIO::list(profile, alloc_list) == RikerIO::result_error) {
                spdlog::error("Error receiving allocation list.");
                throw automation::http::ExtendedApp::internal_error();
            }

            Json::Value root = Json::arrayValue;

            for (auto& a : alloc_list) {
                Json::Value node = Json::objectValue;

                node["offset"] = a.offset;
                node["size"] = a.byte_size;
                node["id"] = a.id;

                root.append(node);

            }

            res.set_data(root);
            res.set_status(automation::http::ExtendedApp::http_status_ok);

        };

    }

    automation::http::ExtendedApp::Handler get_all_data() {

        return [&](auto& res, auto&) {

            std::vector<std::string> dp_id_list;

            if (RikerIO::Data::list(profile, dp_id_list) == RikerIO::result_error) {
                spdlog::error("Error receiving data list.");
                throw automation::http::ExtendedApp::internal_error();
            }

            Json::Value root = Json::arrayValue;

            for (auto& id : dp_id_list) {

                RikerIO::DataPoint dp;
                if (RikerIO::Data::get(profile, id, dp) == RikerIO::result_error) {
                    spdlog::error("Error receiving data with id {}.", id);
                    continue;
                }

                Json::Value node = Json::objectValue;

                node["offset"] = dp.byte_offset;
                node["index"] = dp.bit_index;
                node["size"] = dp.bit_size;
                node["id"] = dp.id;
                node["type"] = string_type(dp.type);

                root.append(node);

            }

            res.set_data(root);
            res.set_status(automation::http::ExtendedApp::http_status_ok);

        };

    }

    automation::http::ExtendedApp::Handler get_all_links() {

        return [&](auto& res, auto&) {

            std::vector<std::string> link_list;

            if (RikerIO::Link::list(profile, link_list) == RikerIO::result_error) {
                spdlog::error("Error receiving link list.");
                throw automation::http::ExtendedApp::internal_error();
            }

            Json::Value root = Json::arrayValue;

            for (auto& key : link_list) {

                std::vector<std::string> id_list;
                if (RikerIO::Link::get(profile, key, id_list) == RikerIO::result_error) {
                    spdlog::error("Error receiving link with key {}.", key);
                    continue;
                }

                Json::Value node = Json::objectValue;
                Json::Value ids = Json::arrayValue;

                node["key"] = key;

                for (auto& id : id_list) {
                    Json::Value node_id = id;
                    ids.append(node_id);
                }
                node["ids"] = ids;

                root.append(node);

            }

            res.set_data(root);
            res.set_status(automation::http::ExtendedApp::http_status_ok);

        };

    }



  private:

    RikerIO::Profile profile;

  private:

    const std::string string_type(RikerIO::Type type) {

        switch (type) {
        case (RikerIO::Type::BIT):
            return "bit";
        case (RikerIO::Type::BOOL):
            return "bool";
        case (RikerIO::Type::UINT8):
            return "uint8";
        case (RikerIO::Type::INT8):
            return "int8";
        case (RikerIO::Type::UINT16):
            return "uint16";
        case (RikerIO::Type::INT16):
            return "int16";
        case (RikerIO::Type::UINT32):
            return "uint32";
        case (RikerIO::Type::INT32):
            return "int32";
        case (RikerIO::Type::UINT64):
            return "uint64";
        case (RikerIO::Type::INT64):
            return "int64";
        case (RikerIO::Type::FLOAT):
            return "float";
        case (RikerIO::Type::DOUBLE):
            return "double";
        case (RikerIO::Type::STRING):
            return "string";
        case (RikerIO::Type::UNDEF):
            return "undef";
        }

        return "undef";
    }

};
}

#endif
