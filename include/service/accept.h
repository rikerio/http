#ifndef __AUTOMATION_HTTP_SERVICE_ACCEPT_H__
#define __AUTOMATION_HTTP_SERVICE_ACCEPT_H__

#include "xapp.h"
#include "utils.h"

namespace automation::http::service {

class Accept {

  public:

    static automation::http::ExtendedApp::Handler contentType(const std::string& wildcard) {

        return [wc = wildcard](auto, auto req) {

            auto ct = req->get_header("content-type");
            auto new_ct = Utils::trim(ct);

            if (!Utils::pattern_match(new_ct, wc)) {
                spdlog::debug("{}:{} : Content Type not accepted (pattern_match({}, {}))", req->get_method(), req->get_url(), new_ct, wc);
                std::string msg = "Unaccepted content type : " + wc + " != " + std::string(new_ct) + "";
                throw automation::http::ExtendedApp::bad_request_error(msg);
            }

        };

    };

    static automation::http::ExtendedApp::Handler maxContentLength(size_t max_length) {

        return [ml = max_length](auto, auto req) {

            auto ct_str = std::string(req->get_header("content-length"));
            size_t ct = atol(ct_str.c_str());

            if (ct > ml) {
                spdlog::debug("{}:{} : Content Length not accepted (max = {}))", req->get_method(), req->get_url(), ml);
                throw automation::http::ExtendedApp::bad_request_error("Unaccepted content length (max " + std::to_string(ml) + ")");
            }

        };
    }

};
}

#endif
