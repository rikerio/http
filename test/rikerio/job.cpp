#include "gtest/gtest.h" // googletest header file

#include "rikerio/job.h"
#include <string>

using namespace automation;

TEST(RikerIO_Job, Uint8_Write) {

    std::shared_ptr<ValueContainer> vc_ptr = std::make_shared<ValueContainer>(ValueContainer::Perm::Write, Type::create<uint8_t>());
    rikerio::Job job("test", TypeInfos<uint8_t>::BitSize(), vc_ptr);

    uint8_t memory_value = 0;

    job.add_adr((char*) &memory_value, 0);

    vc_ptr->write<uint8_t>(1);
    job.write();

    ASSERT_EQ(memory_value, 1);

    vc_ptr->write<uint8_t>(233);
    job.write();

    ASSERT_EQ(memory_value, 233);

}

TEST(RikerIO_Job, Bool_Write_A) {

    std::shared_ptr<ValueContainer> vc_ptr =
        std::make_shared<ValueContainer>(ValueContainer::Perm::Write, Type::create<uint8_t>());
    rikerio::Job job("test", TypeInfos<bool>::BitSize(), vc_ptr);

    uint8_t memory_value = 0;

    job.add_adr((char*) &memory_value, 1); // index = 1

    vc_ptr->write<bool>(true);
    job.write();

    ASSERT_EQ(memory_value, 2);

    vc_ptr->write<bool>(false);
    job.write();

    ASSERT_EQ(memory_value, 0);


}

TEST(RikerIO_Job, Bool_Write_B) {

    std::shared_ptr<ValueContainer> vc_ptr =
        std::make_shared<ValueContainer>(ValueContainer::Perm::Write, Type::create<uint8_t>());
    rikerio::Job job("test", TypeInfos<bool>::BitSize(), vc_ptr);

    uint8_t memory_value = 0b00001010;

    ASSERT_EQ(memory_value, 10);

    job.add_adr((char*) &memory_value, 1); // index = 1

    vc_ptr->write<bool>(true); // only to trigger the updated flag
    vc_ptr->write<bool>(false);
    job.write();

    ASSERT_EQ(memory_value, 8);

}

