#include "factory/rio-link.h"
#include "xapp.h"

using namespace automation::http;

factory::RioLink::RioLink(EventEmitter& emitter, std::string event_prefix, RikerIO::Profile& rio_profile) :
    factory::Cached<model::RioLink>(emitter, event_prefix),
    rio_profile(rio_profile) {
    update_cache();
}

std::shared_ptr<model::RioLink> factory::RioLink::add(model::RioLink& rio_link) {

    /* remove old link */

    if (RikerIO::Link::remove(rio_profile, rio_link.get_id()) == RikerIO::result_error) {
        spdlog::debug("Error removing rikerio link {}", rio_link.get_id());
    }

    /* recreate link */

    for (auto& key : rio_link.get_keys()) {

        if (RikerIO::Link::set(rio_profile, rio_link.get_id(), key) == RikerIO::result_error) {
            spdlog::error("Error setting rikerio link {}:{}.", rio_link.get_id(), key);
            throw ExtendedApp::internal_error("Error setting rikerio link.");
        }

    }

    std::shared_ptr<model::RioLink> rio_link_ptr = std::make_shared<model::RioLink>(rio_link);
    t_map[rio_link.get_id()] = rio_link_ptr;

    return rio_link_ptr;
}

std::shared_ptr<model::RioLink> factory::RioLink::update(const std::string& id, model::RioLink& rio_link) {

    /* remove old link */

    if (RikerIO::Link::remove(rio_profile, id) == RikerIO::result_error) {
        spdlog::error("Error removing rikerio link {}", id);
        throw ExtendedApp::internal_error("Error removing rikerio link.");
    }

    /* recreate link */

    for (auto& key : rio_link.get_keys()) {

        if (RikerIO::Link::set(rio_profile, id, key) == RikerIO::result_error) {
            spdlog::error("Error setting rikerio link {}:{}.", id, key);
            throw ExtendedApp::internal_error("Error setting rikerio link.");
        }

    }

    std::shared_ptr<model::RioLink> rio_link_ptr = std::make_shared<model::RioLink>(rio_link);
    t_map[rio_link.get_id()] = rio_link_ptr;


    return rio_link_ptr;
}

std::shared_ptr<model::RioLink> factory::RioLink::remove(const std::string& id) {
    /* remove old link */

    if (RikerIO::Link::remove(rio_profile, id) == RikerIO::result_error) {
        spdlog::error("Error removing rikerio link {}", id);
        throw ExtendedApp::internal_error("Error removing rikerio link.");
    }

    auto deleted_t = t_map[id];

    return deleted_t;
}

bool factory::RioLink::filter(const model::RioLink& data, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != data.get_id().find(value)) {
        return true;
    }

    for (auto& k : data.get_keys()) {
        if (std::string::npos != k.find(value)) {
            return true;
        }
    }

    return false;

}

int factory::RioLink::compare(const std::string& field, const model::RioLink& a, const model::RioLink& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    return 0;

}

void factory::RioLink::update_cache() {

    std::filesystem::file_time_type new_last_write;

    spdlog::debug("RioLink::update_cache() : Checking rikerio for updates.");
    if (RikerIO::last_write(rio_profile, new_last_write) == RikerIO::result_error) {
        spdlog::error("RioLink::update_cache() : Error receiving last write time.");
    };

    if (new_last_write == rio_last_write) {
        spdlog::debug("RioLink::update_cache() : No updates available, cache stays unchanged.");
        return;
    }

    spdlog::debug("RioLink::update_cache() : Updates available!");

    rio_last_write = new_last_write;

    std::vector<std::string> key_list;
    if (RikerIO::Link::list(rio_profile, key_list) == RikerIO::result_error) {
        spdlog::error("Error updating RikerIO Link list.");
        return;
    }

    t_map.clear();
    query_cache_map.clear();


    std::vector<model::RioLink> link_list;
    for (auto& key : key_list) {

        std::vector<std::string> id_list;

        if (RikerIO::Link::get(rio_profile, key, id_list) == RikerIO::result_error) {
            spdlog::error("Error getting RikerIO Link {}.", key);
        }

        auto l = std::make_shared<model::RioLink>(key, id_list);
        t_map[key] = l;

    }

}
