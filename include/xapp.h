#ifndef __DEPOT_AUTOMATION_EXTENDED_HTTP_APP_H__
#define __DEPOT_AUTOMATION_EXTENDED_HTTP_APP_H__

#include "iostream"
#include "queue"
#include "set"
#include "spdlog/spdlog.h"
#include "string"
#include "uWebSockets/App.h"

#include "xrequest.h"
#include "xresponse.h"

namespace automation::http {

class ExtendedApp {

public:
  static const std::string http_status_ok;             // 200
  static const std::string http_status_created;        // 201
  static const std::string http_status_no_content;     // 204
  static const std::string http_status_not_allowed;    // 403
  static const std::string http_status_internal_error; // 500

public:
  class basic_http_error : public std::exception {
  public:
    basic_http_error(unsigned int code, const std::string status_text,
                     const std::string reason = "")
        : code(code), status_text(status_text),
          status(std::to_string(code) + " " + status_text), reason(reason) {}
    const std::string &get_reason() const { return reason; }
    const std::string &get_status_text() const { return status_text; }
    unsigned int get_code() const { return code; }
    const std::string &get_status() const { return status; }

  private:
    const unsigned int code;
    const std::string status_text;
    const std::string status;
    const std::string reason;
  };

  class bad_request_error : public basic_http_error {
  public:
    bad_request_error(const std::string reason = "")
        : basic_http_error(400, "Bad Request", reason) {}
  }; // 400
  class unauthorized_error : public basic_http_error {
  public:
    unauthorized_error(const std::string reason = "")
        : basic_http_error(401, "Unauthorized", reason) {}
  }; // 401
  class forbidden_error : public basic_http_error {
  public:
    forbidden_error(const std::string reason = "")
        : basic_http_error(403, "Forbidden", reason) {}
  }; // 403
  class not_found_error : public basic_http_error {
  public:
    not_found_error(const std::string reason = "")
        : basic_http_error(404, "Not Found", reason) {}
  }; // 404

  class internal_error : public basic_http_error {
  public:
    internal_error(const std::string reason = "")
        : basic_http_error(500, "Internal Server Error", reason) {}
  }; // 500
  class not_implemented_error : public basic_http_error {
  public:
    not_implemented_error(const std::string reason = "")
        : basic_http_error(501, "Not implemented", reason) {}
  }; // 501

public:
  using Handler = std::function<void(std::shared_ptr<ExtendedResponse>,
                                     std::shared_ptr<ExtendedRequest>)>;

  class HandlerItem {
  public:
    HandlerItem(const std::string pattern) : pattern(pattern) {}

    HandlerItem &operator<<(Handler handler) { return add_handler(handler); }

    HandlerItem &add_handler(Handler handler) {
      handler_list.push_back(handler);
      return *this;
    }

    std::vector<Handler> &get_list() { return handler_list; }

    const std::string &get_pattern() const { return pattern; }

    HandlerItem &add_tag(std::initializer_list<std::string> tags) {
      for (auto tag : tags) {
        tag_set.insert(tag);
      }
      return *this;
    }

    const std::set<std::string> &get_tags() const { return tag_set; }

  private:
    const std::string pattern;
    std::vector<Handler> handler_list;
    std::set<std::string> tag_set;
  };

public:
  ExtendedApp(uWS::App &app);

  void use(Handler);
  void ws(const std::string pattern, uWS::App::WebSocketBehavior &behavior,
          std::string tags = "");

  HandlerItem &use(const std::string method, const std::string pattern);
  HandlerItem &get(const std::string pattern);
  HandlerItem &post(const std::string pattern);
  HandlerItem &del(const std::string pattern);
  HandlerItem &put(const std::string pattern);
  HandlerItem &options(const std::string pattern);

  void set_cors_enabled(bool);
  void listen(const std::string &, unsigned int port);
  void close();

private:
  Handler handle_preflight_request(const std::string);
  Handler allow_cors_header();

private:
  struct Job {
    std::shared_ptr<ExtendedRequest> req;
    std::shared_ptr<ExtendedResponse> res;
  };

private:
  uWS::App &app;
  us_listen_socket_t *listen_socket;

  std::vector<std::string> pattern_list;
  std::map<std::string, std::shared_ptr<HandlerItem>> handler_map;
  std::vector<Handler> middleware_list;

  std::queue<Job> jobs;

  std::map<std::string, std::set<std::string>> pattern_map;
  bool cors_enabled = false;

private:
  void handle_request(std::shared_ptr<HandlerItem>, uWS::HttpResponse<false> *,
                      uWS::HttpRequest *);
};

} // namespace automation::http

#endif
