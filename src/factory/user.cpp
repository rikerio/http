#include "factory/user.h"
#include "openssl/ssl.h"
#include "sstream"
#include "iostream"
#include "string"
#include "iomanip"

using namespace automation::http;

const std::string factory::User::hash_password(const std::string& password) {


    unsigned char hash[SHA256_DIGEST_LENGTH];

    unsigned char pw[2048];
    memset(pw, 0, 2048);
    memcpy(pw, password.c_str(), password.size());

    SHA256(pw, password.size(), hash);

    stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        ss << std::hex << setw(2) << setfill('0') << (int)hash[i];
    }

    return ss.str();
}


factory::User::User(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::User>(emitter, filename, "user") {

    deserialize();

}

factory::User::~User() {
    serialize();
}

std::shared_ptr<model::User> factory::User::initiate_admin(const std::string& admin_password) {

    admin_user = std::make_shared<model::User>();
    (*admin_user).set_username("admin")
    .set_password(hash_password(admin_password))
    .set_name("Administrator")
    .set_disabled(false);

    return add(*admin_user);

}

std::shared_ptr<model::User> factory::User::initiate_guest() {

    guest_user = std::make_shared<model::User>();
    (*guest_user).set_username("guest")
    .set_password(hash_password("guest"))
    .set_name("Guest")
    .set_disabled(true);

    return add(*guest_user);

}

std::shared_ptr<model::User> factory::User::initiate_nobody() {

    nobody_user = std::make_shared<model::User>();
    (*nobody_user).set_username("nobody")
    .set_password("")
    .set_name("Nobody")
    .set_disabled(false);

    return add(*nobody_user);

}

std::shared_ptr<model::User> factory::User::verify(
    const std::string& username, const std::string& password) {

    auto it = username_map.find(username);

    if (it == username_map.end()) {
        throw not_found_error();
    }

    auto user = it->second;

    if (user->is_disabled()) {
        throw not_found_error();
    }

    if (password != user->get_password()) {
        throw verification_error();
    }

    return user;

}

std::shared_ptr<model::User> factory::User::add(model::User& user) {

    auto user_it = username_map.find(user.get_username());

    if (user_it != username_map.end()) {
        throw fixed_item_error();
    }

    auto res = factory::YamlFile<model::User>::add(user);

    username_map[res->get_username()] = res;

    return res;

}

std::shared_ptr<model::User> factory::User::update(const std::string& id, model::User& user) {

    if (!admin_user) {
        auto admin_user_it = username_map.find("admin");
        admin_user = admin_user_it->second;
    }

    if (!nobody_user) {
        auto nobody_user_it = username_map.find("nobody");
        nobody_user = nobody_user_it->second;
    }

    if (!guest_user) {
        auto guest_user_it = username_map.find("guest");
        guest_user = guest_user_it->second;
    }

    bool id_check = id == admin_user->get_id();
    bool empty_pw_check = user.get_password() != "";
    bool new_pw_check = user.get_password() != admin_user->get_password();

    if (id_check && empty_pw_check && new_pw_check) {
        model::User new_admin_user = *admin_user;
        new_admin_user.set_password(user.get_password());
        return factory::YamlFile<model::User>::update(id, new_admin_user);

    } else if (id == admin_user->get_id()) {
        throw fixed_item_error();
    }

    if (id == nobody_user->get_id()) {
        throw fixed_item_error();
    }

    if (id == guest_user->get_id()) {
        model::User new_guest_user = *guest_user;
        new_guest_user.set_disabled(user.is_disabled());
        return factory::YamlFile<model::User>::update(id, new_guest_user);
    }


    if (user.get_username() == "admin" || user.get_username() == "guest" || user.get_username() == "nobody") {
        throw fixed_item_error();
    }

    return factory::YamlFile<model::User>::update(id, user);

}

std::shared_ptr<model::User> factory::User::remove(const std::string& id) {

    auto admin_user = get_user_by_username("admin");
    auto guest_user = get_user_by_username("guest");
    auto nobody_user = get_user_by_username("nobody");

    if (id == admin_user->get_id() || id == guest_user->get_id() || id == nobody_user->get_id()) {
        throw fixed_item_error();
    }

    return factory::YamlFile<model::User>::remove(id);

}

std::shared_ptr<model::User> factory::User::get_user_by_username(const std::string& username) {

    auto it = username_map.find(username);

    if (it == username_map.end()) {
        throw not_found_error();
    }

    return it->second;

}

void factory::User::deserialize() {
    YamlFile<model::User>::deserialize();

    for (auto user_it : t_map) {
        username_map[user_it.second->get_username()] = user_it.second;
    }
}

YAML::Node factory::User::to_yaml(const model::User& rhs) const {

    YAML::Node user_node;

    user_node["id"] = rhs.get_id();
    user_node["username"] = rhs.get_username();
    user_node["name"] = rhs.get_name();
    user_node["password"] = rhs.get_password();
    user_node["disabled"] = rhs.is_disabled();

    return user_node;
}

model::User factory::User::from_yaml(YAML::Node node) const {

    automation::http::model::User user;

    YAML::Node yaml_id = node["id"];
    YAML::Node yaml_username = node["username"];
    YAML::Node yaml_name = node["name"];
    YAML::Node yaml_password = node["password"];
    YAML::Node yaml_disabled = node["disabled"];

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        user.set_id(yaml_id.as<std::string>());
    }

    if (yaml_username && yaml_username.Type() == YAML::NodeType::Scalar) {
        user.set_username(yaml_username.as<std::string>());
    }

    if (yaml_name && yaml_name.Type() == YAML::NodeType::Scalar) {
        user.set_name(yaml_name.as<std::string>());
    }

    if (yaml_password && yaml_password.Type() == YAML::NodeType::Scalar) {
        user.set_password(yaml_password.as<std::string>());
    }

    if (yaml_disabled && yaml_disabled.Type() == YAML::NodeType::Scalar) {
        user.set_disabled(yaml_disabled.as<bool>());
    }

    return user;

}

bool factory::User::filter(const automation::http::model::User& user, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != user.get_username().find(value)) {
        return true;
    }

    if (std::string::npos != user.get_name().find(value)) {
        return true;
    }

    return false;

}

int factory::User::compare(
    const std::string& field,
    const model::User& a,
    const model::User& b) const {

    if (field == "username" || field == "") {
        return a.get_username().compare(b.get_username());
    }

    if (field == "name") {
        return a.get_name().compare(b.get_name());
    }

    return 0;

}

std::string factory::User::create_sha256_hex_hash(const std::string unhashed) {
    std::string hashed;

    EVP_MD_CTX* context = EVP_MD_CTX_new();

    if(context != NULL) {
        if(EVP_DigestInit_ex(context, EVP_sha256(), NULL)) {
            if(EVP_DigestUpdate(context, unhashed.c_str(), unhashed.length())) {
                unsigned char hash[EVP_MAX_MD_SIZE];
                unsigned int lengthOfHash = 0;

                if(EVP_DigestFinal_ex(context, hash, &lengthOfHash)) {
                    std::stringstream ss;
                    for(unsigned int i = 0; i < lengthOfHash; ++i) {
                        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
                    }

                    hashed = ss.str();
                }
            }
        }

        EVP_MD_CTX_free(context);
    }

    return hashed;
}
