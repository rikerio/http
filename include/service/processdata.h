#ifndef __DEPOT_AUTOMATION_HTTP_IO_SERVICE_H__
#define __DEPOT_AUTOMATION_HTTP_IO_SERVICE_H__

#include "App.h"
#include "json/json.h"
#include "yaml-cpp/yaml.h"

#include "http/xapp.h"
#include "http/stream.h"

namespace automation::http::service {

class ProcessData {

  public:

    ProcessData(const std::string& base_uri, automation::http::Stream& stream);

    automation::http::ExtendedApp::Handler update_data_by_key();
    automation::http::ExtendedApp::Handler update_multiple_data();

  private:

    Json::CharReaderBuilder json_read_builder;
    Json::StreamWriterBuilder json_write_builder;
    std::unique_ptr<Json::CharReader> json_reader;

  private:

    automation::http::Stream& stream;

  private:

    const std::strig base_uri;

    Json::Value create_update(Stream::Update&);
    Json::Value create_update_array(std::vector<Stream::Update>&);

};

}

#endif
