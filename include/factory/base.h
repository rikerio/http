#ifndef __AUTOMATION_HTTP_FACTORY_BASE_H__
#define __AUTOMATION_HTTP_FACTORY_BASE_H__

#include "stdexcept"

namespace automation::http::factory {

class not_found_error : public std::exception { };
class duplicate_error : public std::exception { };
class update_id_error : public std::exception { };
class not_implemented_error : public std::exception { };
class fixed_item_error : public std::exception { };


class Base {


};

}

#endif
