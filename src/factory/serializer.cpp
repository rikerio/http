#include "factory/serializer.h"

#include "unistd.h"
#include "spdlog/spdlog.h"

using namespace automation::http::factory;

SerializerUnit::SerializerUnit(Serializable& s, unsigned int waiting_time) :
    s(s),
    last_counter_value(0),
    waiting_time(waiting_time) {


}

void SerializerUnit::trigger() {

    current_state = next_state;

    switch (current_state) {

    case (State::Ready) :

        if (last_counter_value != s.get_change_count()) {
            last_counter_value = s.get_change_count();
            next_state = State::Changed;
            spdlog::debug("Changes detected.");
            timer.reset();
            break;
        }

        break;

    case (State::Changed):

        if (last_counter_value != s.get_change_count()) {
            timer.reset();
            last_counter_value = s.get_change_count();
            spdlog::debug("Changed detected inside waiting time.");
            break;
        }

        if (timer.is_time_past(waiting_time)) {

            spdlog::debug("No new changes during waiting time, serializing.");

            s.serialize();
            s.reset_change_count();
            sync();
            last_counter_value = 0;

            next_state = State::Ready;
            break;
        }


        break;

    }

}


Serializer::Serializer() { }

Serializer& Serializer::add(Serializable& s, unsigned int waiting_time) {
    serializer_list.push_back(SerializerUnit(s, waiting_time));
    return *this;
}

Serializer& Serializer::trigger() {
    for (auto& su : serializer_list) {
        su.trigger();
    }
    return *this;
}
