#ifndef __RIKERIO_HTTP_SERIALIZER_H__
#define __RIKERIO_HTTP_SERIALIZER_H__

#include "vector"
#include "rikerio-automation/automation.h"
#include "factory/serializable.h"

namespace automation::http::factory {

class SerializerUnit {

  public:
    SerializerUnit(Serializable&, unsigned int);

    void trigger();

  private:

    Serializable& s;
    unsigned int last_counter_value;
    unsigned int waiting_time;

  private:

    enum class State {
        Ready, Changed
    };

    State current_state;
    State next_state = State::Ready;

    automation::utils::Timer timer;

};

class Serializer {

  public:

    Serializer();

    Serializer& add(Serializable&, unsigned int waiting_time = 1000);
    Serializer& trigger();

  private:

    std::vector<SerializerUnit> serializer_list;


};


}


#endif
