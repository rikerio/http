#ifndef __AUTOMATION_HTTP_FACTORY_GROUP_H__
#define __AUTOMATION_HTTP_FACTORY_GROUP_H__

#include "string"
#include "set"
#include "fstream"

#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#include "event-emitter.h"
#include "factory/yaml-file.h"
#include "models/group.h"

namespace automation::http::factory {

class Group : public YamlFile<model::Group> {

  public:

    Group(EventEmitter& emitter, const std::string& filename);
    ~Group();

    std::shared_ptr<model::Group> add(model::Group&) override;
    std::shared_ptr<model::Group> remove(const std::string&) override;
    std::shared_ptr<model::Group> update(const std::string&, model::Group&) override;

  private:

    YAML::Node to_yaml(const model::Group&) const;
    model::Group from_yaml(YAML::Node) const;

    bool filter(const model::Group&, const std::string&) const;
    int compare(const std::string&, const model::Group&, const model::Group&) const;
    const std::string provide_unique_id () const override;



};

}

#endif
