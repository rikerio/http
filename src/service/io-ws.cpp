#include "service/io-ws.h"
#include "models/rio-data.h"
#include "algorithm"

using namespace automation::http::service;

IoWebsocket::IoWebsocket(automation::http::IoTask& io_task, RikerIO::Profile& rio_profile) :
    io_task(io_task),
    rio_profile(rio_profile),
    json_read_builder(),
    json_reader(json_read_builder.newCharReader()) {

}

IoWebsocket::~IoWebsocket() { }

void IoWebsocket::on_connected(uWS::WebSocket<false, true>* ws) {
    auto websocket_context = std::make_shared<WebSocketContext>(ws);
    websocket_map[ws] = websocket_context;
}

void IoWebsocket::on_close(uWS::WebSocket<false, true>* ws) {
    spdlog::debug("Websocket: Connection closed.");

    auto ws_it = websocket_map.find(ws);

    if (ws_it == websocket_map.end()) {
        return;
    }

    remove_ids(ws, "*");

    websocket_map.erase(ws);

}

void IoWebsocket::on_data(uWS::WebSocket<false, true>* ws, Json::Value payload) {

    spdlog::debug("WebSocket/IO : Handling payload.");

    Json::Value json_method = payload["method"];

    if (!json_method || !json_method.isString()) {
        spdlog::debug("WebSocket/IO : Ill formated json (no method).", json_method.as<std::string>());
        WebSocket::send_error_response(ws, payload, "Ill formated json (no method).");
        return;
    }

    std::string str_method = json_method.as<std::string>();

    spdlog::debug("WebSocket/IO : Payload Method = {}", str_method);

    if (str_method != "addIds" && str_method != "removeIds") {
        spdlog::debug("WebSocket/IO : Ill formated json (no such method).");
        WebSocket::send_error_response(ws, payload, "Ill formated json (no such method).");
        return;
    }

    Json::Value json_args = payload["args"];

    if (!json_args || !json_args.isArray()) {
        spdlog::debug("WebSocket/IO : Ill formated json (no arguments).");
        WebSocket::send_error_response(ws, payload, "Ill formated json (no arguments).");
        return;
    }

    if (json_args.size() < 1) {
        spdlog::debug("WebSocket/IO : Ill formated json (insufficient number of arguments).");
        WebSocket::send_error_response(ws, payload, "Ill formated json (insufficient number of arguments.");
        return;
    }

    std::vector<std::string> args;
    for (unsigned int i = 0; i < json_args.size(); i += 1) {
        Json::Value json_arg = json_args[i];

        if (!json_arg || !json_arg.isString()) {
            spdlog::debug("WebSocket/IO : Ill formated json (argument musst be a string.");
            WebSocket::send_error_response(ws, payload, "Ill formated json (argument musst be a string.");
            return;
        }

        args.push_back(json_arg.as<std::string>());

    }

    // at this point we have the method name and the argument

    if (str_method == "addIds") {
        std::for_each(args.begin(), args.end(), [&](auto& id) {
            spdlog::debug("WebSocket/IO : addIds {}.", id);
            add_ids(ws, id);
        });

        auto wsctx_it = websocket_map.find(ws);

        if (wsctx_it == websocket_map.end()) {
            spdlog::error("IoWebsocket::add_ids(...) : websocket not found.");
            return;
        }

        auto websocket_context = wsctx_it->second;

        send_updates(*websocket_context, false);

    } else if (str_method == "removeIds") {
        std::for_each(args.begin(), args.end(), [&](auto& id) {
            spdlog::debug("WebSocket/IO : removeIds {}.", id);
            remove_ids(ws, id);
        });
    }


}

void IoWebsocket::trigger() {

    std::vector<std::string> data_erase_vector;

    std::for_each(data_map.begin(), data_map.end(), [&](auto it) {

        auto data = it.second;

        if (data->holder_count == 0) {
            data_erase_vector.push_back(data->id);
            return;
        }

        /* unsigned int byte_offset = data->dp.byte_offset;
        unsigned int bit_size = data->dp.bit_size;
        spdlog::debug("Read Data id : {} / byte offset {} / byte size {} {}.", data->id, byte_offset, data->byte_size, bit_size); */

        io_task.add_read_data(data->dp.byte_offset, data->byte_size, [data](char* buffer) {

            /* translate buffer to string value */
            std::string str_value = translate_buffer(data->byte_size, (uint8_t*) buffer, data->dp);

            if (!data->buffer_initialized) {
                data->string_value = str_value;
                data->buffer_updated = true;
                data->buffer_initialized = true;
                return;
            }

            if (str_value != data->string_value) {
                data->string_value = str_value;
                data->buffer_updated = true;
            }

        });

    });

    std::for_each(websocket_map.begin(), websocket_map.end(), [&](auto it) {
        auto& ws = it.second;
        send_updates(*ws, true);
    });

    // reset data_map

    std::for_each(data_map.begin(), data_map.end(), [&](auto it) {
        it.second->buffer_updated = false;
    });

    // remove unused data
    std::for_each(data_erase_vector.begin(), data_erase_vector.end(), [&](auto& id) {
        auto& data = data_map[id];
        delete data;
        data_map.erase(id);
    });

}

void IoWebsocket::add_ids(uWS::WebSocket<false, true>* ws, const std::string& pattern) {

    //spdlog::debug("Update::add_ids(...,{}) : Called.", pattern);

    auto wsctx_it = websocket_map.find(ws);

    if (wsctx_it == websocket_map.end()) {
        spdlog::error("IoWebsocket::add_ids(...) : websocket not found.");
        return;
    }

    auto websocket_context = wsctx_it->second;

    /* get latest data list from rikerio */
    std::vector<std::string> data_id_list;
    if (RikerIO::Data::list(rio_profile, data_id_list) == RikerIO::result_error) {
        spdlog::error("Error getting data list from rikerio.");
        return;
    }

    unsigned int new_key_count = 0;
    std::for_each(data_id_list.begin(), data_id_list.end(), [&](auto& id) {

        //spdlog::debug("Update::add_ids(...,{}) : Checking data {} with pattern {}.", pattern, id, pattern);

        if (!automation::http::Utils::pattern_match(id, pattern)) {
            return;
        }

        RikerIO::DataPoint dp;
        RikerIO::Data::get(rio_profile, id, dp);

        spdlog::debug("Update::add_ids(...,{}) : Adding id {}.", pattern, id);

        Data* d;

        auto global_data_it = data_map.find(id);

        if (global_data_it == data_map.end()) {
            /* add data globaly */
            d = new Data(id, dp);
            data_map.insert(std::make_pair(id, d));
        } else {
            d = global_data_it->second;
            d->holder_count += 1;
        }

        if (websocket_context->key_map.find(id) != websocket_context->key_map.end()) {
            spdlog::debug("Update::add_ids(...,{}) : Id {} already in filter..", pattern, id);
            return;
        }

        new_key_count += 1;

        websocket_context->key_map.insert(std::make_pair(id, true));
        websocket_context->data_vector.push_back(d);


    });

    /* update new keys */

    if (new_key_count == 0) {
        return;
    }


}

void IoWebsocket::remove_ids(uWS::WebSocket<false, true>* ws, const std::string& pattern) {

    spdlog::debug("IoWebsocket::remove_ids({}) called.", pattern);

    auto wsctx_it = websocket_map.find(ws);

    if (wsctx_it == websocket_map.end()) {
        spdlog::error("IoWebsocket::add_ids(...) : websocket not found.");
        return;
    }

    auto websocket_context = wsctx_it->second;

    spdlog::debug("IoWebsocket::remove_ids({}) : found websocket in list.", pattern);

    std::vector<unsigned int> erase_data_vector;
    auto& data_vector = websocket_context->data_vector;

    int index = -1;
    // remove data from websocket contexts data set and mark data for removal
    std::for_each(data_vector.begin(), data_vector.end(), [&](Data* data) {

        index += 1;

        if (!automation::http::Utils::pattern_match(data->id, pattern)) {
            return;
        }

        spdlog::debug("remove_ids({}) : removing ids {}.", pattern, data->id);
        websocket_context->key_map.erase(data->id);
        data->holder_count -= 1;

        erase_data_vector.push_back(index);

    });

    // remove data with no holder

    std::for_each(erase_data_vector.rbegin(), erase_data_vector.rend(), [&](auto& index) {

        auto data = data_vector.at(index);

        if (data->holder_count == 0) {
            data_vector.erase(data_vector.begin() + index);
        }

    });

}

void IoWebsocket::send_updates(WebSocketContext& ws, bool updated_only) {

    Json::Value root;

    unsigned int update_counter = 0;
    std::for_each(ws.data_vector.begin(), ws.data_vector.end(), [&](auto& data) {

        if (!data->buffer_initialized) {
            return;
        }

        auto freshly_added_it = ws.key_map.find(data->id);
        auto freshly_added_data = false;

        if (freshly_added_it != ws.key_map.end()) {
            freshly_added_data = freshly_added_it->second;
        }

        if (!freshly_added_data) {
            if (updated_only && !data->buffer_updated) {
                return;
            }
        } else {
            ws.key_map[data->id] = false;
        }

        update_counter += 1;

        Json::Value json;
        JSONCPP_STRING err;
        std::string json_string;


        if (data->type == "string" || data->type == "undefined" || data->type == "binary") {
            json_string = "{\"id\":\""+data->id+"\",\"value\":\""+data->string_value+"\",\"type\":\""+data->type+"\",\"size\":"+ std::to_string(data->dp.bit_size)+"}";
        } else {
            json_string = "{\"id\":\""+data->id+"\",\"value\":"+data->string_value+",\"type\":\""+data->type+"\",\"size\":" + std::to_string(data->dp.bit_size)+"}";
        }

        std::string message_string = "{\"t\":\"rioasync\",\"d\":" + json_string + "}";
        json_reader->parse(message_string.c_str(), message_string.c_str() + message_string.length(), &json, &err);
        root.append(json);

    });

    if (update_counter == 0) {
        return;
    }

    WebSocket::send_json(ws.websocket, root);

}
