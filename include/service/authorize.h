#ifndef __AUTOMATION_HTTP_SERVICE_AUTHORIZE_H__
#define __AUTOMATION_HTTP_SERVICE_AUTHORIZE_H__

#include "xapp.h"
#include "service/redirect.h"

namespace automation::http::service {

class Authorize {

    using xHandler = automation::http::ExtendedApp::Handler;

  public:

    static xHandler authorize_handler(
        std::function<void(
            std::shared_ptr<automation::http::ExtendedResponse>,
            std::shared_ptr<automation::http::ExtendedRequest>)> reject);

    static xHandler authorize();
    static xHandler authorize(const std::string redir_url);
    static xHandler authorize(
        std::function<xHandler(
            std::shared_ptr<automation::http::ExtendedResponse>,
            std::shared_ptr<automation::http::ExtendedRequest>)> handler);
    static automation::http::ExtendedApp::Handler permit_by_username(const std::string user_id);
    static automation::http::ExtendedApp::Handler permit_if_logged_in();

    static automation::http::ExtendedApp::Handler filter ();

};

}

#endif
