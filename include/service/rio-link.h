#ifndef __AUTOMATION_HTTP_SERVICE_RIO_LINK_H__
#define __AUTOMATION_HTTP_SERVICE_RIO_LINK_H__

#include "factory/rio-link.h"
#include "service/service.h"

namespace automation::http::service {

class RioLink : public FactoryService<factory::RioLink, model::RioLink> {

  public:

    RioLink(const std::string&, factory::RioLink&);

  private:

    Json::Value to_json(const model::RioLink&) const;
    model::RioLink from_json(Json::Value, const std::string&) const;

};

}

#endif
