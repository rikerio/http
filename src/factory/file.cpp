#include "factory/file.h"

using namespace automation::http;

factory::File::File(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::File>(emitter, filename, "file") {

    deserialize();

}

factory::File::~File() {
    serialize();
}

YAML::Node factory::File::to_yaml(const model::File& file) const {

    YAML::Node node;

    node["id"] = file.get_id();
    node["name"] = file.get_name();
    node["type"] = file.get_type();
    node["size"] = file.get_size();

    return node;

}

model::File factory::File::from_yaml(YAML::Node node) const {

    auto yaml_id = node["id"];
    auto yaml_name = node["name"];
    auto yaml_type = node["type"];
    auto yaml_size = node["size"];

    model::File file;

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        file.set_id(yaml_id.as<std::string>());
    }

    if (yaml_name && yaml_name.Type() == YAML::NodeType::Scalar) {
        file.set_name(yaml_name.as<std::string>());
    }

    if (yaml_type && yaml_type.Type() == YAML::NodeType::Scalar) {
        file.set_type(yaml_type.as<std::string>());
    }

    if (yaml_size && yaml_size.Type() == YAML::NodeType::Scalar) {
        file.set_size(yaml_size.as<size_t>());
    }

    return file;

}

bool factory::File::filter(const model::File& file, const std::string& str) const {

    if (str == "") {
        return true;
    }

    if (std::string::npos != file.get_id().find(str)) {
        return true;
    }

    if (std::string::npos != file.get_name().find(str)) {
        return true;
    }

    if (std::string::npos != file.get_type().find(str)) {
        return true;
    }

    return false;

}

int factory::File::compare(const std::string& field, const model::File& a, const model::File& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "name") {
        return a.get_name().compare(b.get_name());
    }

    if (field == "type") {
        return a.get_type().compare(b.get_type());
    }

    if (field == "size") {
        if (a.get_size() == b.get_size()) {
            return 0;
        }

        if (a.get_size() > b.get_size()) {
            return -1;
        }

        if (a.get_size() < b.get_size()) {
            return 1;
        }
    }

    return 0;

}
