#include "service/io.h"
#include "models/rio-data.h"
#include "base64.h"
#include "parser/json.h"

using namespace automation::http::service;

Io::Io(automation::http::IoTask& io_task, RikerIO::Profile& rio_profile) :
    io_task(io_task),
    rio_profile(rio_profile) {

}

Io::~Io() { };

automation::http::ExtendedApp::Handler Io::get_one() {

    return [&](auto res, auto req) {

        std::string id = req->get_parameter(0);

        id = ExtendedRequest::url_decode(id);

        RikerIO::DataPoint dp;

        if (RikerIO::Data::get(rio_profile, id, dp) == RikerIO::result_error) {
            spdlog::debug("Io::add_request({}) : Error getting data from rikerio.", id);
            throw ExtendedApp::not_found_error();
            return;
        }

        uint16_t byte_size = RikerIO::calc_bytesize(dp.bit_size);

        io_task.add_read_data(dp.byte_offset, byte_size, [id, res, dp, byte_size](char* ptr) {

            // convert ptr to valid response

            const std::string json_string = to_json(id, dp, byte_size, ptr);

            res->set_header("Content-Type", "application/json; charset=utf-8");
            res->set_data(json_string);
            res->set_status(ExtendedApp::http_status_ok);
            res->resume();
        });

        res->wait();

    };

}

automation::http::ExtendedApp::Handler Io::update() {

    return [&](auto res, auto req) {

        std::string data_id = req->get_parameter(0);

        data_id = automation::http::ExtendedRequest::url_decode(data_id);

        spdlog::debug("{}:{} Data Id after url decoding = {}.", req->get_method(), req->get_url(), data_id);

        Json::Value json_body = automation::http::parser::json::parse(req->get_body());

        if (!json_body.isObject()) {
            throw ExtendedApp::bad_request_error("Json Body musst be an object.");
        }

        Json::Value json_value = json_body["value"];

        if (!json_value) {
            throw ExtendedApp::bad_request_error("Missing key 'value' in json body.");
        }


        RikerIO::DataPoint dp;

        if (RikerIO::Data::get(rio_profile, data_id, dp) == RikerIO::result_error) {
            throw ExtendedApp::bad_request_error("Data with requested id not found.");
        }

        if (dp.type == RikerIO::Type::BIT || dp.type == RikerIO::Type::BOOL) {

            if (!json_value.isBool()) {
                throw ExtendedApp::bad_request_error("Value musst be boolean");
            }

            io_task.add_write_data<bool>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asBool(), []() { });

        } else if (dp.type == RikerIO::Type::UINT8) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be UInt8");
            }

            io_task.add_write_data<uint8_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
                json_value.asUInt(),
            []() { });

        } else if (dp.type == RikerIO::Type::INT8) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be Int8");
            }

            io_task.add_write_data<int8_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asInt(), []() {

            });

        } else if (dp.type == RikerIO::Type::UINT16) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be UInt16");
            }

            io_task.add_write_data<uint16_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asUInt(), []() { });

        } else if (dp.type == RikerIO::Type::INT16) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be Int16");
            }

            io_task.add_write_data<int16_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asInt(), []() {
            });

        } else if (dp.type == RikerIO::Type::UINT32) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be UInt32");
            }

            io_task.add_write_data<uint32_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asUInt(), []() { });

        } else if (dp.type == RikerIO::Type::INT32) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be Int32");
            }

            io_task.add_write_data<int32_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asInt(), []() { });

        } else if (dp.type == RikerIO::Type::UINT64) {

            if (!json_value.isUInt64()) {
                throw ExtendedApp::bad_request_error("Value musst be UInt64");
            }

            io_task.add_write_data<uint64_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asUInt64(), [] () { });

        } else if (dp.type == RikerIO::Type::INT64) {

            if (!json_value.isInt64()) {
                throw ExtendedApp::bad_request_error("Value musst be Int64");
            }

            io_task.add_write_data<int64_t>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asInt64(), []() { });

        } else if (dp.type == RikerIO::Type::FLOAT) {

            if (!json_value.isNumeric()) {
                throw ExtendedApp::bad_request_error("Value musst be Float");
            }

            io_task.add_write_data<float>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asFloat(), []() { });

        } else if (dp.type == RikerIO::Type::DOUBLE) {

            if (!json_value.isDouble()) {
                throw ExtendedApp::bad_request_error("Value musst be Double");
            }

            io_task.add_write_data<float>(
                dp.byte_offset,
                dp.bit_index,
                dp.bit_size,
            json_value.asDouble(), []() { });

        } else {

            if (!json_value.isString()) {
                throw ExtendedApp::bad_request_error("Value musst be base64 encoded string");
            }

            try {
                const std::string base64_decoded_string = json_value.asString();
                const std::string base64_encoded_string = base64_decode(base64_decoded_string);
                unsigned int byte_size = 3 * ((float) base64_decoded_string.length() / (float) 4);

                if (base64_decoded_string.length() > 0 && base64_decoded_string.back() == '=') {
                    byte_size -= 1;
                }

                if (base64_decoded_string.length() > 1 && base64_decoded_string[base64_decoded_string.length() - 2] == '=') {
                    byte_size -= 1;
                }

                char* char_str = (char*) calloc(1, byte_size);
                memcpy(char_str, base64_encoded_string.c_str(), byte_size);

                io_task.add_write_data(dp.byte_offset, byte_size, (char*) char_str, []() { });

                free(char_str);
            } catch (...) {
                throw ExtendedApp::bad_request_error("Possible error while decoding base64 value.");
            }

        }
        res->set_status(ExtendedApp::http_status_ok);
    };

}


void Io::trigger() {

    /* iterate res_map */

    std::set<std::string> remove_set;

    for (auto it : res_map) {

        //spdlog::debug("Io::trigger() : Checking stored request for id {}.", it.first);

        auto data_it = data_map.find(it.first);

        if (data_it == data_map.end()) {
            continue;
        }

        auto data = data_it->second;

        if (!data.updated) {
            continue;
        }

        data.updated = false;

        Json::Value json;
        JSONCPP_STRING err;
        std::string json_string;


        while (it.second.size() > 0) {

            if (data.type == "string" || data.type == "undefined" || data.type == "binary") {

                json_string = "{\"meta\":{\"urn\":\"/api/1/io/" + data.id + "\"},\"data\":{\"id\":\""+data.id+"\",\"value\":\""+data.string_value+"\",\"type\":\""+data.type+"\",\"size\":" + std::to_string(data.bit_size) + "}}";
            } else {
                json_string = "{\"meta\":{\"urn\":\"/api/1/io/" + data.id + "\"},\"data\":{\"id\":\""+data.id+"\",\"value\":"+data.string_value+",\"type\":\""+data.type+"\",\"size\":" + std::to_string(data.bit_size) + "}}";
            }

            auto ptr = it.second.front();
            it.second.pop();

            ptr.http_res->set_header("Content-Type", "application/json; charset=utf-8");
            ptr.http_res->set_data(json_string);
            ptr.http_res->set_status(ExtendedApp::http_status_ok);
            ptr.http_res->resume();
        }

        remove_set.insert(it.first);

    }

    for (auto& k : remove_set) {
        res_map.erase(k);
        data_map.erase(k);
    }

}

const std::string Io::to_json(
    const std::string& id,
    const RikerIO::DataPoint& dp,
    unsigned int byte_size,
    char* ptr) {

    std::string json_string = "";
    const std::string string_datatype = model::RioData::type_to_string(dp.type);
    const std::string string_value = translate_buffer(byte_size, (uint8_t*) ptr, dp);

    if (string_datatype == "string" || string_datatype == "undefined" || string_datatype == "binary") {

        json_string = "{\"meta\":{\"urn\":\"/api/1/io/" + id + "\"},\"data\":{\"id\":\""+id+"\",\"value\":\""+string_value+"\",\"type\":\""+string_datatype+"\",\"size\":" + std::to_string(dp.bit_size) + "}}";
    } else {
        json_string = "{\"meta\":{\"urn\":\"/api/1/io/" + id + "\"},\"data\":{\"id\":\""+id+"\",\"value\":"+string_value+",\"type\":\""+string_datatype+"\",\"size\":" + std::to_string(dp.bit_size) + "}}";
    }

    return json_string;

}
