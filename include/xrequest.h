#ifndef __AUTOMATION_EXTENDED_HTTP_REQUEST_H__
#define __AUTOMATION_EXTENDED_HTTP_REQUEST_H__

#include "models/group.h"
#include "models/permission.h"
#include "models/session.h"
#include "models/user.h"
#include "spdlog/spdlog.h"
#include "stdexcept"
#include "uWebSockets/App.h"

namespace automation::http {

class ExtendedApp;

class ExtendedRequest {

public:
  ExtendedRequest(uWS::HttpRequest req);
  ~ExtendedRequest();

  void set_url(std::string);
  void set_session(std::shared_ptr<model::Session> s);
  void set_user(std::shared_ptr<model::User> u);
  void set_group(std::shared_ptr<model::Group> g);
  void set_body(const std::string &data);

  void add_permission(std::shared_ptr<model::Permission>);
  const std::vector<std::shared_ptr<model::Permission>> &
  get_permission_list() const;

  const std::string_view get_url();
  const std::string_view get_method();
  const std::string get_parameter(unsigned int index);
  const std::string &get_query(const std::string &, const std::string &);
  const std::string_view get_header(const std::string &key);
  const std::string *get_cookie(const std::string &key) const;
  const std::shared_ptr<model::Session> get_session() const;
  const std::shared_ptr<model::User> get_user() const;
  const std::shared_ptr<model::Group> get_group() const;
  const std::string_view get_body() const;

  static std::string url_decode(const std::string &);

private:
  uWS::HttpRequest uws_request;
  std::string decoded_url;
  std::string http_method;
  std::map<std::string, std::string> cookie_map;
  std::map<std::string, std::string> query_map;
  std::map<std::string, std::string> header_map;

  std::shared_ptr<model::Session> session;
  std::shared_ptr<model::User> user;
  std::shared_ptr<model::Group> group;

  std::vector<std::shared_ptr<model::Permission>> permission_list;

  std::string string_body;

private:
  void parse_cookies();
  void parse_query();
  void copy_header(uWS::HttpRequest &);
  void copy_method(uWS::HttpRequest &);

  void add_cookie(const std::string &, const std::string &);
  void add_query(const std::string &, const std::string &);
};

} // namespace automation::http

#endif
