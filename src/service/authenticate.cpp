#include "service/authenticate.h"

using namespace automation::http::service;

Authenticate::Authenticate(
    factory::User& user_factory,
    factory::Permission& permission_factory,
    factory::Key& key_factory,
    factory::JWT& jwt_factory) :
    user_factory(user_factory),
    permission_factory(permission_factory),
    key_factory(key_factory),
    jwt_factory(jwt_factory) { }

Authenticate::~Authenticate() { }

automation::http::ExtendedApp::Handler Authenticate::authenticate() {

    return [&](auto res, auto req) {

        /*
        * 1. if there are no data in the request, use the jwt token!
        * 2. if there is already a user present in the request,
        *    return user object
        * 3. if body contains "username" and "password" (sha256) then
        *    validate and return user object
        * 4. if body contains "key" (sha256) then validate key
        *    and return user object
        */

        if (req->get_user() != user_factory.get_user_by_username("nobody")) {
            spdlog::debug("{} {} : User in request present, ignoring json body and sending current user response.", req->get_method(), req->get_url());
            send_response(req->get_user(), res);
            return;
        }

        std::shared_ptr<model::User> user;

        auto json = automation::http::parser::json::parse(req->get_body());

        if (!json.isObject()) {
            spdlog::debug("{} {} : Json Object in body is no object.", req->get_method(), req->get_url());
            throw ExtendedApp::bad_request_error("Body musst be json object.");
        }

        spdlog::debug("{} {} : Checking json body for username/password/key combo.", req->get_method(), req->get_url());

        auto username = json["username"];
        auto password = json["password"];
        auto key = json["key"];

        if (username && password && !key) {
            spdlog::debug("{} {} : Authentication by credentials.", req->get_method(), req->get_url());
            auth_with_credentials(req, res, username, password);
        } else if (!username && !password && key) {
            spdlog::debug("{} {} : Authentication by key.", req->get_method(), req->get_url());
            auth_with_key(req, res, key);
        } else {
            spdlog::debug("{} {} : Missing username/password or key field in json object.", req->get_method(), req->get_url());
            throw ExtendedApp::bad_request_error("Missing username/password or key.");

        }

    };

}

void Authenticate::auth_with_credentials(
    std::shared_ptr<ExtendedRequest> req,
    std::shared_ptr<ExtendedResponse> res,
    Json::Value username,
    Json::Value password) {

    if (!username.isString() || !password.isString()) {
        spdlog::debug("{} {} : Username and/or Password fields are not strings in json object.", req->get_method(), req->get_url());
        throw ExtendedApp::bad_request_error("Username/Password are no strings.");
    }

    spdlog::debug("{} {} : Json Body parsed with Username and/or Password.", req->get_method(), req->get_url());

    std::shared_ptr<model::User> user;


    try {
        spdlog::debug("{} {} : Trying to validate user {} with password {}.",
                      req->get_method(), req->get_url(), username.asString(), password.asString());
        user = user_factory.verify(username.asString(), password.asString());
    } catch (factory::not_found_error& e) {
        spdlog::debug("{} {} : Username not found {}.", req->get_method(), req->get_url(), username.asString());
        throw ExtendedApp::not_found_error("Username not found.");
    } catch (factory::User::verification_error& e) {
        spdlog::debug("{} {} : Username and/or password don't match.", req->get_method(), req->get_url());
        throw ExtendedApp::bad_request_error("Username or Password don't match.");
    } catch (...) {
        spdlog::error("{} {} : Unknown error emerged.", req->get_method(), req->get_url());
        throw ExtendedApp::internal_error();
    }

    if (user->is_disabled()) {
        spdlog::debug("{} {} : Key is dissabled.", req->get_method(), req->get_url());
        throw ExtendedApp::bad_request_error("Key disabled.");
    }

    spdlog::debug("{} {} : Validation successfull, sending user information.", req->get_method(), req->get_url());

    req->set_user(user);

    send_response(user, res);

};

void Authenticate::auth_with_key(
    std::shared_ptr<ExtendedRequest> req,
    std::shared_ptr<ExtendedResponse> res,
    Json::Value json_key) {

    if (!json_key.isString()) {
        spdlog::debug("{} {} : Key field is no strings in json object.", req->get_method(), req->get_url());
        throw ExtendedApp::bad_request_error("Key is no string.");
    }

    std::shared_ptr<model::Key> key;
    std::shared_ptr<model::User> user;
    std::string str_key = json_key.asString();

    try {
        spdlog::debug("{} {} : Looking for key {}.", req->get_method(), req->get_url(), str_key);

        key = key_factory.get_one(str_key);
        spdlog::debug("{} {} : Found key in key factory.", req->get_method(), req->get_url());

    } catch (factory::not_found_error& e) {
        spdlog::debug("{} {} : Key/User/Group not found {}.", req->get_method(), req->get_url(), json_key.asString());
        throw ExtendedApp::not_found_error("Key/User/Group not found.");
    } catch (...) {
        spdlog::error("{} {} : Unknown error emerged.", req->get_method(), req->get_url());
        throw ExtendedApp::internal_error();
    }

    if (key->is_disabled()) {
        spdlog::debug("{} {} : Key is dissabled.", req->get_method(), req->get_url());
        throw ExtendedApp::bad_request_error("Key disabled.");
    }

    try {
        user = user_factory.get_one(key->get_user_id());
        spdlog::debug("{} {} : Found associated user {}.", req->get_method(), req->get_url(), key->get_user_id());

    } catch (factory::not_found_error& e) {
        spdlog::debug("{} {} : Key/User/Group not found {}.", req->get_method(), req->get_url(), json_key.asString());
        throw ExtendedApp::not_found_error("Key/User/Group not found.");
    } catch (...) {
        spdlog::error("{} {} : Unknown error emerged.", req->get_method(), req->get_url());
        throw ExtendedApp::internal_error();
    }

    spdlog::debug("{} {} : Validation successfull, sending user information.", req->get_method(), req->get_url());

    req->set_user(user);

    send_response(user, res);

}

void Authenticate::send_response(
    std::shared_ptr<model::User> user,
    std::shared_ptr<ExtendedResponse> res) {

    Json::Value response = to_json(*user, permission_factory.get_all_by_user(user->get_id()));

    res->set_header("Content-Type", "application/json; charset=utf-8");
    std::string resp_string = automation::http::parser::json::stringify(response);
    res->set_data(resp_string);
    res->set_status(ExtendedApp::http_status_ok);

}

automation::http::ExtendedApp::Handler Authenticate::is_authenticated() {
    return [&](auto, auto) {

        /* auto session = req->get_session();

        try {
            auto user_id = session->get_user_id();
            auto user = user_factory.get_one(user_id);
            req->set_user(user);
        } catch (...) {
            throw ExtendedApp::unauthorized_error();
        }

        res->set_status(ExtendedApp::http_status_ok);
        */
    };
}

Json::Value Authenticate::to_json(
    const model::User& user,
    const std::vector<std::shared_ptr<model::Permission>>& permission_list) const {

    Json::Value value = Json::objectValue;

    Json::Value jwt_result;

    jwt_result["token"] = jwt_factory.create_token(user);

    Json::Value user_result;

    user_result["id"] = user.get_id();
    user_result["name"] = user.get_name();
    user_result["disabled"] = user.is_disabled();
    user_result["username"] = user.get_username();

    Json::Value permission_result;

    for (auto p : permission_list) {
        Json::Value tmp;
        tmp["id"] = p->get_id();
        tmp["userid"] = p->get_user_id();
        tmp["type"] = p->get_type() == model::Permission::Type::ALLOW ? "allow" : "deny";
        tmp["method"] = p->get_method_pattern();
        tmp["uri"] = p->get_uri_pattern();

        permission_result.append(tmp);
    }

    value["jwt"] = jwt_result;
    value["user"] = user_result;
    value["permission"] = permission_result;

    return value;

}


