#ifndef __AUTOMATION_HTTP_JSON_SINGLE_RESPONSE_H__
#define __AUTOMATION_HTTP_JSON_SINGLE_RESPONSE_H__

#include "xapp.h"
#include "vector"
#include "memory"
#include "functional"
#include "response-data-handler.h"
#include "parser/json.h"
#include "json/json.h"

namespace automation::http::json {

template <typename T>
class SingleResponse : public ResponseHandler {

  public:

    SingleResponse(
        const std::string& base_uri,
        std::function<Json::Value(T&)> jsonify) :
        base_uri(base_uri),
        jsonify(jsonify)  {

    }


    void set_response_body(ExtendedRequest& req, ExtendedResponse& res) const {

        Json::Value json_object;
        Json::Value meta_object;
        Json::Value data_object = jsonify(*data);

        meta_object["uri"] = base_uri + "/" + data->get_id();
        json_object["meta"] = meta_object;
        json_object["data"] = data_object;

        res.set_header("Content-Type", "application/json; charset=utf-8");
        res.set_data(automation::http::parser::json::stringify(json_object));

    }

    void set_data(std::shared_ptr<T> value) {
        data = value;
    }

    void filter(std::function<bool(MetaObject&)>) { }

  private:

    const std::string base_uri;
    std::function<Json::Value(T&)> jsonify;
    std::shared_ptr<T> data;

};

}



#endif
