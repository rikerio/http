#ifndef __AUTOMATION_HTTP_FACTORY_JWT_H__
#define __AUTOMATION_HTTP_FACTORY_JWT_H__

#include "service/service.h"
#include "factory/user.h"
#include "xapp.h"
#include "config.h"


namespace automation::http::factory {

class JWT {

  public:

    class decoding_error : public std::exception { };

    JWT(factory::User& user_factory, std::string secret);

    const std::string create_token(const model::User& user);
    const std::string decode_token(std::string& token);

  private:
    factory::User& user_factory;
    std::string secret;

};

}

#endif
