#include "xrequest.h"
#include "xapp.h"

using namespace automation::http;

ExtendedRequest::ExtendedRequest(uWS::HttpRequest req) : uws_request(req) {
    spdlog::debug("{}:{} : Creating new request.", req.getMethod(), req.getUrl());

    decoded_url = url_decode(std::string(uws_request.getUrl()));
    copy_header(req);
    copy_method(req);
    // parse_cookies();
    parse_query();

}

ExtendedRequest::~ExtendedRequest() {
    spdlog::debug("{}:{} : Deleting request.", uws_request.getMethod(), uws_request.getUrl());
}

void ExtendedRequest::set_url(std::string url) {
    decoded_url = url;
}

const std::string_view ExtendedRequest::get_method() {
    return http_method;
}

const std::string_view ExtendedRequest::get_url() {
    return decoded_url;
}

const std::string ExtendedRequest::get_parameter(unsigned int index) {
    return std::string(uws_request.getParameter(index));
}

const std::string& ExtendedRequest::get_query(const std::string& key, const std::string& default_value = "") {
    auto it = query_map.find(key);
    if (it == query_map.end()) {
        return default_value;
    }
    return it->second;
}

const std::string_view ExtendedRequest::get_header(const std::string& key) {
    auto it = header_map.find(key);
    if (it == header_map.end()) {
        return std::string_view({nullptr}, 0);
    }
    return it->second;
}

const std::string* ExtendedRequest::get_cookie(const std::string& key) const {
    auto it = cookie_map.find(key);
    if (it == cookie_map.end()) {
        return nullptr;
    }
    return &(it->second);
}

const std::shared_ptr<model::Session> ExtendedRequest::get_session() const {
    return session;
}

void ExtendedRequest::set_session(std::shared_ptr<model::Session> s) {
    session = s;
}

void ExtendedRequest::set_user(std::shared_ptr<model::User> u) {
    user = u;
}

void ExtendedRequest::set_group(std::shared_ptr<model::Group> g) {
    group = g;
}

void ExtendedRequest::set_body(const std::string& data) {
    string_body = data;
}

void ExtendedRequest::add_permission(std::shared_ptr<model::Permission> value) {
    permission_list.push_back(value);
}

const std::vector<std::shared_ptr<model::Permission>>& ExtendedRequest::get_permission_list() const {
    return permission_list;
}

const std::string_view ExtendedRequest::get_body() const {
    return string_body;
}

const std::shared_ptr<model::User> ExtendedRequest::get_user() const {
    return user;
}

const std::shared_ptr<model::Group> ExtendedRequest::get_group() const {
    return group;
}

std::string ExtendedRequest::url_decode(const std::string& str) {

    std::string ret;
    char ch;
    int i, ii, len = str.length();

    for (i=0; i < len; i++) {
        if(str[i] != '%') {
            if(str[i] == '+')
                ret += ' ';
            else
                ret += str[i];
        } else {
            sscanf(str.substr(i + 1, 2).c_str(), "%x", &ii);
            ch = static_cast<char>(ii);
            ret += ch;
            i = i + 2;
        }
    }

    return ret;

}

void ExtendedRequest::parse_cookies() {

    const std::string cookie = std::string(get_header("cookie"));

    spdlog::debug("{} : Cookie Header = {}", get_url(), cookie);

    std::vector<std::string> cookie_items;
    automation::http::Utils::Split(cookie, ';', cookie_items);
    for (auto v : cookie_items) {
        spdlog::debug("{} : Cookie Key/Value {}", get_url(), v);
        std::vector<std::string> key_value;
        automation::http::Utils::Split(v, '=', key_value);
        if (key_value.size() == 2) {
            spdlog::debug("{} : Adding cookie {} = {}", get_url(), key_value[0], key_value[1]);
            add_cookie(key_value[0], key_value[1]);
        } else {
            spdlog::error("{} : Error parsing cookie.", get_url());
            for (auto c : key_value) {
                spdlog::debug("{} : {}", c);
            }
        }
    }

}

void ExtendedRequest::copy_header (uWS::HttpRequest& req) {

    for (uWS::HttpRequest::HeaderIterator h  = req.begin(); h != req.end(); ++h) {
        header_map[std::string(h.ptr->key)] = std::string(h.ptr->value);
    }

}

void ExtendedRequest::copy_method(uWS::HttpRequest& req) {
    http_method = std::string(req.getMethod());
}

void ExtendedRequest::parse_query() {

    const std::string query_string = std::string(uws_request.getQuery());

    spdlog::debug("{} : Query String = {}", get_url(), query_string);

    std::vector<std::string> query_items;
    automation::http::Utils::Split(query_string, '&', query_items);
    for (auto v : query_items) {
        spdlog::debug("{} : Query Key/Value {}", get_url(), v);
        std::vector<std::string> key_value;
        automation::http::Utils::Split(v, '=', key_value);
        if (key_value.size() == 2) {
            spdlog::debug("{} : Adding query {} = {}", get_url(), key_value[0], key_value[1]);
            add_query(key_value[0], key_value[1]);
	} else if (key_value.size() == 1) {
	    spdlog::debug("{} : Adding query {} = ''", get_url(), key_value[0]);
	    add_query(key_value[0], "");
        } else {
            spdlog::error("{} : Error parsing query.", get_url());
            for (auto c : key_value) {
                spdlog::debug("{} : {}", get_url(), c);
            }
        }
    }

}

void ExtendedRequest::add_cookie(const std::string& key, const std::string& value) {
    cookie_map[key] = value;
}

void ExtendedRequest::add_query(const std::string& key, const std::string& value) {
    query_map[key] = value;
}
