#ifndef __AUTOMATION_HTTP_FACTORY_YAML_FILE_H__
#define __AUTOMATION_HTTP_FACTORY_YAML_FILE_H__

#include "string"
#include "set"
#include "fstream"

#include "factory/base.h"
#include "factory/serializable.h"
#include "utils.h"
#include "event-emitter.h"
#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"

#define ID_LENGTH 12

namespace automation::http::factory {

template <typename T>
class YamlFile : public Serializable {

  public:

    class yaml_parse_error : public std::exception { };

  public:

    YamlFile(EventEmitter& emitter, const std::string& filename, const std::string& event_prefix) :
        filename(filename),
        event_prefix(event_prefix),
        emitter(emitter) {
    };

    virtual ~YamlFile() { }

    virtual std::shared_ptr<T> add(T& t) {

        try {

            if (t.get_id() == "") {
                const std::string id = provide_unique_id();
                t.set_id(id);
            }
        } catch (not_implemented_error& e) {
            spdlog::debug("YamlFile::add : provide_unique_id overwritten, using model id {}.", t.get_id());
        }

        spdlog::debug("YamlFile::add : checking for existing item with id {}.", t.get_id());
        auto it = t_map.find(t.get_id());

        if (it != t_map.end()) {
            spdlog::debug("YamlFile::add : item with id {} already available.", t.get_id());
            release_id(t.get_id());
            throw duplicate_error();
        }

        spdlog::debug("YamlFile::add : adding item with id {}.", t.get_id());

        std::shared_ptr<T> new_t = std::make_shared<T>(t);

        t_map[t.get_id()] = new_t;

        /* clear query cache */
        query_cache.clear();

        change_counter += 1;

        emitter.emit<T&>(event_prefix + ".added", *new_t);

        release_id(t.get_id());
        return new_t;

    }

    virtual std::shared_ptr<T> update(const std::string& id, T& t) {

        auto it = t_map.find(id);

        if (id != t.get_id()) {
            throw update_id_error();
        }

        if (it == t_map.end()) {
            throw not_found_error();
        }

        const T old_t = *t_map[id];

        *t_map[id] = t;

        /* clear query cache */
        query_cache.clear();

        emitter.emit<T&, const T&>(event_prefix + ".updated", *t_map[id], old_t);

        change_counter += 1;

        return t_map[id];

    }

    virtual std::shared_ptr<T> remove(const std::string& id) {

        auto it = t_map.find(id);

        if (it == t_map.end()) {
            throw not_found_error();
        }

        /* important to create another link to
         * the object that should be deletet,
         * otherwise the base constructor is beeing
         * called and the object cannot be handled */
        auto result = it->second;

        t_map.erase(id);

        /* clear query cache */
        query_cache.clear();

        emitter.emit<const std::string&>(event_prefix + ".removed", id);

        change_counter += 1;

        return result;

    }

    virtual std::shared_ptr<T> get_one(const std::string& id) {
        spdlog::debug("YamlFile::get_one : Looking for entity with id {}.", id);
        auto it = t_map.find(id);
        if (it == t_map.end()) {
            spdlog::debug("YamlFile::get_one : Entity with id {} not found.", id);
            throw not_found_error();
        }
        return it->second;
    }

    std::vector<std::shared_ptr<T>>& query(const std::string& filter_string, const std::string& sort, bool descending) {

        spdlog::debug("YamlFile::query : Executed with filter={}, sort={}, descending={}.", filter_string, sort, descending);

        const std::string query_str = filter_string + "/" + sort + "/" + (descending ? "desc" : "asc");

        spdlog::debug("YamlFile::query : Looking for query {}.", query_str);

        auto it = query_cache.find(query_str);

        if (it != query_cache.end()) {
            spdlog::debug("YamlFile::query : Cached query found, serving.");
            return it->second;
        }

        std::vector<std::shared_ptr<T>> current_query_list;
        for (auto fit : t_map) {

            std::shared_ptr<T> value = fit.second;

            if (filter(*value, filter_string)) {
                spdlog::debug("YamlFile::query : Filter item with id {}.", value->get_id());
                current_query_list.push_back(value);
            }

        }

        spdlog::debug("YamlFile::query : Filter done with {} items left.", current_query_list.size());

        std::sort(current_query_list.begin(), current_query_list.end(), [&](auto a, auto b) {
            return compare(sort, *a, *b) < 0;
        });

        spdlog::debug("YamlFile::query : Sorting done with {} items left.", current_query_list.size());

        if (descending) {
            std::reverse(current_query_list.begin(), current_query_list.end());
        }

        spdlog::debug("YamlFile::query : Reverse done with {} items left.", current_query_list.size());

        query_cache[query_str] = current_query_list;

        return query_cache[query_str];

    }

  public:

    const std::string reserve_new_id() {
        return provide_unique_id();
    }

    void release_id(const std::string& id) {
        /* no matter if it exists, we erase id */
        reserved_id_pool.erase(id);
    }

  public:

    void serialize() const {
        YAML::Node root_node;

        for (auto u : t_map) {
            root_node[u.first] = to_yaml(*u.second);
        }

        std::ofstream persistentFile(filename);
        persistentFile << root_node;
        persistentFile.close();
    }

    virtual void deserialize() {
        YAML::Node root_node;

        try {
            root_node = YAML::LoadFile(filename);
        } catch (...) {
            spdlog::error("Error loading session data file {}.", filename);
            return;
        }

        for (auto node : root_node) {

            /* get duration seconds for expires */

            T new_t = from_yaml(node.second);
            std::shared_ptr<T> t = std::make_shared<T>(new_t);

            t_map[t->get_id()] = t;

        }
    }

    unsigned int get_change_count() const {
        return change_counter;
    }

    void reset_change_count() {
        change_counter = 0;
    }

  private:

    const std::string filename;
    const std::string event_prefix;

  private:

    std::map<std::string, std::vector<std::shared_ptr<T>>> query_cache;

  private:

    std::set<std::string> reserved_id_pool;

  private:

    unsigned int change_counter;

  protected:

    EventEmitter& emitter;
    std::map<std::string, std::shared_ptr<T>> t_map;


  private:

    virtual YAML::Node to_yaml(const T&) const = 0;
    virtual T from_yaml(YAML::Node) const = 0;
    virtual bool filter(const T&, const std::string&) const = 0;
    virtual int compare(const std::string&, const T&, const T&) const = 0;

    virtual const std::string provide_unique_id() const {
        /* create random id try 1000 times */
        unsigned int count = 0;
        while (count <= 1000) {

            count += 1;

            const std::string id = http::Utils::create_random_token(ID_LENGTH);
            spdlog::debug("Random ID created {}.", id);
            auto it = t_map.find(id);

            if (it != t_map.end()) {
                spdlog::debug("Random ID already taken (woaaaat?) {}.", id);
                // id already taken ... woaaaat?
                continue;
            }

            if (reserved_id_pool.find(id) != reserved_id_pool.end()) {
                spdlog::debug("Random ID in reserved id pool {}.", id);
                continue;
            }

            return id;
        }

        throw duplicate_error();
    }


};

}

#endif
