#include "config.h"

#include <filesystem>

using namespace automation::http;

const Config Config::deserialize(const std::string &filename) {

  Config c{.bind = "0.0.0.0",
           .port = 80,
           .cors_enabled = false,
           .rikerio = {.profile = "default"},
           .folder = {.webapp = "/var/lib/rikerio-http/www",
                      .db = "/var/lib/rikerio-http/db",
                      .files = "/var/lib/rikerio-http/files",
                      .apps = "/var/lib/rikerio-http/apps"},
           .max_file_size = 512000000,
           .io = {.short_cycle = 10000,
                  .long_cycle = 100000,
                  .priority = 90,
                  .buffer_size = 16000000,
                  .requests_per_cycle = 10000}};

  auto root_node = YAML::LoadFile(filename);

  auto bind_node = root_node["bind"];
  auto port_node = root_node["port"];
  auto cors_enabled_node = root_node["cors"];
  auto rikerio_node = root_node["rikerio"];
  auto folder_node = root_node["folder"];
  auto max_file_size_node = root_node["max_file_size"];
  auto io_node = root_node["io"];

  if (bind_node && bind_node.Type() == YAML::NodeType::Scalar) {
    c.bind = bind_node.as<std::string>();
  }

  if (port_node && port_node.Type() == YAML::NodeType::Scalar) {
    c.port = port_node.as<unsigned int>();
  }

  if (cors_enabled_node && cors_enabled_node.Type() == YAML::NodeType::Scalar) {
    c.cors_enabled = cors_enabled_node.as<bool>();
  }

  if (rikerio_node && rikerio_node.Type() == YAML::NodeType::Map) {

    auto rikerio_profile_node = rikerio_node["profile"];

    if (rikerio_profile_node &&
        rikerio_profile_node.Type() == YAML::NodeType::Scalar) {
      c.rikerio.profile = rikerio_profile_node.as<std::string>();
    }
  }

  if (folder_node && folder_node.Type() == YAML::NodeType::Scalar) {

    auto base_folder = std::filesystem::path{folder_node.as<std::string>()};

    c.folder.webapp = base_folder / "static";
    c.folder.db = base_folder / "db";
    c.folder.files = base_folder / "files";
    c.folder.apps = base_folder / "apps";
  }

  if (max_file_size_node &&
      max_file_size_node.Type() == YAML::NodeType::Scalar) {
    c.max_file_size = max_file_size_node.as<unsigned int>();
  }

  if (io_node && io_node.Type() == YAML::NodeType::Scalar) {

    auto io_short_cycle_node = io_node["short_cycle"];
    auto io_long_cycle_node = io_node["long_cycle"];
    auto io_cycle_node = io_node["cycle"];
    auto io_priority_node = io_node["priority"];
    auto io_buffer_size_node = io_node["buffer_size"];
    auto io_requests_per_cycle_node = io_node["requests_per_cycle"];

    if (io_cycle_node && io_cycle_node.Type() == YAML::NodeType::Scalar) {
      c.io.short_cycle = io_cycle_node.as<unsigned int>();
    }

    if (io_short_cycle_node &&
        io_short_cycle_node.Type() == YAML::NodeType::Scalar) {
      c.io.short_cycle = io_short_cycle_node.as<unsigned int>();
    }

    if (io_long_cycle_node &&
        io_long_cycle_node.Type() == YAML::NodeType::Scalar) {
      c.io.long_cycle = io_long_cycle_node.as<unsigned int>();
    }

    if (io_priority_node && io_priority_node.Type() == YAML::NodeType::Scalar) {
      c.io.priority = io_priority_node.as<unsigned int>();
    }

    if (io_buffer_size_node &&
        io_buffer_size_node.Type() == YAML::NodeType::Scalar) {
      c.io.buffer_size = io_buffer_size_node.as<unsigned int>();
    }

    if (io_requests_per_cycle_node &&
        io_requests_per_cycle_node.Type() == YAML::NodeType::Scalar) {
      c.io.requests_per_cycle = io_requests_per_cycle_node.as<unsigned int>();
    }
  }

  return c;
}
