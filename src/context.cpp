#include "http/context.h"

using namespace Http;

Context::Context(std::shared_ptr<Runtime::Config> config) :
    config(config) {

    rio_client = std::make_shared<RikerIO::Client>("default");
    rio_master = std::make_shared<RikerIO::Master>(*rio_client);
    rio_alloc = rio_master->allocate(128);

    out_system_cmd_id = rio_alloc->add_data("out.default.http.system.command.id", 0, 0, "uint16");

    RikerIO::Request::v1::LinkAdd link_add_request(
        "out.default.system.command.id", { "out.default.http.system.command.id" });
    auto link_add_response = rio_client->link_add(link_add_request);

    out_system_cmd_index = rio_alloc->add_data("out.default.http.system.command.index", 2, 0, "uint16");
    out_system_cmd_arg = rio_alloc->add_data("out.default.http.system.command.arg", 4, 0, "float");

    auto out_system_flag_enabled = rio_alloc->add_data("out.default.http.system.flag.enabled", 8, 0, "bit");
    auto out_system_flag_error = rio_alloc->add_data("out.default.http.system.flag.error", 9, 0, "bit");

}

void Context::set_system_cmd(uint16_t id, uint16_t idx, float arg) {

    out_system_cmd_id->set<uint16_t>(id);
    out_system_cmd_index->set<uint16_t>(idx);
    out_system_cmd_arg->set<float>(arg);

}
