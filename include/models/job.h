#ifndef __AUTOMATION_HTTP_MODELS_JOB_H__
#define __AUTOMATION_HTTP_MODELS_JOB_H__

#include "string"
#include "set"
#include "atomic"
#include "chrono"

#include "http/models/model.h"

namespace automation::http::model {

class Job : public automation::http::model::Base {

  public:
    enum class State {
        Queued = 0,
        Processing = 1,
        Error = 2,
        Success = 3
    };

    class Request {
      public:
        Request(const std::string method, Json::Value args) : method(method), args(args) { }
        Request(Json::Value from) {

            if (!from) {
                throw json_parse_error();
            }

            Json::Value json_method = from["method"];
            Json::Value json_args = from["args"];

            if (!json_method || !json_method.isString()) {
                throw json_parse_error();
            }

            if (!json_args || !json_args.isArray()) {
                throw json_parse_error();
            }

            method = from["method"].as<std::string>();

            for (auto& a : json_args) {

                if (!a.isString() && !a.isNumeric() && !a.isBool()) {
                    throw json_parse_error();
                }
                args.push_back(a.as<std::string>());

            }
        };

      private:
        std::string method;
        std::vector<std::string> args;
    };

    class Response {
      public:
        Response(bool error, Json::Value result) :
            error(error),
            result(result) { }

        operator Json::Value () {
            Json::Value r;
            r["result"] = result;
            return r;
        };

        bool is_error() const {
            return error;
        }

      private:
        bool error = false;
        Json::Value result;
    };

  public:

    Job(const std::string& id) :id(id) { }

    Job(Json::Value) {

    }

    Job(YAML::Node n) {

    }

    const std::string& get_id() const {
        return id;
    }

    Request&& get_request() {
        return std::move(request);
    }

    Response&& get_response() {
        return std::move(response);
    }

    operator Json::Value () {

        Json::Value result;

        result["id"] = id;
        result["issued"] = issued.time_since_epoch().count();
        result["issuer"] = issuer_id;
        result["request"] = request;
        result["response"] = response;

        return result;

    }

    bool filter(const std::string value) const {

        if (value == "") {
            return true;
        }

        if (std::string::npos != issuer_id.find(value)) {
            return true;
        }

        if (std::string::npos != request.get_method().find(value)) {
            return true;
        }

        return false;

    }

    int compare(const std::string& field, const Job& b) {

        if (field == "issuer" || field == "") {
            return issuer_id.compare(b.get_issuer_id());
        }

        return 0;

    }

  private:

    std::string id;
    std::chrono::system_clock::time_point issued;
    std::string issuer_id;
    std::atomic<State> state;

    Request request;
    Response response;

};

}

namespace YAML {

template<>
struct convert<automation::http::User> {

    static Node encode(const automation::http::User& rhs) {
        YAML::Node user_node;

        user_node["username"] = rhs.get_username();
        user_node["realname"] = rhs.get_realname();
        user_node["password"] = rhs.get_password();
        user_node["group"] = rhs.get_group();
        user_node["disabled"] = rhs.is_disabled();

        return user_node;
    }

};

}

#endif
