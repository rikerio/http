#ifndef __AUTOMATION_HTTP_SERVICE_IO_H__
#define __AUTOMATION_HTTP_SERVICE_IO_H__

#include "xapp.h"
#include "utils.h"
#include "io-task.h"
#include "json/json.h"

#include "service/io-base.h"

#include "rikerio-automation/automation.h"

namespace automation::http::service {

class Io : public IoBase {

  public:

    Io(automation::http::IoTask&, RikerIO::Profile&);
    ~Io();

    ExtendedApp::Handler get_one();
    ExtendedApp::Handler update();

    void trigger();

  public:

    IoTask& io_task;
    RikerIO::Profile& rio_profile;

  private:

    struct Data {
        std::string id;
        std::string string_value;
        std::string type;
        unsigned int bit_size;
        RikerIO::DataPoint rio_datapoint;
        bool updated;
    };

    struct RequestContext {

        std::string id;
        std::string io_task_id;
        std::shared_ptr<ExtendedResponse> http_res;

    };

  private:

    Json::CharReaderBuilder json_read_builder;
    Json::StreamWriterBuilder json_write_builder;
    std::unique_ptr<Json::CharReader> json_reader;

  private:

    std::map<std::string, Data> data_map;
    std::map<std::string, std::queue<RequestContext>> res_map;

  private:

    static const std::string to_json(const std::string&, const RikerIO::DataPoint&, unsigned int, char*);

};
}

#endif
