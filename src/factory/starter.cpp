#include "factory/starter.h"

using namespace automation::http;

factory::Starter::Starter(EventEmitter& emitter, const std::string& filename, factory::App& app_factory) :
    factory::YamlFile<model::Starter>(emitter, filename, "starter"),
    app_factory(app_factory) {
    deserialize();

    std::function<void(const std::string&)> app_removed_handler = [&](const std::string& app_id) {

        std::set<std::string> remove_starter_ids;
	for (auto it : t_map) {
	    if (it.second->get_app_id() == app_id) {
	        remove_starter_ids.insert(it.first);
	    }
	}

	for (auto& starter_id : remove_starter_ids) {

	    try {
	        remove(starter_id);
	    } catch (...) {
	        spdlog::error("Error while removing starter {} for app {}.", starter_id, app_id);
	    }

	}

    };

    emitter.on<const std::string&>("app.removed", app_removed_handler);

}

factory::Starter::~Starter() {
    serialize();
}

std::shared_ptr<model::Starter> factory::Starter::add(model::Starter& model) {

    auto& app_id = model.get_app_id();

    app_factory.get_one(app_id);

    return YamlFile<model::Starter>::add(model);

}

std::shared_ptr<model::Starter> factory::Starter::update(const std::string& id, model::Starter& model) {

    auto& app_id = model.get_app_id();

    app_factory.get_one(app_id);

    return YamlFile<model::Starter>::update(id, model);

}


YAML::Node factory::Starter::to_yaml(const model::Starter& model) const {

    YAML::Node node;

    node["id"] = model.get_id();
    node["app_id"] = model.get_app_id();
    node["label"] = model.get_label();
    node["description"] = model.get_description();
    node["disabled"] = model.is_disabled();

    YAML::Node parameter_map_node;

    auto& parameter_map = model.get_parameter_map();

    for (auto it : parameter_map) {
        parameter_map_node[it.first] = it.second;
    }

    node["parameter"] = parameter_map_node;

    return node;

}

model::Starter factory::Starter::from_yaml(YAML::Node node) const {

    auto yaml_id = node["id"];
    auto yaml_app_id = node["app_id"];
    auto yaml_label = node["label"];
    auto yaml_description = node["description"];
    auto parameter_map_node = node["parameter"];
    auto yaml_disabled = node["disabled"];

    model::Starter model;

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        model.set_id(yaml_id.as<std::string>());
    }

    if (yaml_app_id && yaml_app_id.Type() == YAML::NodeType::Scalar) {
        model.set_app_id(yaml_app_id.as<std::string>());
    }

    if (yaml_label && yaml_label.Type() == YAML::NodeType::Scalar) {
        model.set_label(yaml_label.as<std::string>());
    }

    if (yaml_description && yaml_description.Type() == YAML::NodeType::Scalar) {
        model.set_description(yaml_label.as<std::string>());
    }

    if (yaml_disabled && yaml_disabled.Type() == YAML::NodeType::Scalar) {
	model.set_disabled(yaml_disabled.as<bool>());
    }

    if (parameter_map_node && parameter_map_node.Type() == YAML::NodeType::Map) {

        for (auto node_it : parameter_map_node) {

            if (node_it.first.Type() != YAML::NodeType::Scalar) {
                continue;
            }

            if (node_it.second.Type() != YAML::NodeType::Scalar) {
                continue;
            }

            model.add_parameter(node_it.first.as<std::string>(), node_it.second.as<std::string>());

        }

    }

    return model;

}

bool factory::Starter::filter(const model::Starter& model, const std::string& str) const {

    if (str == "") {
        return true;
    }

    if (std::string::npos != model.get_id().find(str)) {
        return true;
    }

    if (std::string::npos != model.get_app_id().find(str)) {
        return true;
    }

    if (std::string::npos != model.get_label().find(str)) {
        return true;
    }

    if (std::string::npos != model.get_description().find(str)) {
        return true;
    }


    return false;

}

int factory::Starter::compare(const std::string& field, const model::Starter& a, const model::Starter& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "app_id") {
        return a.get_app_id().compare(b.get_app_id());
    }

    if (field == "label") {
        return a.get_label().compare(b.get_label());
    }

    return 0;

}
