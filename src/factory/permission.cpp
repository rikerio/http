#include "factory/permission.h"

using namespace automation::http;

factory::Permission::Permission(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::Permission>(emitter, filename, "permission") {

    deserialize();

    std::function<void(const std::string&)> user_removed_handler = [&](const std::string& id) {

        std::set<std::string> remove_keys;
        for (auto it : t_map) {
            if (it.second->get_user_id() == id) {
                remove_keys.insert(it.first);
            }
        }

        for (auto& k : remove_keys) {
            t_map.erase(k);
        }

    };

    emitter.on<const std::string&>("user.removed", user_removed_handler);

}

factory::Permission::~Permission() {
    serialize();
}

YAML::Node factory::Permission::to_yaml(const model::Permission& rhs) const {
    YAML::Node key_node;

    key_node["id"] = rhs.get_id();
    key_node["userid"] = rhs.get_user_id();
    key_node["type"] = rhs.get_type() == model::Permission::Type::DENY ? "deny" : "allow";
    key_node["method"] = rhs.get_method_pattern();
    key_node["uri"] = rhs.get_uri_pattern();
    key_node["disabled"] = rhs.is_disabled();

    return key_node;
}

model::Permission factory::Permission::from_yaml(YAML::Node node) const {

    model::Permission model;

    auto yaml_id = node["id"];
    auto yaml_user_id = node["userid"];
    auto yaml_type = node["type"];
    auto yaml_method = node["method"];
    auto yaml_uri = node["uri"];
    auto yaml_disabled = node["disabled"];

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        model.set_id(yaml_id.as<std::string>());
    }

    if (yaml_user_id && yaml_user_id.Type() == YAML::NodeType::Scalar) {
        model.set_user_id(yaml_user_id.as<std::string>());
    }

    if (yaml_type && yaml_type.Type() == YAML::NodeType::Scalar) {
        std::string type_string = yaml_type.as<std::string>();
        if (type_string != "allow" && type_string != "deny") {
            throw yaml_parse_error();
        }
        model.set_type(type_string == "allow" ? model::Permission::Type::ALLOW : model::Permission::Type::DENY);
    }

    if (yaml_method && yaml_method.Type() == YAML::NodeType::Scalar) {
        model.set_method_pattern(yaml_method.as<std::string>());
    }

    if (yaml_uri && yaml_uri.Type() == YAML::NodeType::Scalar) {
        model.set_uri_pattern(yaml_uri.as<std::string>());
    }

    if (yaml_disabled && yaml_disabled.Type() == YAML::NodeType::Scalar) {
	model.set_disabled(yaml_disabled.as<bool>());
    }

    return model;
}

bool factory::Permission::filter(const model::Permission& model, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != model.get_id().find(value)) {
        return true;
    }

    if (std::string::npos != model.get_user_id().find(value)) {
        return true;
    }

    if (model.get_type() == model::Permission::Type::ALLOW && value == "allow") {
        return true;
    }

    if (model.get_type() == model::Permission::Type::DENY && value == "deny") {
        return true;
    }

    return false;

}

int factory::Permission::compare(const std::string& field, const model::Permission& a, const model::Permission& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "user") {
        return a.get_user_id().compare(b.get_user_id());
    }

    return 0;

}

void factory::Permission::deserialize() {

    YamlFile<model::Permission>::deserialize();

    for (auto it : t_map) {

        user_permission_list.insert({ it.second->get_user_id(), it.second });

    }


}
