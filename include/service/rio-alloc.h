#ifndef __AUTOMATION_HTTP_SERVICE_RIO_ALLOC_H__
#define __AUTOMATION_HTTP_SERVICE_RIO_ALLOC_H__

#include "factory/rio-alloc.h"
#include "service/service.h"

namespace automation::http::service {

class RioAlloc : public FactoryService<factory::RioAlloc, model::RioAlloc> {

  public:

    RioAlloc(const std::string& base_uri, factory::RioAlloc&);

  private:

    Json::Value to_json(const model::RioAlloc&) const;
    model::RioAlloc from_json(Json::Value, const std::string&) const;

};

}

#endif
