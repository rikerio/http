#include "service/websocket.h"

#include "spdlog/spdlog.h"

using namespace automation::http::service;

void WebSocket::send_error_response(uWS::WebSocket<false, true>* ws, Json::Value payload, std::string_view message) {

    if (!payload["id"]) {
        spdlog::debug("WebSocket Request without client request id, not issuing a response.");
        return;
    }

    Json::Value error_response;
    error_response["id"] = payload["id"];
    error_response["msg"] = std::string(message);

    WebSocket::send_json(ws, error_response);

}

void WebSocket::send_success_response(uWS::WebSocket<false, true>* ws, Json::Value payload, Json::Value response) {

    if (!payload["id"]) {
        spdlog::debug("WebSocket Request without client request id, not issuing a response.");
        return;
    }

    Json::Value json_response;
    json_response["id"] = payload["id"];
    json_response["data"] = response;;

    WebSocket::send_json(ws, json_response);

}

void WebSocket::send_json(uWS::WebSocket<false, true>* ws, Json::Value json) {

    static Json::StreamWriterBuilder json_write_builder;
    json_write_builder["indentation"] = "";

    const std::string json_message = Json::writeString(json_write_builder, json);
    ws->send(json_message, uWS::OpCode::TEXT);

}

WebSocket::WebSocket() :
    json_read_builder(),
    json_reader(json_read_builder.newCharReader()) {

    behavior = {
        .compression = uWS::SHARED_COMPRESSOR,
        .maxPayloadLength = 16 * 1024,
        .idleTimeout = 3600,
        .maxBackpressure = 1 * 1024 * 1204,
        .upgrade = nullptr,
        .open = [&](auto *ws) {

            spdlog::debug("WebSocket: New connection established.");

            websocket_set.insert(ws);

            for (auto s : service_map) {
                if (!s.second) {
                    continue;
                }

                s.second->on_connected(ws);

            }

        },
        .message = [&](auto *ws, std::string_view message, uWS::OpCode opcode) {

            spdlog::debug("WebSocket : Receiving data {}", message);

            if (opcode != uWS::OpCode::TEXT) {
                spdlog::debug("WebSocket : opcode != uWS::OpCode::Text, ignoring message.");
                // do nothing
                return;
            }

            Json::Value root;
            JSONCPP_STRING err;
            std::string str_message = std::string(message);
            auto raw_json_length = static_cast<int>(message.length());
            auto start = str_message.c_str();
            auto end = start + raw_json_length;
            if (!json_reader->parse(start, end, &root, &err)) {
                spdlog::debug("WebSocket : JSON Parse error on body data {} {}.", message, err);
                return;
            }

            if (!root || !root.isObject()) {
                // TODO: send error
                spdlog::error("Ill formated json.");
                return;
            }

            Json::Value target_service_node = root["service"];

            if (!target_service_node || !target_service_node.isString()) {
                // TODO: send error
                spdlog::error("Ill formated json (no target service).");
                return;
            }

            std::string target_service_id = target_service_node.as<std::string>();
            Json::Value payload = root["payload"];

            auto it = service_map.find(target_service_id);

            if (it == service_map.end()) {
                spdlog::debug("WebSocket : No such service {}.", target_service_id);
                // TODO: send error "no such service" message
                return;
            }

            if (it->second) {
                spdlog::debug("WebSocket : Handling payload.");
                it->second->on_data(ws, payload);
            }

        },
        .drain = NULL,
        .ping = [](auto *ws) {
            ws->send("", uWS::OpCode::PONG);
        },
        .pong = [](auto *ws) {
            ws->send("", uWS::OpCode::PING);
        },
        .close = [&](auto *ws, int /*code*/, std::string_view /*message*/) {

            spdlog::debug("Websocket: Connection closed.");

            for (auto s : service_map) {
                if (!s.second) {
                    continue;
                }

                s.second->on_close(ws);

            }

            websocket_set.erase(ws);

        }

    };
}

WebSocket::operator uWS::App::WebSocketBehavior& () {
    return behavior;
}

void WebSocket::register_service(const std::string& id, Service& s) {
    auto it = service_map.find(id);

    if (it != service_map.end()) {
        // TODO: throw service id error
        return;
    }

    service_map[id] = &s;

}

void WebSocket::close_all() {

    std::for_each(websocket_set.begin(), websocket_set.end(), [](auto ws) {
        ws->close();
    });

}
