#ifndef __DEPOT_AUTOMATION_HTTP_FILE_SERVICE_H__
#define __DEPOT_AUTOMATION_HTTP_FILE_SERVICE_H__

#include "xapp.h"
#include "functional"
#include "filesystem"

namespace automation::http::service {

class Static {

  public:

    Static(const std::string&, const std::string&);

    static automation::http::ExtendedApp::Handler replace_url(std::string replacement);

    automation::http::ExtendedApp::Handler operator ()(
        const std::string&,
        std::function<void(const std::filesystem::path&, std::string&)> = nullptr);

  private:

    const std::string base_path;
    const std::string index_file = "index.html";

};


}



#endif
