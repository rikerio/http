#ifndef __AUTOMATION_HTTP_LIST_NODE_H__
#define __AUTOMATION_HTTP_LIST_NODE_H__

namespace depot::http {

template<typename T>
struct ListNode {
    T* item;
    ListNode* next;
};

};


#endif
