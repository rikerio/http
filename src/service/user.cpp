#include "service/user.h"

using namespace automation::http;

service::User::User(const std::string& base_uri, factory::User& user_factory) :
    automation::http::service::FactoryService<factory::User, model::User>(base_uri, user_factory) {}

Json::Value service::User::to_json(const model::User& user) const {

    Json::Value result;

    result["id"] = user.get_id();
    result["username"] = user.get_username();
    result["name"] = user.get_name();
    result["disabled"] = user.is_disabled();

    return result;

}


model::User service::User::from_json(Json::Value body, const std::string& method) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_username = body["username"];
    auto json_password = body["password"];
    auto json_name = body["name"];
    auto json_disabled = body["disabled"];

    if (method == "create") {

        if (!json_password || !json_password.isString()) {
            spdlog::debug("No password present.");
            throw ExtendedApp::bad_request_error("Missing password in request body.");
        }

    }

    if (!json_username || !json_username.isString()) {
        spdlog::debug("No username present.");
        throw ExtendedApp::bad_request_error("Missing username in request body.");
    }

    if (!json_name || !json_name.isString()) {
        spdlog::debug("No name present.");
        throw ExtendedApp::bad_request_error("Missing name in request body.");
    }

    if (!json_disabled || !json_disabled.isBool()) {
        spdlog::debug("No disabled present.");
        throw ExtendedApp::bad_request_error("Missing disabled state in request body.");
    }

    const std::string id = json_id.as<std::string>();
    const std::string username = json_username.as<std::string>();
    const std::string password = json_password ? json_password.as<std::string>() : "";
    const std::string name = json_name.as<std::string>();
    const bool disabled = json_disabled.as<bool>();

    if (username.length() < 3) {
        spdlog::debug("Username length < 3.");
        throw ExtendedApp::bad_request_error("Username to short.");
    }

    if (json_password && password.length() != PASSWORD_HASH_SIZE) {
        spdlog::debug("Password hash does not fit {}.length != {}.", password, PASSWORD_HASH_SIZE);
        throw ExtendedApp::bad_request_error("No SHA-256 Password.");
    }

    if (name.length() == 0) {
        spdlog::debug("Name to small.");
        throw ExtendedApp::bad_request_error("Name to small.");
    }

    model::User user;
    user.set_id(id)
    .set_username(username)
    .set_password(password)
    .set_name(name)
    .set_disabled(disabled);

    return user;

}
