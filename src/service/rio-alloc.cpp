#include "service/rio-alloc.h"

using namespace automation::http;

service::RioAlloc::RioAlloc(const std::string& base_uri, factory::RioAlloc& factory) :
    FactoryService(base_uri, factory) { };

Json::Value service::RioAlloc::to_json(const model::RioAlloc& a) const {

    Json::Value result;

    result["id"] = a.get_id();
    result["offset"] = a.get_offset();
    result["size"] = a.get_size();

    return result;

}

model::RioAlloc service::RioAlloc::from_json(Json::Value, const std::string&) const {
    throw ExtendedApp::not_implemented_error();
}
