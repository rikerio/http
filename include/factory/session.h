#ifndef __AUTOMATION_HTTP_MIDDLEWARE_SESSION_FACTORY_H__
#define __AUTOMATION_HTTP_MIDDLEWARE_SESSION_FACTORY_H__

#include "string"
#include "map"
#include "stdexcept"
#include "sstream"
#include "iomanip"
#include "memory"
#include "fstream"

#include "factory/yaml-file.h"
#include "models/session.h"

#include "event-emitter.h"
#include "yaml-cpp/yaml.h"
#include "spdlog/spdlog.h"

namespace automation::http::factory {

class Session : public YamlFile<model::Session> {

  public:

    class not_found_error : public std::exception { };

  public:

    Session(EventEmitter& emitter, const std::string& filename);
    ~Session();

  private:

    YAML::Node to_yaml(const model::Session&) const;
    model::Session from_yaml(YAML::Node) const;
    bool filter(const model::Session&, const std::string&) const;
    int compare(const std::string&, const model::Session&, const model::Session&) const;


};

}

#endif
