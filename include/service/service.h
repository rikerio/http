#ifndef __AUTOMATION_HTTP_SERVICE_TEMPLATE_H__
#define __AUTOMATION_HTTP_SERVICE_TEMPLATE_H__

#include "xapp.h"
#include "factory/base.h"
#include "spdlog/spdlog.h"
#include "parser/json.h"
#include "json/list-response.h"
#include "json/single-response.h"

namespace automation::http::service {

template <typename F, typename T>
class FactoryService {

  public:

    FactoryService(const std::string& base_uri, F& factory) : base_uri(base_uri), factory(factory) { }
    virtual ~FactoryService() { }

    virtual automation::http::ExtendedApp::Handler create() {

        return [&](auto res, auto req) {

            spdlog::debug("{} {} : New create request.", req->get_method(), req->get_url());
            spdlog::debug("{} {} : ", req->get_method(), req->get_url(), req->get_body());
            auto json_body = automation::http::parser::json::parse(req->get_body());
            T t = from_json(json_body, "create");

            try {

                spdlog::debug("{} {} : Passing object to factory.", req->get_method(), req->get_url());
                auto shared_t = factory.add(t);
                std::shared_ptr<json::SingleResponse<T>> response_handler =
                std::make_shared<json::SingleResponse<T>>(base_uri, [&](T& obj) -> Json::Value {
                    return to_json(obj);
                });
                response_handler->set_data(shared_t);

                res->set_data(response_handler);
                res->set_status(ExtendedApp::http_status_created);

            } catch (typename automation::http::factory::duplicate_error& e) {
                spdlog::error("Error adding resource, dulicate id.");
                throw ExtendedApp::bad_request_error("Duplicate id.");
            }

        };

    }

    virtual automation::http::ExtendedApp::Handler get_one() {

        return [&](auto res, auto req) {

            const std::string& id = req->get_parameter(0);

            try {
                auto t = factory.get_one(id);

                std::shared_ptr<json::SingleResponse<T>> response_handler =
                std::make_shared<json::SingleResponse<T>>(base_uri, [&](T& obj) -> Json::Value {
                    return to_json(obj);
                });
                response_handler->set_data(t);

                res->set_data(response_handler);
                res->set_status(ExtendedApp::http_status_ok);

            } catch (typename automation::http::factory::not_found_error& e) {
                throw ExtendedApp::not_found_error();
            }

        };

    }

    virtual automation::http::ExtendedApp::Handler get_all() {
        return [&](auto res, auto req) {

            const std::string filter_string = req->get_query("filter", "");
            const std::string sort_string = req->get_query("sort", "");
            const std::string sort_order_string = req->get_query("sort_order", "asc");

            bool descending = sort_order_string == "desc";

            std::vector<std::shared_ptr<T>>& result_list = factory.query(filter_string, sort_string, descending);


            std::shared_ptr<json::ListResponse<T>> response_handler =
            std::make_shared<json::ListResponse<T>>(base_uri, [&](T& obj) -> Json::Value {
                return to_json(obj);
            });
            response_handler->set_data(result_list);

            res->set_data(std::static_pointer_cast<ResponseHandler>(response_handler));
            res->set_status(ExtendedApp::http_status_ok);

        };

    }


    virtual automation::http::ExtendedApp::Handler remove() {
        return [&](auto res, auto req) {

            const std::string& id = req->get_parameter(0);

            try {
                auto t = factory.remove(id);

                std::shared_ptr<json::SingleResponse<T>> response_handler =
                std::make_shared<json::SingleResponse<T>>(base_uri, [&](T& obj) -> Json::Value {
                    return to_json(obj);
                });
                response_handler->set_data(t);

                res->set_data(response_handler);
                res->set_status(ExtendedApp::http_status_ok);

            } catch (typename automation::http::factory::fixed_item_error& e) {
                throw ExtendedApp::bad_request_error("Resource '" + id + "' cannot be removed.");
            } catch (typename automation::http::factory::not_found_error& e) {
                throw ExtendedApp::not_found_error();
            }

        };

    }

    virtual automation::http::ExtendedApp::Handler update() {

        return [&](auto res, auto req) {

            const std::string& id = req->get_parameter(0);

            auto json_body = automation::http::parser::json::parse(req->get_body());
            auto new_t = from_json(json_body, "update");

            try {
                auto shared_t = factory.update(id, new_t);

                std::shared_ptr<json::SingleResponse<T>> response_handler =
                std::make_shared<json::SingleResponse<T>>(base_uri, [&](T& obj) -> Json::Value {
                    return to_json(obj);
                });
                response_handler->set_data(shared_t);

                res->set_data(response_handler);
                res->set_status(ExtendedApp::http_status_ok);

            } catch (typename automation::http::factory::not_found_error& e) {
                throw ExtendedApp::not_found_error();
            } catch (typename automation::http::factory::fixed_item_error& e) {
                throw ExtendedApp::bad_request_error("Resource '" + id + "' cannot be changed.");
            }

        };


    }


  protected:

    const std::string base_uri;
    F& factory;

  protected:

    virtual Json::Value to_json(const T&) const = 0;
    virtual T from_json(Json::Value, const std::string&) const = 0;

};

}


#endif
