#ifndef __AUTOMATION_HTTP_FACTORY_RIO_LINK_H__
#define __AUTOMATION_HTTP_FACTORY_RIO_LINK_H__

#include "stdexcept"
#include "rikerio-automation/automation.h"
#include "spdlog/spdlog.h"

#include "factory/cache.h"
#include "models/rio-link.h"

namespace automation::http::factory {

class RioLink : public Cached<model::RioLink> {

  public:

    RioLink(EventEmitter&, std::string, RikerIO::Profile&);

    std::shared_ptr<model::RioLink> add(model::RioLink&);
    std::shared_ptr<model::RioLink> update(const std::string&, model::RioLink&);
    std::shared_ptr<model::RioLink> remove(const std::string&);

  private:

    RikerIO::Profile& rio_profile;
    std::filesystem::file_time_type rio_last_write;

    bool filter(const model::RioLink&, const std::string&) const;
    int compare(const std::string&, const model::RioLink&, const model::RioLink&) const;

    void update_cache() override;
};

}

#endif
