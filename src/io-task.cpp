#include "io-task.h"
#include "spdlog/spdlog.h"
#include "limits"
#include "common.h"

using namespace automation::http;

IoTask::IoTask(
    RikerIO::Profile& rio_profile,
    unsigned int cycle,
    unsigned int read_list_size,
    unsigned int requests_per_cycle) :
    context(),
    cycle(cycle),
    requests_per_cycle(requests_per_cycle),
    measure(cycle),
    memory_ptr(rio_profile.ptr),
    to_rt_command_list(read_list_size),
    from_rt_command_list(read_list_size) {

    /* memory profile */
    /* server version major (2), minor (2), patch (2) -> 6 */
    /* rx_list_capacity (8), rx_list_size (8), rx_job_count (8) -> 24 */
    /* tx_list_capacity (8), tx_list_size (8), tx_job_count (8) -> 24 */
    /* cycle last (4), cycle avg (4), cycle max (4), cycle_base (4) -> 16 */
    /* 70 bytes */


    auto& provider = context.add_provider<automation::rikerio::Provider>(
                         rio_profile.root_path, "rikerio-http", "http.", "http.");

    const auto f_write = automation::rikerio::Provider::F_Write;

    provider.create_data<uint16_t>("version.major", true)
    .create_data<uint16_t>("version.minor", true)
    .create_data<uint16_t>("version.patch", true)
    .create_data<uint64_t>("rx.list.cap", true)
    .create_data<uint64_t>("rx.list.size", true)
    .create_data<uint64_t>("rx.jobs.count", true)
    .create_data<uint64_t>("tx.list.cap", true)
    .create_data<uint64_t>("tx.list.size.last", true)
    .create_data<uint64_t>("tx.list.size.max", true)
    .create_data<uint64_t>("tx.list.size.avg", true)
    .create_data<uint64_t>("tx.jobs.count", true)
    .create_data<uint32_t>("cycle.last", true)
    .create_data<uint32_t>("cycle.avg", true)
    .create_data<uint32_t>("cycle.max", true)
    .create_data<uint32_t>("cycle.base", true)
    .create<uint16_t>("version.major", io.version.major, f_write)
    .create<uint16_t>("version.minor", io.version.minor, f_write)
    .create<uint16_t>("version.patch", io.version.patch, f_write)
    .create<uint64_t>("rx.list.cap", io.rx.list.cap, f_write)
    .create<uint64_t>("rx.list.size", io.rx.list.size, f_write)
    .create<uint64_t>("rx.jobs.count", io.rx.jobs.count, f_write)
    .create<uint64_t>("tx.list.cap", io.tx.list.cap, f_write)
    .create<uint64_t>("tx.list.size.last", io.tx.list.size.last, f_write)
    .create<uint64_t>("tx.list.size.max", io.tx.list.size.max, f_write)
    .create<uint64_t>("tx.list.size.avg", io.tx.list.size.avg, f_write)
    .create<uint64_t>("tx.jobs.count", io.tx.jobs.count, f_write)
    .create<uint32_t>("cycle.last", io.cycle.last, f_write)
    .create<uint32_t>("cycle.avg", io.cycle.avg, f_write)
    .create<uint32_t>("cycle.max", io.cycle.max, f_write)
    .create<uint32_t>("cycle.base", io.cycle.base, f_write);


}

IoTask::~IoTask() {

}

void IoTask::init() {
    context.init();
}
void IoTask::quit() {
    context.quit();
}

void IoTask::loop() {

    context.loop([&]() {

        measure.start();
        lock_handler();

        io.rx.list.cap = to_rt_command_list.get_capacity();
        io.rx.list.size = to_rt_command_list.get_size();
        io.rx.jobs.count = 0;

        /* read read register */

        unsigned int cntr = 0;
        while (!to_rt_command_list.is_empty()) {

            // max jobs in one cycle
            if (cntr++ == requests_per_cycle) {
                break;
            }

            Request req = to_rt_command_list.pop();

            if (req.cmd == RequestCommand::ADD_READ_JOB) {

                //spdlog::debug("IoTask::loop : added new read job with id {}", req.read_node->item->get_id());
                req.job.read->read();

                //spdlog::debug("IOTask::loop() : ReadJob updated for id {}.", job->get_id());

                Response res = {
                    ResponseType::READ_JOB_DONE, { .read = req.job.read }
                };

                from_rt_command_list.push(res);

            } else if (req.cmd == RequestCommand::ADD_WRITE_JOB) {

                req.job.write->write();

                Response res = { ResponseType::WRITE_JOB_DONE, { .write = req.job.write } };

                from_rt_command_list.push(res);

            }

        }

        measure.stop();

        double avg = 0;
        if (tx_list_data_counter == std::numeric_limits<uint32_t>::max()) {
            tx_list_size_sum = 0;
            tx_list_data_counter = 0;
        } else {
            tx_list_size_sum += from_rt_command_list.get_size();
            tx_list_data_counter += 1;
            avg = tx_list_size_sum / (double) tx_list_data_counter;
        }

        io.tx.list.cap = from_rt_command_list.get_capacity();
        io.tx.list.size.last = from_rt_command_list.get_size();
        io.tx.list.size.max = std::max(io.tx.list.size.last.value, io.tx.list.size.max.value);
        io.tx.list.size.avg = (uint64_t) avg;
        io.tx.jobs.count = 0;

        io.cycle.last = measure.get_last();
        io.cycle.max = measure.get_max();
        io.cycle.avg = measure.get_average();
        io.cycle.base = cycle;

        io.version.major = automation::http::version.get_major();
        io.version.minor = automation::http::version.get_minor();
        io.version.patch = automation::http::version.get_patch();

        unlock_handler();

    });

}

void IoTask::add_read_data(unsigned int byte_offset, unsigned int byte_size, std::function<void(char*)> callback) {

    /* create request and transport it into the Realtime-Thread */

    ReadJob* job = new ReadJob((char*) memory_ptr + byte_offset, byte_size, callback);

    //ReadList::Node* node = new ReadList::Node { job, nullptr };

    Request req = { RequestCommand::ADD_READ_JOB, { .read = job } };

    try {
        to_rt_command_list.push(req);
    } catch (automation::utils::BufferOverflow& e) {
        spdlog::error("Error adding ReadJob to IoTask, Buffer capacity zero.");
        delete job;
    }

    /* from now on the job is not to be touched unless there is a response on
     * the from_rt_command_list */

}

void IoTask::add_write_data(unsigned int byte_offset, unsigned int byte_size, char* ptr, std::function<void()> callback) {

    WriteJob* job = new WriteJob(
        (char*) memory_ptr + byte_offset,
        byte_size * 8,
        0,
        callback);

    job->init(ptr, byte_size);

    Request req = { RequestCommand::ADD_WRITE_JOB, { .write = job } };

    try {
        to_rt_command_list.push(req);
    } catch (automation::utils::BufferOverflow& e) {
        spdlog::error("Error adding WriteJob to IoTask, Buffer capacity zero.");
        delete job;
    }

}

void IoTask::set_lock_handler(std::function<void()> handler) {
    lock_handler = handler;
}

void IoTask::set_unlock_handler(std::function<void()> handler) {
    unlock_handler = handler;
}

void IoTask::receive() {

    // Loop through read queue and update updateables

    while (!from_rt_command_list.is_empty()) {

        Response res = from_rt_command_list.pop();

        if (res.type == ResponseType::READ_JOB_DONE) {

            res.job.read->resolve();

            delete res.job.read;

        } else if (res.type == ResponseType::WRITE_JOB_DONE) {

            res.job.write->resolve();

            delete res.job.write;

        }

    }

}
