#ifndef __AUTOMATION_HTTP_SERVICE_GROUP_H__
#define __AUTOMATION_HTTP_SERVICE_GROUP_H__

#include "xapp.h"
#include "service/service.h"
#include "factory/group.h"

using namespace automation::http;

namespace automation::http::service {

class Group : public FactoryService<factory::Group, model::Group> {
  public:

    Group(const std::string& base_uri, factory::Group&);

    operator automation::http::ExtendedApp::Handler () {
        return [&](auto, auto req) {

            auto user = req->get_user();

            if (!user) {
                spdlog::debug("{}:{} : No user found in request.", req->get_method(), req->get_url());
                req->set_group(automation::http::model::Group::Noone);
                return;
            }

            try {

                auto group = factory.get_one(user->get_group());

                spdlog::debug("{}:{} : Setting group in request.", req->get_method(), req->get_url());

                req->set_group(group);

            } catch (...) {
                spdlog::error("{} {} : Missing group for user {}.", req->get_method(), req->get_url(), user->get_id());
                req->set_group(automation::http::model::Group::Noone);
            }
        };
    }

  protected:

    Json::Value to_json(const model::Group&) const;
    model::Group from_json(Json::Value, const std::string&) const;


};

}

#endif
