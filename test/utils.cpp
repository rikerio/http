#include "gtest/gtest.h" // googletest header file

#include "utils.h"

using namespace automation;

TEST(Utils, split_string_view) {

    const std::string str = "aa;bbb;ccc;dddd";

    std::vector<std::string_view> result_vector;

    http::Utils::Split(str, ';', result_vector);

    ASSERT_EQ(result_vector.size(), 4);
    ASSERT_TRUE(result_vector[0] == "aa");
    ASSERT_TRUE(result_vector[1] == "bbb");
    ASSERT_TRUE(result_vector[2] == "ccc");
    ASSERT_TRUE(result_vector[3] == "dddd");

}

TEST(Utils, split_string_view_2) {

    const std::string str = "aa;bbb;cccccccc;dddd;";

    std::vector<std::string_view> result_vector;

    http::Utils::Split(str, ';', result_vector);

    ASSERT_EQ(result_vector.size(), 4);
    ASSERT_TRUE(result_vector[0] == "aa");
    ASSERT_TRUE(result_vector[1] == "bbb");
    ASSERT_TRUE(result_vector[2] == "cccccccc");
    ASSERT_TRUE(result_vector[3] == "dddd");

}
