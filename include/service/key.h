#ifndef __AUTOMATION_HTTP_SERVICE_KEY_H__
#define __AUTOMATION_HTTP_SERVICE_KEY_H__

#include "service/service.h"
#include "factory/key.h"
#include "xapp.h"

namespace automation::http::service {

class Key : public FactoryService<factory::Key, model::Key> {

  public:

    Key(const std::string& base_uri, factory::Key& factory);

  private:

    Json::Value to_json(const model::Key& key) const;
    model::Key from_json(Json::Value value, const std::string&) const;


};

}


#endif
