#include "service/rio-link.h"

using namespace automation::http;

service::RioLink::RioLink(const std::string& base_uri, factory::RioLink& factory) :
    FactoryService(base_uri, factory) { };

Json::Value service::RioLink::to_json(const model::RioLink& a) const {

    Json::Value result;

    result["id"] = a.get_id();
    Json::Value json_keys = Json::arrayValue;

    for (auto& k : a.get_keys()) {
        json_keys.append(k);
    }

    result["keys"] = json_keys;

    return result;

}

model::RioLink service::RioLink::from_json(Json::Value value, const std::string&) const {

    Json::Value json_id = value["id"];

    if (!json_id || !json_id.isString()) {
        spdlog::debug("No id present");
        throw ExtendedApp::bad_request_error("Missing id in request body.");
    }

    Json::Value json_keys = value["keys"];

    if (!json_keys || !json_keys.isArray()) {
        spdlog::debug("Missing keys.");
        throw ExtendedApp::bad_request_error("Missing keys in request body.");
    }

    std::vector<std::string> tmp_keys;

    for (auto& k : json_keys) {
        if (!k || !k.isString()) {
            spdlog::debug("Wrong key in keys");
            throw ExtendedApp::bad_request_error("Wrong key in key list in request body.");
        }
        tmp_keys.push_back(k.as<std::string>());
    }

    std::string id = json_id.as<std::string>();

    model::RioLink link(id, tmp_keys);

    return link;

}
