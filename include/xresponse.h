#ifndef __DEPOT_AUTOMATION_EXTENDED_HTTP_RESPONSE_H__
#define __DEPOT_AUTOMATION_EXTENDED_HTTP_RESPONSE_H__

#include "spdlog/spdlog.h"
#include "uWebSockets/App.h"
#include "json/json.h"

namespace automation::http {

class ExtendedRequest;
class ResponseHandler;

class ExtendedResponse {

public:
  ExtendedResponse(uWS::HttpResponse<false> *uws_response,
                   const std::set<std::string> tags);

  ExtendedResponse &set_request(std::shared_ptr<ExtendedRequest>);
  ExtendedResponse &set_status(const std::string &value);
  ExtendedResponse &set_header(const std::string key, const std::string value);
  ExtendedResponse &set_data(const std::string &value);
  ExtendedResponse &set_data(std::shared_ptr<ResponseHandler>);
  ExtendedResponse &wait();
  ExtendedResponse &resume();

  std::string &get_data();

  void apply();
  bool has_responded() const;
  bool has_tag(const std::string &tag);
  bool is_waiting() const;
  std::shared_ptr<ResponseHandler> get_response_handler();

private:
  uWS::HttpResponse<false> *uws_response;

private:
  std::shared_ptr<ExtendedRequest> request;
  std::string status;
  std::map<std::string, std::string> header_map;
  std::string data;
  std::shared_ptr<ResponseHandler> response_handler = nullptr;

  std::set<std::string> tags;

  bool waiting = false;
};

} // namespace automation::http

#endif
