#ifndef __AUTOMATION_HTTP_SERVICE_FILE_H__
#define __AUTOMATION_HTTP_SERVICE_FILE_H__

#include "models/file.h"
#include "factory/file.h"
#include "service/service.h"
#include "parser/multipart-form.h"
#include "iostream"

namespace automation::http::service {

class File : public FactoryService<factory::File, model::File> {

  public:

    File(const std::string& base_uri, factory::File&, const std::string&);
    ~File();

    automation::http::ExtendedApp::Handler create() override;
    automation::http::ExtendedApp::Handler remove() override;
    automation::http::ExtendedApp::Handler update() override;

    automation::http::ExtendedApp::Handler download();

  protected:

    model::File from_json(Json::Value, const std::string&) const;

    Json::Value to_json(const model::File&) const;

  private:

    const std::string file_folder;

};

}

#endif
