#include "factory/key.h"

using namespace automation::http;

factory::Key::Key(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::Key>(emitter, filename, "key") {

    deserialize();

    std::function<void(const std::string&)> user_removed_handler = [&](const std::string& id) {

        std::set<std::string> remove_keys;
        for (auto it : t_map) {
            if (it.second->get_user_id() == id) {
                remove_keys.insert(it.first);
            }
        }

        for (auto& k : remove_keys) {
            t_map.erase(k);
        }

    };

    emitter.on<const std::string&>("user.removed", user_removed_handler);

}

factory::Key::~Key() {
    serialize();
}

YAML::Node factory::Key::to_yaml(const model::Key& rhs) const {
    YAML::Node key_node;

    key_node["id"] = rhs.get_id();
    key_node["userid"] = rhs.get_user_id();
    key_node["disabled"] = rhs.is_disabled();

    return key_node;
}

model::Key factory::Key::from_yaml(YAML::Node node) const {

    model::Key key;

    auto yaml_id = node["id"];
    auto yaml_user_id = node["userid"];
    auto yaml_disabled = node["disabled"];

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        key.set_id(yaml_id.as<std::string>());
    }

    if (yaml_user_id && yaml_user_id.Type() == YAML::NodeType::Scalar) {
        key.set_user_id(yaml_user_id.as<std::string>());
    }

    if (yaml_disabled && yaml_disabled.Type() == YAML::NodeType::Scalar) {
        key.set_disabled(yaml_disabled.as<bool>());
    }

    return key;
}

bool factory::Key::filter(const model::Key& key, const std::string& value) const {

    if (value == "") {
        return true;
    }

    if (std::string::npos != key.get_id().find(value)) {
        return true;
    }

    if (std::string::npos != key.get_user_id().find(value)) {
        return true;
    }

    return false;

}

int factory::Key::compare(const std::string& field, const model::Key& a, const model::Key& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "user") {
        return a.get_user_id().compare(b.get_user_id());
    }

    return 0;

}
