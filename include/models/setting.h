#ifndef __AUTOMATION_HTTP_MODEL_SETTING_H__
#define __AUTOMATION_HTTP_MODEL_SETTING_H__

#include "string"
#include "utils.h"

namespace automation::http::model {

class Setting {

  public:

    Setting() :
        id(""),
        value(""),
        type("")
    { }

    const std::string& get_id() const {
        return id;
    }

    Setting& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    const std::string& get_value() const {
        return value;
    }

    Setting& set_value(const std::string& v) {
        value = v;
        return *this;
    }

    const std::string& get_type() const {
        return type;
    }

    Setting& set_type(const std::string& value) {
        type = Utils::to_lower(value);
        return *this;
    }

  private:

    std::string id;
    std::string value;
    std::string type;

};

}

#endif
