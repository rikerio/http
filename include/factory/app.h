#ifndef __AUTOMATION_HTTP_FACTORY_APP_H__
#define __AUTOMATION_HTTP_FACTORY_APP_H__

#include "string"
#include "factory/yaml-file.h"
#include "models/app.h"

namespace automation::http::factory {

class App : public YamlFile<model::App> {

  public:

    App(EventEmitter&, const std::string&);
    ~App();

  private:

    YAML::Node to_yaml(const model::App&) const;
    model::App from_yaml(YAML::Node) const;
    bool filter(const model::App&, const std::string&) const;
    int compare(const std::string& field, const model::App&, const model::App&) const;


};

}

#endif
