#include "models/session.h"

using namespace automation::http::model;

Session::Session() :
    id(""),
    path(""),
    user_agent(""),
    expires() { }

const std::string Session::get_expires_utc_string() const {

    auto itt = std::chrono::system_clock::to_time_t(expires);
    std::ostringstream ss;
    ss << std::put_time(gmtime(&itt), "%a,%d-%b-%Y %H:%M:00 GMT");
    return ss.str();

}

bool Session::has_expired() const {
    return std::chrono::system_clock::now() > expires;
}

void Session::set_value(const std::string& key, const std::string& value) {
    key_value_map[key] = value;
}

void Session::set_id(const std::string& value) {
    id = value;
}

void Session::set_expires(std::chrono::system_clock::time_point& value) {
    expires = value;
}

void Session::set_max_age(unsigned int max_age) {
    expires = std::chrono::system_clock::now() + std::chrono::seconds(max_age);
}

void Session::set_path(const std::string& value) {
    path = value;
}

const std::string& Session::get_value(const std::string& key) const {
    static std::string empty_value = "";
    auto it = key_value_map.find(key);
    if (it == key_value_map.end()) {
        return empty_value;
    }
    return it->second;
}

const std::string& Session::get_id() const {
    return id;
}

const std::chrono::system_clock::time_point& Session::get_expires() const {
    return expires;
}

const std::string& Session::get_path() const {
    return path;
}

const std::string& Session::get_user_id() const {

    auto it = key_value_map.find("user_id");

    if (it == key_value_map.end()) {
        throw no_such_key_error();
    }

    return it->second;

}

void Session::set_user_id(const std::string& value) {
    if (value == "") {
        key_value_map.erase("user_id");
        return;
    }
    key_value_map["user_id"] = value;
}

void Session::set_user_agent(const std::string& value) {
    user_agent = value;
}

const std::string& Session::get_user_agent() const {
    return user_agent;
}

const std::map<std::string, std::string> Session::get_key_value_map() const {
    return key_value_map;
}
