#ifndef __AUTOMATION_HTTP_COOKIE_MIDDLEWARE_H__
#define __AUTOMATION_HTTP_COOKIE_MIDDLEWARE_H__


#include "xapp.h"
#include "spdlog/spdlog.h"
#include "factory/session.h"

namespace automation::http::middleware {

class Cookies {

  public:
    Cookies(factory::Session& session_factory);
    ~Cookies();

    operator automation::http::ExtendedApp::Handler();

  private:

    factory::Session& session_factory;

};

}


#endif
