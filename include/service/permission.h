#ifndef __AUTOMATION_HTTP_SERVICE_PERMISSION_H__
#define __AUTOMATION_HTTP_SERVICE_PERMISSION_H__

#include "service/service.h"
#include "factory/permission.h"
#include "factory/user.h"
#include "xapp.h"

namespace automation::http::service {

class Permission : public FactoryService<factory::Permission, model::Permission> {

  public:

    Permission(const std::string& base_uri, factory::Permission& factory, factory::User& user_factory);

    operator automation::http::ExtendedApp::Handler () {
        return [&](auto, auto req) {

            auto user = req->get_user();

            if (!user) {
                spdlog::debug("{}:{} : No user found in request.", req->get_method(), req->get_url());
                req->set_user(automation::http::model::User::Noone);
                return;
            }

            try {

                auto permission_list = factory.get_all_by_user(user->get_id());
                for (auto p : permission_list) {
                    req->add_permission(p);
                }

                spdlog::debug("{}:{} : Copying User Permissions in request.", req->get_method(), req->get_url());


            } catch (...) {
                spdlog::error("{} {} : Missing permissions for user {}.", req->get_method(), req->get_url(), user->get_id());
            }
        };
    }

  private:

    factory::User& user_factory;


    Json::Value to_json(const model::Permission& key) const;
    model::Permission from_json(Json::Value value, const std::string&) const;


};

}


#endif
