#include "service/session.h"

#include "xapp.h"

using namespace automation::http;

service::Session::Session(const std::string& base_uri, factory::Session& factory) :
    FactoryService<factory::Session, model::Session>(base_uri, factory) { }

model::Session service::Session::from_json(Json::Value body, const std::string&) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    model::Session session;

    auto json_store = body["store"];
    auto user_agent = body["user_agent"];

    if (user_agent && user_agent.isString()) {
        session.set_user_agent(user_agent.asString());
    }

    if (!json_store) {
        return session;
    }

    if (!json_store.isObject()) {
        throw ExtendedApp::bad_request_error("Session store musst be a map.");
    }

    for (auto key : json_store.getMemberNames()) {
        auto json_value = json_store[key];
        if (!json_value.isString()) {
            throw ExtendedApp::bad_request_error("Session store can only contain strings.");
        }
        session.set_value(key, json_value.asString());
    }

    return session;
}

Json::Value service::Session::to_json(const model::Session& session) const {

    Json::Value result;

    auto expires_sec = std::chrono::time_point_cast<std::chrono::seconds>(session.get_expires());
    auto epoch = expires_sec.time_since_epoch();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(epoch);

    result["id"] = session.get_id();
    result["path"] = session.get_path();
    result["expires"] = duration.count();
    result["user_agent"] = session.get_user_agent();

    Json::Value json_store = Json::objectValue;

    for (auto v : session.get_key_value_map()) {
        json_store[v.first] = v.second;
    }

    result["store"] = json_store;

    return result;

};
