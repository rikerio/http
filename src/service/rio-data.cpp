#include "service/rio-data.h"

using namespace automation::http;

service::RioData::RioData(const std::string& base_uri, factory::RioData& factory) :
    FactoryService(base_uri, factory) { };

Json::Value service::RioData::to_json(const model::RioData& a) const {

    Json::Value result;

    result["id"] = a.get_id();
    result["type"] = a.get_type();
    result["byte_offset"] = a.get_byte_offset();
    result["bit_index"] = a.get_bit_index();
    result["bit_size"] = a.get_bit_size();

    return result;

}

model::RioData service::RioData::from_json(Json::Value, const std::string&) const {
    throw ExtendedApp::not_implemented_error();
}
