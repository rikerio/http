#ifndef __AUTOMATION_HTTP_MIDDLEWARE_SESSION_H__
#define __AUTOMATION_HTTP_MIDDLEWARE_SESSION_H__

#include "string"
#include "map"
#include "chrono"
#include "iomanip"
#include "models/model.h"

namespace automation::http::model {

class Session : public Base {

  public:

    class no_such_key_error : public std::exception { };

  public:

    /* create new session */
    Session();

  public:

    const std::string get_expires_utc_string() const;
    bool has_expired() const;

    void set_id(const std::string& id);
    void set_expires(std::chrono::system_clock::time_point&);
    void set_max_age(unsigned int);
    void set_path(const std::string&);
    void set_value(const std::string& key, const std::string& value);

    const std::string& get_id() const;
    const std::chrono::system_clock::time_point& get_expires() const;
    const std::string& get_path() const;
    const std::string& get_value(const std::string& key) const;

    // wrapper for key value store
    void set_user_id(const std::string& value = "");
    const std::string& get_user_id() const;

    void set_user_agent(const std::string&);
    const std::string& get_user_agent() const;

    const std::map<std::string, std::string> get_key_value_map() const;

  private:

    std::string id;
    std::string path;
    std::string user_agent;
    std::chrono::system_clock::time_point expires;

    std::map<std::string, std::string> key_value_map;

};

}

#endif
