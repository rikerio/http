#include "factory/app.h"

using namespace automation::http;

factory::App::App(EventEmitter& emitter, const std::string& filename) :
    factory::YamlFile<model::App>(emitter, filename, "app") {
    deserialize();
}

factory::App::~App() {
    serialize();
}

YAML::Node factory::App::to_yaml(const model::App& app) const {

    YAML::Node node;

    node["id"] = app.get_id();
    node["name"] = app.get_name();
    node["description"] = app.get_description();
    node["version"] = app.get_version();
    node["author"]["name"] = app.get_author_name();
    node["author"]["email"] = app.get_author_email();
    node["disabled"] = app.is_disabled();

    return node;

}

model::App factory::App::from_yaml(YAML::Node node) const {

    model::App app;

    auto yaml_id = node["id"];

    if (yaml_id && yaml_id.Type() == YAML::NodeType::Scalar) {
        app.set_id(yaml_id.as<std::string>());
    }

    auto yaml_name = node["name"];

    if (yaml_name && yaml_name.Type() == YAML::NodeType::Scalar) {
        app.set_name(yaml_name.as<std::string>());
    }

    auto yaml_description = node["description"];

    if (yaml_description && yaml_description.Type() == YAML::NodeType::Scalar) {
        app.set_description(yaml_description.as<std::string>());
    }

    auto yaml_version = node["version"];

    if (yaml_version && yaml_version.Type() == YAML::NodeType::Scalar) {
        app.set_version(yaml_version.as<std::string>());
    }

    auto yaml_author = node["author"];

    if (yaml_author) {

        auto yaml_author_name = yaml_author["name"];
        auto yaml_author_email = yaml_author["email"];

        if (yaml_author_name && yaml_author_name.Type() == YAML::NodeType::Scalar) {
            app.set_author_name(yaml_author_name.as<std::string>());
        }

        if (yaml_author_email && yaml_author_email.Type() == YAML::NodeType::Scalar) {
            app.set_author_email(yaml_author_email.as<std::string>());
        }

    }

    auto yaml_disabled = node["disabled"];

    if (yaml_disabled && yaml_disabled.Type() == YAML::NodeType::Scalar) {
        app.set_disabled(yaml_disabled.as<bool>());
    }

    return app;

}

bool factory::App::filter(const model::App& app, const std::string& str) const {

    if (str == "") {
        return true;
    }

    if (std::string::npos != app.get_id().find(str)) {
        return true;
    }

    if (std::string::npos != app.get_name().find(str)) {
        return true;
    }

    if (std::string::npos != app.get_description().find(str)) {
        return true;
    }

    return false;

}

int factory::App::compare(const std::string& field, const model::App& a, const model::App& b) const {

    if (field == "id" || field == "") {
        return a.get_id().compare(b.get_id());
    }

    if (field == "name") {
        return a.get_name().compare(b.get_name());
    }

    if (field == "disabled") {
        return a.is_disabled() - b.is_disabled();
    }

    return 0;

}
