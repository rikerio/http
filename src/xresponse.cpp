#include "xresponse.h"
#include "response-data-handler.h"

using namespace automation::http;

ExtendedResponse::ExtendedResponse(uWS::HttpResponse<false>* uws_response, const std::set<std::string> tags) :
    uws_response(uws_response),
    tags(tags) {


};

ExtendedResponse& ExtendedResponse::set_request(std::shared_ptr<ExtendedRequest> value) {
    request = value;
    return *this;
}

ExtendedResponse& ExtendedResponse::set_status(const std::string& value) {
    status = value;
    return *this;
}

ExtendedResponse& ExtendedResponse::set_header(const std::string key, const std::string value) {
    header_map.insert(std::pair(key, value));
    return *this;
}

ExtendedResponse& ExtendedResponse::set_data(const std::string& value) {
    data = value;
    return *this;
};

ExtendedResponse& ExtendedResponse::set_data(const std::shared_ptr<ResponseHandler> value) {
    response_handler = value;
    return *this;
};

std::string& ExtendedResponse::get_data() {
    return data;
}


ExtendedResponse& ExtendedResponse::wait() {
    waiting = true;
    return *this;
}

ExtendedResponse& ExtendedResponse::resume() {
    waiting = false;
    return *this;
}

void ExtendedResponse::apply() {

    spdlog::debug("Applying request status: {}.", status);

    uws_response->writeStatus(status);

    spdlog::debug("sending data.");

    if (response_handler != nullptr) {
        spdlog::debug("setting response body");
        response_handler->set_response_body(*request, *this);
    }

    std::for_each(header_map.begin(), header_map.end(), [&](auto pair) {
        spdlog::debug("Setting header {} : {}", pair.first, pair.second);
        uws_response->writeHeader(pair.first, pair.second);
    });

    uws_response->end(data);

}

bool ExtendedResponse::has_responded() const {
    return status != "";
}

bool ExtendedResponse::has_tag(const std::string& tag) {
    return tags.find(tag) != tags.end();
}

bool ExtendedResponse::is_waiting() const {
    return waiting;
}

std::shared_ptr<ResponseHandler> ExtendedResponse::get_response_handler() {
    return response_handler;
}
