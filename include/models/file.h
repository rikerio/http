#ifndef __AUTOMATION_HTTP_MODEL_FILE_H__
#define __AUTOMATION_HTTP_MODEL_FILE_H__

#include "string"
#include "utils.h"

namespace automation::http::model {

class File {

  public:

    File() :
        id(""),
        name(""),
        type(""),
        size(0) {
    }

    const std::string& get_id() const {
        return id;
    }

    File& set_id(const std::string& value) {
        id = Utils::to_lower(value);
        return *this;
    }

    File& set_id(std::string_view& view) {
        id = std::string(view);
        return *this;
    }

    const std::string& get_name() const {
        return name;
    }

    File& set_name(const std::string& value) {
        name = value;
        return *this;
    }

    File& set_name(std::string_view view) {
        name = std::string(view);
        return *this;
    }

    const std::string& get_type() const {
        return type;
    }

    File& set_type(const std::string& value) {
        type = value;
        return *this;
    }

    File& set_type(std::string_view view) {
        type = std::string(view);
        return *this;
    }

    size_t get_size() const {
        return size;
    }

    File& set_size(size_t value) {
        size = value;
        return *this;
    }

  private:

    std::string id;
    std::string name;
    std::string type;
    size_t size;

};

}


#endif
