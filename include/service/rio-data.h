#ifndef __AUTOMATION_HTTP_SERVICE_RIO_DATA_H__
#define __AUTOMATION_HTTP_SERVICE_RIO_DATA_H__

#include "factory/rio-data.h"
#include "service/service.h"

namespace automation::http::service {

class RioData : public FactoryService<factory::RioData, model::RioData> {

  public:

    RioData(const std::string& base_uri, factory::RioData&);

  private:

    Json::Value to_json(const model::RioData&) const;
    model::RioData from_json(Json::Value, const std::string&) const;

};

}

#endif
