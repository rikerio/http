#ifndef _AUTOMATION_HTTP_EVENT_EMITTER_H_
#define _AUTOMATION_HTTP_EVENT_EMITTER_H_

#include <functional>
#include <map>
#include <memory>
#include <list>
#include <algorithm>
#include <stdio.h>

using namespace std;

class EventEmitter {
  public:

    EventEmitter() { };

    ~EventEmitter() { }

    template <typename... Args>
    unsigned int add_listener(std::string event_id, std::function<void (Args...)> cb);

    unsigned int add_listener(std::string event_id, std::function<void ()> cb);

    template <typename... Args>
    unsigned int on(std::string event_id, std::function<void (Args...)> cb);

    unsigned int on(const std::string event_id, std::function<void ()> cb);

    void remove_listener(unsigned int listener_id);

    template <typename... Args>
    void emit(std::string event_id, Args... args);

  private:
    struct ListenerBase {
        ListenerBase() {}

        ListenerBase(unsigned int i)
            : id(i) {}

        virtual ~ListenerBase() {}

        unsigned int id;
    };

    template <typename... Args>
    struct Listener : public ListenerBase {
        Listener() {}

        Listener(unsigned int i, std::function<void (Args...)> c)
            : ListenerBase(i), cb(c) {}

        std::function<void (Args...)> cb;
    };

    unsigned int last_listener;
    multimap<std::string, shared_ptr<ListenerBase>> listeners;

    EventEmitter(const EventEmitter&) = delete;
    const EventEmitter& operator = (const EventEmitter&) = delete;
};

template <typename... Args>
unsigned int EventEmitter::add_listener(std::string event_id, std::function<void (Args...)> cb) {
    if (!cb) {
        throw std::invalid_argument("AdvEventEmitter::add_listener: No callback provided.");
    }

    unsigned int listener_id = ++last_listener;
    listeners.insert(std::make_pair(event_id, std::make_shared<Listener<Args...>>(listener_id, cb)));

    return listener_id;
}

template <typename... Args>
unsigned int EventEmitter::on(std::string event_id, std::function<void (Args...)> cb) {
    return add_listener(event_id, cb);
}

template <typename... Args>
void EventEmitter::emit(std::string event_id, Args... args) {
    list<std::shared_ptr<Listener<Args...>>> handlers;

    {

        auto range = listeners.equal_range(event_id);
        handlers.resize(std::distance(range.first, range.second));
        transform(range.first, range.second, handlers.begin(), [] (pair<std::string, shared_ptr<ListenerBase>> p) {
            auto l = dynamic_pointer_cast<Listener<Args...>>(p.second);
            if (l) {
                return l;
            } else {
                string errStr = "EventEmitter::emit: " + p.first + " Invalid event signature.";

                fprintf(stderr, "%s\n", errStr.c_str());

                throw logic_error("EventEmitter::emit: Invalid event signature.");
            }
        });
    }

    for (auto& h : handlers) {
        h->cb(args...);
    }
}


#endif
