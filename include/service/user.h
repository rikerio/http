#ifndef __AUTOMATION_HTTP_SERVICE_USERSERVICE_H__
#define __AUTOMATION_HTTP_SERVICE_USERSERVICE_H__

#include "service/service.h"
#include "factory/user.h"
#include "xapp.h"
#include "constants.h"


namespace automation::http::service {

class User : public FactoryService<factory::User, model::User> {

  public:

    User(const std::string&, factory::User& user_factory);

    /* add user to request if session given and session holds a user id */
    operator automation::http::ExtendedApp::Handler() {

        return [&](auto, auto req) {

        };
    }

  protected:

    Json::Value to_json(const model::User&) const;
    model::User from_json(Json::Value, const std::string&) const;

};

}

#endif
