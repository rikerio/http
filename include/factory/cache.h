#ifndef __AUTOMATION_HTTP_CACHED_MODEL_FACTORY_H__
#define __AUTOMATION_HTTP_CACHED_MODEL_FACTORY_H__

#include "string"
#include "set"
#include "fstream"

#include "event-emitter.h"
#include "spdlog/spdlog.h"
#include "yaml-cpp/yaml.h"
#include "factory/base.h"

namespace automation::http::factory {

template <typename T>
class Cached {

  public:

    Cached(EventEmitter& emitter, std::string& event_prefix) :
        event_prefix(event_prefix),
        emitter(emitter) {
        update_cache();
    };

    virtual ~Cached() {}

    std::shared_ptr<T> add(T&) {
        throw std::runtime_error("Not implemented");
        return nullptr;
    }

    std::shared_ptr<T> update(const std::string&, T&) {
        throw std::runtime_error("Not implemented");
        return nullptr;
    }

    std::shared_ptr<T> remove(const std::string&) {
        throw std::runtime_error("Not implemented");
        return nullptr;
    }

    std::shared_ptr<T> get_one(const std::string&) {
        throw std::runtime_error("Not implemented");
        return nullptr;
    }

    std::vector<std::shared_ptr<T>>& query(const std::string& filter_str, const std::string& sort, bool descending) {

        spdlog::debug("Cached::query : Executed with filter={}, sort={}, descending={}.", filter_str, sort, descending);

        update_cache();

        const std::string query_str = filter_str + "/" + sort + "/" + (descending ? "desc" : "asc");

        spdlog::debug("Cached::query : Looking for query {}.", query_str);

        auto it = query_cache_map.find(query_str);

        if (it != query_cache_map.end()) {
            spdlog::debug("Cached::query : Cached query found, serving.");
            return it->second;
        }

        std::vector<std::shared_ptr<T>> current_query_list;
        for (auto fit : t_map) {

            std::shared_ptr<T> value = fit.second;

            if (filter(*value, filter_str)) {
                spdlog::debug("Cached::query : Filter item with id {}.", value->get_id());
                current_query_list.push_back(value);
            }

        }

        spdlog::debug("Cached::query : Filter done with {} items left.", current_query_list.size());

        std::sort(current_query_list.begin(), current_query_list.end(), [&](auto& a, auto& b) {
            return compare(sort, *a, *b) < 0;
        });

        spdlog::debug("Cached::query : Sorting done with {} items left.", current_query_list.size());

        if (descending) {
            std::reverse(current_query_list.begin(), current_query_list.end());
        }

        spdlog::debug("Cached::query : Reverse done with {} items left.", current_query_list.size());

        query_cache_map[query_str] = current_query_list;

        return query_cache_map[query_str];

    }

  private:

    const std::string event_prefix;

  protected:

    EventEmitter& emitter;
    std::map<std::string, std::shared_ptr<T>> t_map;
    std::map<std::string, std::vector<std::shared_ptr<T>>> query_cache_map;

  private:

    virtual bool filter(const T&, const std::string&) const = 0;
    virtual int compare(const std::string& field, const T&, const T&) const = 0;

    virtual void update_cache() { }

};

}

#endif
