#include "service/file.h"
#include "experimental/filesystem"
#include "fstream"

using namespace automation::http::service;

File::File(const std::string& base_uri, factory::File& factory, const std::string& file_folder) :
    FactoryService<factory::File, model::File>(base_uri, factory),
    file_folder(file_folder) { }

File::~File() { }

automation::http::ExtendedApp::Handler File::create () {

    return [&](auto res, auto req) {

        automation::http::parser::MultipartFormData mfd(req->get_header("content-type"), req->get_body());

        model::File file;

        for (auto part_data : mfd.get_data()) {

            auto cd = part_data.get_content_disposition();

            if (cd.get_header("name") == "file") {
                spdlog::debug("filename {}, size: {}, type: {}", cd.get_header("filename"), part_data.get_data().size(), part_data.get_header("content-type"));
                file.set_name(cd.get_header("filename"));
                file.set_size(part_data.get_data().size());
                file.set_type(part_data.get_header("content-type"));
            }

        }

        std::shared_ptr<model::File> shared_file;

        try {
            shared_file = factory.add(file);
            std::shared_ptr<json::SingleResponse<model::File>> response_handler =
            std::make_shared<json::SingleResponse<model::File>>(base_uri, [&](model::File& obj) -> Json::Value {
                return to_json(obj);
            });
            response_handler->set_data(shared_file);

            res->set_data(response_handler);


        } catch (typename automation::http::factory::duplicate_error& e) {
            spdlog::debug("Error adding resource, duplicate id.");
            throw ExtendedApp::bad_request_error("Duplicate id.");
        }

        for (auto part_data : mfd.get_data()) {

            auto cd = part_data.get_content_disposition();

            if (cd.get_header("name") == "file") {
                std::string_view input = part_data.get_data();
                std::ofstream out(file_folder + "/" + shared_file->get_id());
                out << input;
                out.close();
            }
        }

        res->set_status(ExtendedApp::http_status_created);

    };

}

automation::http::ExtendedApp::Handler File::remove() {
    auto orig_handler = FactoryService<factory::File, model::File>::remove();
    return [&,oh = orig_handler](auto res, auto req) {

        std::string filename = file_folder + "/" + std::string(req->get_parameter(0));
        ::remove(filename.c_str());

        oh(res, req);

    };
}

automation::http::ExtendedApp::Handler File::update () {
    return [&](auto, auto) {
        throw automation::http::ExtendedApp::not_implemented_error();
    };
}

automation::http::ExtendedApp::Handler File::download () {
    return [&](auto res, auto req) {

        auto file_ptr = factory.get_one(req->get_parameter(0));

        std::string file = file_folder + "/" + file_ptr->get_id();

        std::experimental::filesystem::path p(file);

        spdlog::debug("Requesting file {}.", file);

        try {

            std::ifstream t(file);
            std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

            res->set_header("Content-Type", file_ptr->get_type());
            res->set_header("Content-Disposition", "inline; filename=\"" + file_ptr->get_name() + "\"");
            res->set_status("200 OK");
            res->set_data(str);
        } catch (...) {
            res->set_status("501 Internal Server Error");
        }

    };
}

Json::Value File::to_json(const model::File& file) const {

    Json::Value result;

    result["id"] = file.get_id();
    result["name"] = file.get_name();
    result["type"] = file.get_type();
    result["size"] = file.get_size();

    return result;

}


automation::http::model::File File::from_json(Json::Value, const std::string&) const {

    return model::File();

}
