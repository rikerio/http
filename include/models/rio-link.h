#ifndef __AUTOMATION_HTTP_MODEL_RIO_LINK_H__
#define __AUTOMATION_HTTP_MODEL_RIO_LINK_H__

#include "rikerio-automation/automation.h"

namespace automation::http::model {

class RioLink {

  public:
    RioLink() : id("") { }
    RioLink(std::string& id, std::vector<std::string>& keys) :
        id(id),
        keys(keys) {
    }

    const std::string& get_id() const {
        return id;
    }

    RioLink& set_id(const std::string& value) {
        id = value;
        return *this;
    }

    const std::vector<std::string>& get_keys() const {
        return keys;
    }

    RioLink& add_key(const std::string& value) {
        keys.push_back(value);
        return *this;
    }

  private:

    std::string id;
    std::vector<std::string> keys;

};

}

#endif
