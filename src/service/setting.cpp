#include "service/setting.h"

using namespace automation::http;

service::Setting::Setting(const std::string& base_uri, factory::Setting& factory) :
    service::FactoryService<factory::Setting, model::Setting>(base_uri, factory) { }

Json::Value service::Setting::to_json(const model::Setting& setting) const {

    Json::Value result;

    result["id"] = setting.get_id();
    if (setting.get_type() == "number") {
        try {
            result["value"] = std::stod(setting.get_value());
        } catch (...) {
            result["value"] = 0;
        }
    } else if (setting.get_type() == "boolean") {
        result["value"] = setting.get_value() == "true" || setting.get_value() == "1";
    } else {
        result["value"] = setting.get_value();
    }

    result["type"] = setting.get_type();

    return result;

}


model::Setting service::Setting::from_json(Json::Value body, const std::string&) const {

    if (!body || !body.isObject()) {
        throw ExtendedApp::bad_request_error("Malformed JSON request body.");
    }

    auto json_id = body["id"];
    auto json_type = body["type"];
    auto json_value = body["value"];

    if (!json_id || !json_id.isString()) {
        spdlog::debug("No id present");
        throw ExtendedApp::bad_request_error("Missing id in request body.");
    }

    if (!json_type || !json_type.isString()) {
        spdlog::debug("No type present.");
        throw ExtendedApp::bad_request_error("Missing type in request body.");
    }

    if (!json_value) {
        spdlog::debug("No value present.");
        throw ExtendedApp::bad_request_error("Missing value in request body.");
    }

    const std::string id = json_id.as<std::string>();
    const std::string type = json_type.as<std::string>();
    const std::string value = json_value.as<std::string>();

    if (id.length() < 3) {
        spdlog::debug("id length < 3.");
        throw ExtendedApp::bad_request_error("id to short.");
    }

    if (type != "number" && type != "string" && type != "boolean") {
        spdlog::debug("Unknown type (number, string, boolean).");
        throw ExtendedApp::bad_request_error("Unknown type.");
    }

    model::Setting setting;

    setting
    .set_id(id)
    .set_value(value)
    .set_type(type);

    return setting;


}
